<?php namespace Widget;

use BlogTopic AS ModelBlogTopic;
use View;

class BlogTopic {

	public function getCommentedThisMonth($viewName, array $args = [])
	{
		$args = $this->_checkArgs($args);
		$topics = ModelBlogTopic::whereHas("comments", function($q)
			{
				$q->where("created_at", ">=", \Date::now()->parse("-1 month"));
			})
			->whereHas("blog", function($q)
			{
				$q->where("is_draft", false);
			})
			->with("blog", "preview", "user", "user.group")
			->limit($args["limit"])
			->orderByRaw("(SELECT COUNT(*) AS count FROM comments WHERE comments.commentable_id = blog_topics.id AND comments.commentable_type = 'BlogTopic') DESC")
			->remember(10)->get();

		return View::make($viewName, ["topics" => $topics]);
	}

	public function getNew($viewName, array $args = [])
	{
		$args = $this->_checkArgs($args);
		$topics = ModelBlogTopic::limit(10)
			->with("blog")
			->whereHas("blog", function($q)
			{
				$q->where("is_draft", false);
			})
			->with("blog", "preview", "user", "user.group")
			->orderBy("created_at", "desc")
			->get();

		return View::make($viewName, ["topics"=>$topics]);
	}

	public function getNewAndUseRating($viewName, array $args = [], $hoursLater = 24, $minRatingNum = 1)
	{
		$args = $this->_checkArgs($args);

		if (!is_null($hoursLater)) {
			$hoursLater = (int) $hoursLater;
			if ($hoursLater <= 0) $hoursLater = 1;
		}

		if (!is_null($minRatingNum)) {
			$minRatingNum = (int) $minRatingNum;
			if ($minRatingNum <= 0) $minRatingNum = 0;
		}

		// get topics
		$topics = ModelBlogTopic::limit($args["limit"])
			->with("blog", "preview", "user", "user.group")
			->whereHas("blog", function($q)
			{
				$q->where("is_draft", "=", false);
			});

		if (!is_null($hoursLater) and !is_null($minRatingNum))
		{
			$topics->where(function($q) use ($minRatingNum, $hoursLater)
			{
				$q->where(function($q) use ($minRatingNum)
				{
					// минимальный рейтинг топика для вывода
					$q->where("rating", ">=", $minRatingNum);

					// с момента публикации не больше недели
					$q->where("created_at", ">=", \Date::now()->parse("-4 week"));
				});

				// или с отсрочкой на "жизнь", кол-во часов с момента публикации
				$q->orWhere(function($q) use ($minRatingNum, $hoursLater)
				{
					// нулевой рейтинг и дата публикации не старше дня (т.е новая новость)
					$q->where("rating", "<", $minRatingNum);
					$q->where("created_at", ">=", \Date::now()->parse("-".$hoursLater." hour"));
				});
			});
		}

		$topics = $topics->orderBy("rating", "desc")->get();

		return View::make($viewName, ["topics" => $topics]);		
	}

	/**
	 * Получает топики из блогов (по их ID)
	 * 	Самые рейтинговые с момента публикации не больше недели (учитывая minRatingNum)
	 * 	И новые с рейтингом меньше minRatingNum с отсрочкой на жизнь hoursLater
	 * 
	 * @param string $viewName имя шаблона
	 * @param array $blogIds id блогов
	 * @param array $args limit (кол-во)
	 * @param int|null $hoursLater сколько часов дать топику на жизнь, null - не учитывать часы
	 * @param int $minRatingNum минимальный рейтинг для вывода, null - любой
	 */
	public function getNewFromAndUseRating($viewName, $blogIds, array $args = [], $hoursLater = 24, $minRatingNum = 1)
	{
		if (!is_array($blogIds)) $blogIds = [$blogIds];

		$corrBlogIds = [];
		foreach ($blogIds as $blogId) {
			$blogId = (int) $blogId;
			if (!$blogId) continue;

			$corrBlogIds[] = $blogId;
		}

		if (!$corrBlogIds)
			return null;

		$args = $this->_checkArgs($args);

		if (!is_null($hoursLater)) {
			$hoursLater = (int) $hoursLater;
			if ($hoursLater <= 0) $hoursLater = 1;
		}

		if (!is_null($minRatingNum)) {
			$minRatingNum = (int) $minRatingNum;
			if ($minRatingNum <= 0) $minRatingNum = 0;
		}

		// get topics
		$topics = ModelBlogTopic::whereIn("blog_id", $corrBlogIds)
			->with("blog")
			->whereHas("blog", function($q)
			{
				$q->where("is_draft", "=", false);
			})
			->limit($args["limit"])
			->orderBy("created_at", "desc");

		if (!is_null($hoursLater) and !is_null($minRatingNum))
		{
			$topics->where(function($q) use ($minRatingNum, $hoursLater)
			{
				$q->where(function($q) use ($minRatingNum)
				{
					// минимальный рейтинг топика для вывода
					$q->where("rating", ">=", $minRatingNum);

					// с момента публикации не больше недели
					$q->where("created_at", ">=", \Date::now()->parse("-4 week"));
				});

				// или с отсрочкой на "жизнь", кол-во часов с момента публикации
				$q->orWhere(function($q) use ($minRatingNum, $hoursLater)
				{
					// нулевой рейтинг и дата публикации не старше дня (т.е новая новость)
					$q->where("rating", "<", $minRatingNum);
					$q->where("created_at", ">=", \Date::now()->parse("-".$hoursLater." hour"));
				});
			});
		}

		$topics = $topics->orderBy("created_at", "desc")->remember(5)->get();

		return View::make($viewName, ["topics" => $topics]);
	}

	public function getNewFrom($viewName, $blogIds, array $args = [], array $where = [])
	{
		if (!is_array($blogIds)) $blogIds = [$blogIds];

		$corrBlogIds = [];
		foreach ($blogIds as $blogId) {
			$blogId = (int) $blogId;
			if (!$blogId) continue;

			$corrBlogIds[] = $blogId;
		}

		if (!$corrBlogIds)
			return null;

		$args = $this->_checkArgs($args);

		// get topics
		$topics = ModelBlogTopic::whereIn("blog_id", $corrBlogIds)
			->with("blog")
			->whereHas("blog", function($q)
			{
				$q->where("is_draft", "=", false);
			})
			->limit($args["limit"])
			->orderBy("created_at", "desc");

		foreach ($where as $value) {
			$topics->where($value[0], $value[1], $value[2]);
		}

		$topics = $topics->remember(5)->get();

		return View::make($viewName, ["topics" => $topics]);
	}

	protected function _checkArgs($args)
	{
		$args = array_only($args, ["limit"]);
		if (!isset($args["limit"])) $args["limit"] = 5;
		$args["limit"] = (int) $args["limit"];
		if ($args["limit"] < 1) $args["limit"] = 1;

		return $args;
	}
}