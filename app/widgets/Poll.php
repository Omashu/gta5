<?php namespace Widget;

class Poll {

	// случайный опрос из топиков
	public function getRandomFromBlogTopic($viewName)
	{
		// топик не должен быть в черновом блоге
		// случайный топик с опросами
		// из него случайный опрос
		$topic = \BlogTopic::has("polls", ">=", 1)
			->whereHas("blog", function($q)
			{
				$q->where("is_draft", false);
			})->orderByRaw("RAND()")->first();


		$poll = null;
		if ($topic)
			$poll = $topic->polls()->orderByRaw("RAND()")->first();
	
		return \View::make($viewName, ["poll" => $poll, "topic" => $topic]);
	}
}