<?php namespace Widget;

use Blog AS ModelBlog;
use DB;
use View;

class Blog {

	public function getPopular($viewName, array $args = [])
	{
		$args = $this->_checkArgs($args);

		$blogs = ModelBlog::where("is_draft", "=", false)
			->orderByRaw(DB::raw("(SELECT SUM(blog_topics.rating) FROM blog_topics WHERE blog_topics.blog_id = blogs.id AND deleted_at IS NULL) DESC"))
			->remember(60);

		$blogs->limit($args["limit"]);
		$blogs = $blogs->get();
		
		return View::make($viewName, ["blogs" => $blogs]);
	}

	public function getNew($viewName, array $args = [])
	{
		$args = $this->_checkArgs($args);

		$blogs = ModelBlog::where("is_draft", "=", false)
			->orderBy("created_at", "desc")
			->remember(60);

		$blogs->limit($args["limit"]);
		$blogs = $blogs->get();
		
		return View::make($viewName, ["blogs" => $blogs]);
	}

	protected function _checkArgs($args)
	{
		$args = array_only($args, ["limit"]);
		if (!isset($args["limit"])) $args["limit"] = 5;
		$args["limit"] = (int) $args["limit"];
		if ($args["limit"] < 1) $args["limit"] = 1;

		return $args;
	}
}