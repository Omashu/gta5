<?php

Widget::register('blogPopular', 'Widget\Blog@getPopular');
Widget::register('blogNew', 'Widget\Blog@getNew');

Widget::register('blogTopicNew', 'Widget\BlogTopic@getNew');
Widget::register('blogTopicNewAndUseRating', 'Widget\BlogTopic@getNewAndUseRating');
Widget::register('blogTopicNewFrom', 'Widget\BlogTopic@getNewFrom');
Widget::register('blogTopicNewFromAndUseRating', 'Widget\BlogTopic@getNewFromAndUseRating');

Widget::register('blogTopicCommentedThisMonth', 'Widget\BlogTopic@getCommentedThisMonth');

// Polls
Widget::register("pollRandomFromBlogTopic", 'Widget\Poll@getRandomFromBlogTopic');