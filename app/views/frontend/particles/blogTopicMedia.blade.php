<div class="thumbnail">
	@if ($topic->preview)
		<a href="{{ route("showTopic", $topic->slug )}}" title="{{ $topic->title }}">
			{{ $topic->preview->getHtmlImage($topic->preview->getUrl(427, 200, ["crop"]), $topic->preview->title, ["class" => ""]) }}
		</a>
	@endif

	<div class="caption"@if ($topic->short) style="padding-bottom:0;" @endif>
		<h3><a href="{{ route("showTopic", $topic->slug )}}" title="{{ $topic->title }}">{{ $topic->title }}</a></h3>
		@if ($topic->short)
			<div class="thumbnail-topic-short">
				{{ $topic->getDescriptionShort() }}
			</div>
		@endif
	</div>
	<div class="caption-footer">
		<span class="badge badge-{{$topic->rating>0?'success':($topic->rating<0?'danger':'default')}}">{{ $topic->rating }}</span>
		&nbsp;&nbsp;&nbsp;
		<i class="fa fa-clock-o fa-fw"></i> {{ Date::parse($topic->created_at)->diffForHumans() }}
		&nbsp;&nbsp;&nbsp;
		<i class="fa fa-comments fa-fw"></i> {{ $topic->comments()->count() }}
		&nbsp;&nbsp;&nbsp;
		<i class="fa fa-eye fa-fw"></i> {{ $topic->views }}
		&nbsp;&nbsp;&nbsp;
		<i class="fa fa-folder fa-fw"></i> <a href="{{ route("showBlog", $topic->blog->slug) }}" title="Блог {{{ $topic->blog->title }}}">@if ($topic->blog->is_draft) <span class="text-danger">{{ $topic->blog->title }}</span>@else{{ $topic->blog->title }}@endif</a>
	</div>
</div>