<?php $answersData = $poll->getAnswersData(); ?>

<table class="table polling-results">
	<tbody>
		@foreach($poll->answers as $answer)
			<tr class="{{$answersData["answerIdMax"] == $answer->id ? "winner" : ""}}">
				<td class="percent">
					<b style="color:#333;">{{ $answersData["results"][$answer->id]["percent"] }}%</b>
					<div class="text-muted" title="Кол-во голосов">({{ $answersData["results"][$answer->id]["countVotes"] }})</div>
				</td>
				<td>{{ $answer->answer }}<div style="width: {{ $answersData["results"][$answer->id]["percent"] }}%" class="bar "></div>
				</td>
			</tr>
		@endforeach			
	</tbody>
	<tfoot>
		<tr>
			<td colspan="2" style="padding-bottom:0;"><span class="text-muted">Всего проголосовало {{ $answersData["countAllUniq"] }} чел.</span></td>
		</tr>
	</tfoot>
</table>