@if (Route::currentRouteName()!=="newTopic" and Route::currentRouteName()!=="showTopicEdit")
<a href="{{ route("newTopic") }}" class="btn btn-primary" style="border-radius:0;display:block;margin-bottom:12px;"><i class="fa fa-pencil"></i> Опубликовать свой пост</a>
@endif

@if ($curUser)
@if (!$curUser->isWriteOnly() OR !$curUser->confirmed)
	<div class="alert alert-warning" style="text-align:center;margin-bottom:12px;">
	<ul class="unstyled">
		@if (!$curUser->isWriteOnly())
		<li>
			<a href="/faq.html#users" data-toggle="tooltip" title="Только чтение">Аккаунт заблокирован! 
				@if ($curUser->read_only_to)
					До: <u>{{ Date::parse($curUser->read_only_to)->format("Y-m-d H:i")}}</u>
				@else
					Навсегда :)
				@endif
			</a>
		</li>
		@endif
		@if (!$curUser->confirmed)
			<li><a href="/faq.html#users" data-toggle="tooltip" title="В случае утери пароля, восстановить доступ будет проблематично">E-mail адрес не подтвержден!</a>
				
				<a href="{{ route("sendConfirm") }}" class="btn btn-primary btn-xs" style="margin-top:10px;">Отправить письмо еще раз</a>
			</li>
		@endif
	</ul>
	</div>
@endif

<div class="sblock sblock-nav">
	<div class="sblock-heading">
		<i class="fa fa-user fa-fw"></i> {{ $curUser->username }}
		<span class="label label-{{$curUser->rating>0?'success':($curUser->rating<0?'danger':'default')}} pull-right">{{ $curUser->rating }}</span>
	</div>
	<div class="sblock-body">
		<a href="{{ route("showUserBlogs", $curUser->username) }}">Блоги <span class="badge pull-right">{{ $curUser->blogs->count() }}</span></a>
		<a href="{{ route("showUserTopics", $curUser->username) }}">Топики <span class="badge pull-right">{{ $curUser->blogTopics->count() }}</span></a>
		<a href="{{ route("showUserSubs") }}">Подписки <span class="badge pull-right">{{ $curUser->subscribes->count() }}</span></a>
		<a href="{{ route("showUserStream") }}">Лента <span class="badge pull-right">{{ $curUser->subscribes->sum("new") }}</span></a>
	</div>
</div>
@endif

<div class="sblock sblock-nav">
	<div class="sblock-heading">
		<i class="fa fa-list fa-fw"></i> Главное
	</div>
	<div class="sblock-body">
		<a href="{{ route("showBlog", "novosti-gta-5") }}"><i class="fa fa-angle-right fa-fw"></i> Новости</a>
		<a href="{{ route("showTopic", "sistemnye-trebovaniya-1") }}"><i class="fa fa-angle-right fa-fw"></i> Системные требования</a>
		<a href="{{ route("showTopic", "ocherednoi-perenos-reliza-gta-5-na-pc") }}"><i class="fa fa-angle-right fa-fw"></i> Дата выхода</a>
		<a href="/tag/chit-kody"><i class="fa fa-angle-right fa-fw"></i> Чит-коды</a>
		<a href="/tag/heists"><i class="fa fa-angle-right fa-fw"></i> Heists (Ограбления)</a>

		<a href="/tag/gta6"><i class="fa fa-angle-right fa-fw"></i> <b>GTA 6</b></a>
		<a href="/tag/gta5"><i class="fa fa-angle-right fa-fw"></i> <b>GTA 5</b></a>
		<a href="/tag/gta-online"><i class="fa fa-angle-right fa-fw"></i> <b>GTA Online</b></a>
	</div>
</div>

@pollRandomFromBlogTopic("frontend.widgets.pollRandomFromBlogTopic_sidebar")
@blogTopicCommentedThisMonth("frontend.widgets.blogTopicCommented_sidebar", ["limit" => 5])