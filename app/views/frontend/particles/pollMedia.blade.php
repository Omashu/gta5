<div class="thumbnail">
	@if ($poll->pollable instanceof BlogTopic)
		@if ($poll->pollable->preview)
			<a href="{{ route("showPoll", $poll->slug )}}" title="{{ $poll->title }}">
				{{ $poll->pollable->preview->getHtmlImage($poll->pollable->preview->getUrl(427, 200, ["crop"]), $poll->pollable->preview->title, ["class" => ""]) }}
			</a>
		@endif
	@endif

	<div class="caption">
		<h3><a href="{{ route("showPoll", $poll->slug )}}" title="{{ $poll->title }}">{{ $poll->title }}</a>
			<small>
				@if ($poll->can_use === "users")
					Только зарегистрированные
				@else
					Гости и пользователи
				@endif
			</small>
		</h3>
	</div>
	<div class="caption-footer">
		<i class="fa fa-fw fa-user"></i> <a href="{{ route("showUserProfile", $poll->user->username) }}" class="group-{{$poll->user->group->name}}">{{ $poll->user->username }}</a>
		&nbsp;&nbsp;&nbsp;
		<i class="fa fa-clock-o fa-fw"></i> {{ Date::parse($poll->created_at)->diffForHumans() }}
		&nbsp;&nbsp;&nbsp;
		<i class="fa fa-history fa-fw"></i> {{ $poll->getCountUnique() }}
		&nbsp;&nbsp;&nbsp;
		@if ($poll->pollable instanceof BlogTopic)
			<a title="Перейти к обсуждению {{ $poll->pollable->title }}" href="{{route("showTopic", $poll->pollable->slug )}}"><i class="fa fa-angle-right"></i> {{$poll->pollable->title}}</a>
		@endif
	</div>
</div>