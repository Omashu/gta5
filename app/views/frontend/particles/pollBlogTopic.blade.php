<div class="polling js-polling" data-poll-id="{{ $poll->id }}">
	<div class="poll poll-blog_topic">
		<div class="poll-title">{{ $poll->title }}</div>

		@if ($poll->allow(Auth::user(), Request::getClientIp()))
			<div class="poll-answers js-polling-answers">
				@foreach($poll->answers as $answer)
					@if ($poll->multiple)
						<div class="checkbox">
							<label>
								{{ Form::checkbox("poll_".$poll->id, $answer->id, false) }}
								{{ $answer->answer }}
							</label>
						</div>
					@else
						<div class="radio">
							<label>
								{{ Form::radio("poll_".$poll->id, $answer->id, false) }}
								{{ $answer->answer }}
							</label>
						</div>
					@endif
				@endforeach
			</div>

			<div class="poll-actions js-polling-actions">
				<button type="button" class="btn btn-xs btn-default js-polling-submit">Проголосовать</button>
				<button type="button" class="btn btn-xs btn-default js-polling-results" onclick="return Space.Poll.getResults({{$poll->id}}, $(this), $(this).parent().parent().find('.js-polling-answers'), $(this).parent().parent().find('.js-polling-actions'));">Результаты</button>
			</div>
		@else
			<div class="poll-answers js-polling-answers">
				{{ View::make("frontend/particles.pollResults", ["poll"=>$poll]) }}
			</div>
		@endif

	</div>
</div>