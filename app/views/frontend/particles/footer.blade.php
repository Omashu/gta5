<div class="footer">
	<div class="container-fluid">
		<div class="footer-wp">
			<div class="footer-brand pull-left">&copy; 2015 pivko-pod-gta.ru</div>
			<div class="footer-nav pull-left">
				<a href="/">Главная</a>&nbsp;&nbsp;&nbsp;
				<a href="{{ URL::route("feedback") }}">Обратная связь</a>
			</div>

			<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter28875555 = new Ya.Metrika({id:28875555, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/28875555" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
			<div class="clearfix"></div>
		</div>
	</div>
</div>