<!DOCTYPE html>
<html lang="ru_RU">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	{{ Front::getHtml() }}
</head>
<body>
	<div id="bg_overlay"></div>
	
	@include("frontend.particles.navbar")

	<div class="container">
		<div class="row">
			<div class="col-lg-9">
				<div class="alerts">{{ Message::getView() }}</div>
				<div class="content">
					@yield("content")
				</div>
			</div>
			<div class="col-lg-3">
				<div class="sidebar">
					@yield("sidebarBefore")
					@include("frontend.particles.sidebarMenu")
					@yield("sidebar")
				</div>
			</div>
		</div>
	</div>


	@include("frontend.particles.footer")

</body>
</html>