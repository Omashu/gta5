<div class="row">
	<div class="col-lg-6">
		<div class="home-media">
			@foreach($topics->take(round($topics->count()/2)) as $topic)
				{{ View::make("frontend/particles.blogTopicMedia", ["topic"=>$topic]) }}
			@endforeach
		</div>
	</div>
	<div class="col-lg-6">
		<div class="home-media">
			@foreach($topics->slice(round($topics->count()/2), round($topics->count()/2)) as $topic)
				{{ View::make("frontend/particles.blogTopicMedia", ["topic"=>$topic]) }}
			@endforeach
		</div>
	</div>
</div>


<hr style="margin:0;">
{{ $topics->links() }}