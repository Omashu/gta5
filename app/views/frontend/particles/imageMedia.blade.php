<div class="media">
	<div class="media-left">
		{{ $image->getHtmlImage($image->getUrl(150, null, ["crop"]), $image->title, ["class" => "media-object"]) }}
	</div>
	<div class="media-body">
		<h4 class="media-heading">{{ $image->title ?: "Без названия" }}</h4>
		
		<div style="font-size:11px;">
			{{ $image->description ?: "Описания нет..." }}

			<ul class="unstyled" style="margin-top:10px;">
				<li><b><i class="fa fa-picture-o fa-fw"></i> Размеры:</b> {{ $image->width}}x{{$image->height}} <small>({{ formatBytes($image->bytes) }})</small></li>
				<li><b><i class="fa fa-clock-o fa-fw"></i> Дата загрузки:</b> {{ Date::parse($image->created_at)->format("j F Y H:i") }}</li>
				<li><b><i class="fa fa-clock-o fa-fw"></i> Дата обновления:</b> {{ Date::parse($image->updated_at)->format("j F Y H:i") }}</li>
			</ul>
		</div>
	</div>
</div>