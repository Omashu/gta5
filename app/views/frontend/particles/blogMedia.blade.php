<div class="media media-gray">

	<div class="media-gray-wrapper">
		@if ($blog->preview)
			<div class="media-left pull-left">
				<a href="{{ route("showBlog", $blog->slug )}}" class="thumbnail" style="margin-bottom:0;">
					{{ $blog->preview->getHtmlImage($blog->preview->getUrl(80, 80, ["crop"]), $blog->preview->title, ["class" => "media-object"]) }}
				</a>
			</div>
		@endif

		<div class="media-body">
			<h4 class="media-heading"> 
				<a href="{{ route("showBlog", $blog->slug )}}">{{ $blog->title }}</a>
				<small>{{ Date::parse($blog->updated_at)->diffForHumans() }}</small>
				
			</h4>
	
			@if ($blog->description)
				<div class="media-content" style="text-indent:0;">
					{{ $blog->description }}
				</div>
			@endif

			@if ($curUser)
				<div style="margin-top:15px;">
					<a onclick="return Space.Subscribe.toggleBlog({{$blog->id}}, $(this).parent().find('a:last'), $(this), '.blog{{$blog->id}}-subscribe-count');" data-toggle="tooltip" title="Отписаться" href="javascript:void(0)" class="btn btn-xs btn-info" style="border-radius:3px;font-size:10px;padding:1px 7px;display:@if ($blog->userIsSubscribed($curUser))inline-block @else none @endif;"><span class="blog{{$blog->id}}-subscribe-count">{{ $blog->subscribes->count() }}</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-dot-circle-o"></i>&nbsp;&nbsp;&nbsp;Не читать</a>

					<a onclick="return Space.Subscribe.toggleBlog({{$blog->id}}, $(this).prev(), $(this), '.blog{{$blog->id}}-subscribe-count');" data-toggle="tooltip" title="Подписаться на блог" href="javascript:void(0)" class="btn btn-xs btn-success" style="border-radius:3px;font-size:10px;padding:1px 7px;display:@if ($blog->userIsSubscribed($curUser))none @else inline-block @endif;"><span class="blog{{$blog->id}}-subscribe-count">{{ $blog->subscribes->count() }}</span>&nbsp;&nbsp;&nbsp;<i class="fa fa-dot-circle-o"></i>&nbsp;&nbsp;&nbsp;Читать</a>
				</div>
			@endif
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="media-footer">
		@if (isset($blogRatings))
		<span class="badge badge-{{$blogRatings[$blog->id]>0?'success':($blogRatings[$blog->id]<0?'danger':'default')}}">{{ $blogRatings[$blog->id] }}</span> 
		@endif
		<i class="fa fa-fw fa-user"></i> <a href="{{ route("showUserProfile", $blog->user->username) }}" class="group-{{$blog->user->group->name}}">{{ $blog->user->username }}</a>
		<span class="pull-right"><span data-toggle="tooltip" title="Подписчиков"><i class="fa fa-users fa-fw"></i> {{ $blog->subscribes()->count() }}</span> &nbsp;&nbsp; <span data-toggle="tooltip" title="Топиков"><i class="fa fa-check-circle-o"></i> {{ $blog->topics->count() }}</span>
		</span>
	</div>
</div>
<div class="clearfix"></div>