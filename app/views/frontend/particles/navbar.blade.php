<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="/"><i class="fa fa-fw fa-home"></i> PIVKO</a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li @if (Route::currentRouteName()=="home") class="active" @endif><a href="/">Главная</a></li>
				<li><a href="/blog/novosti-gta-5">Новости GTA 5</a></li>
				<li @if (Route::currentRouteName()=="showBlogsPopular") class="active" @endif><a href="{{ route("showBlogsPopular") }}">Блоги</a></li>
				<li class="dropdown @if (in_array(Route::currentRouteName(), ["showNewTopics", "showCommentedTopics"])) active @endif">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Топики <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li @if (Route::currentRouteName()=="showNewTopics") class="active" @endif><a href="{{ route("showNewTopics") }}"><i class="fa fa-clock-o fa-fw"></i> Свежее</a></li>
						<li @if (Route::currentRouteName()=="showCommentedTopics") class="active" @endif><a href="{{ route("showCommentedTopics") }}"><i class="fa fa-comments fa-fw"></i> Обсуждаемое</a></li>
					</ul>
				</li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Участники <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{route("showUsers",["t"=>"new"])}}">Новички</a></li>
						<li><a href="{{route("showUsers",["t"=>"last"])}}">Активные</a></li>
						<li class="divider"></li>
						<li><a href="{{route("showUsers",["sex"=>"male"])}}"><i class="fa fa-male fa-fw"></i> Парни</a></li>
						<li><a href="{{route("showUsers",["sex"=>"female"])}}"><i class="fa fa-female fa-fw"></i> Девушки</a></li>
					</ul>
				</li>
				<li @if (Route::currentRouteName()=="showNewPolls") class="active" @endif><a href="{{ route("showNewPolls") }}"><i class="fa fa-question fa-fw"></i> Опросы</a></li>
				@if ($curUser)
					<li><a href="{{ url('faq.html') }}"><i class="fa fa-question-circle fa-fw"></i> FAQ</a></li>
				@endif
			</ul>
			<ul class="nav navbar-nav navbar-right">
				@if (!$curUser)
					<li><a href="/login"><i class="fa fa-sign-in"></i> Вход</a></li>
					<li><a href="/register"><i class="fa fa-plus-circle"></i> Регистрация</a></li>
				@else
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ $curUser->username }} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="{{ route("showUserProfile", $curUser->username) }}">Профиль</a></li>
							<li><a href="{{ route("showUserBlogs", $curUser->username) }}">Мои блоги</a></li>
							<li><a href="{{ route("showUserTopics", $curUser->username) }}">Мои топики</a></li>
							@if (Blog::allowAdd($curUser))
								<li class="divider"></li>
								@if (Blog::allowAdd($curUser))
								<li><a href="{{ route("newBlog") }}">Создать свой блог</a></li>
								@endif
								@if (BlogTopic::allowAdd($curUser))
								<li><a href="{{ route("newTopic") }}">Создать свой топик</a></li>
								@endif
							@endif
							<li class="divider"></li>
							<li><a href="{{ route("logout") }}">Выход</a></li>
						</ul>
					</li>
				@endif
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>