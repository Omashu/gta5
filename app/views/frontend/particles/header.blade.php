<div class="header">
	<div class="header-nav center-block">
		<ul class="nav nav-pills">
			<li @if (Route::currentRouteName()=="home") class="active" @endif><a href="/">Главная</a></li>
			<li><a href="/blog/novosti-gta-5">Новости GTA 5</a></li>
			<li @if (Route::currentRouteName()=="showBlogsPopular") class="active" @endif><a href="{{ route("showBlogsPopular") }}">Блоги</a></li>
			<li @if (Route::currentRouteName()=="showNewTopics") class="active" @endif><a href="{{ route("showNewTopics") }}">Свежее</a></li>
			@if ($curUser)
				<li><a href="{{ url('faq.html') }}">FAQ</a></li>
			@endif
		</ul>
	</div>
	
	<div class="clearfix"></div>
	<div class="header-userbar">

		@if (!$curUser)
			<div>
				<a href="/login" class="btn btn-success"><i class="fa fa-sign-in"></i> Вход</a>
				<a href="/register" class="btn btn-danger"><i class="fa fa-plus-circle"></i> Регистрация</a>
			</div>
		@endif
	</div>
</div>