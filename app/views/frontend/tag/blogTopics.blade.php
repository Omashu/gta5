@extends("frontend.particles.layout")

@section("sidebarBefore")


	<div class="sblock sblock-nav">
		<div class="sblock-heading">
			По тегу <u>{{ $tag->name }}</u>
		</div>
		<div class="sblock-body">
			<a href="{{ route("showTag", $tag->slug) }}">Топики <span class="badge pull-right">{{ $topics->count() }}</span></a>
		</div>
	</div>



@stop

@section("content")

	<div class="page-header">
		<h1>Топики по тегу <b>{{ $tag->name }}</b></h1>
	</div>

	@if ($topics->count())
		{{ View::make("frontend/particles.blogTopicsRowTwoColumns", ["topics"=>$topics]) }}
	@else
		<blockquote style="margin-bottom: 0;font-size: 14px;">
			По тегу <b>{{{ $tag->name }}}</b> ничего нет
		</blockquote>
	@endif

@stop