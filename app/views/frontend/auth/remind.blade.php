@extends("frontend.particles.layout")

@section("content")

{{ Form::open() }}

	<div class="panel panel-default panel-noborder">
		<div class="panel-heading">Восстановление доступа к аккаунту</div>
		<div class="panel-body">

				{{ Form::label("ident", "Логин или e-mail адрес") }}
				{{ Form::text("ident", Input::get("ident"), ["class" => "form-control"]) }}
				<div class="help-block">Ваш логин на сайте или e-mail адрес указанный при регистрации, инструкции будут высланы на e-mail адрес</div>

		</div>

		<div class="panel-footer">
			<button type="submit" class="btn btn-success"><i class="fa fa-paper-plane-o"></i> Восстановить доступ</button>
			<div class="btn-group pull-right">
				<a href="/login" class="btn btn-default"><i class="fa fa-sign-in"></i> Войти</a>
				<a href="/register" class="btn btn-default">Зарегистрироваться</a>
			</div>
		</div>

	</div>


{{ Form::close() }}
@stop