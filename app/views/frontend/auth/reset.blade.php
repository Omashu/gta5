@extends("frontend.particles.layout")

@section("content")

{{ Form::open() }}

	{{ Form::hidden("token", $token) }}

	<div class="panel panel-default panel-noborder">
		<div class="panel-heading">Установите новый пароль</div>
		<div class="panel-body">

				<div class="form-group">
					{{ Form::label("ident", "Логин или e-mail адрес") }}:
					{{ Form::text("ident", Input::get("ident"), ["class" => "form-control"]) }}
					<div class="help-block">Ваш логин на сайте или e-mail адрес указанный при регистрации</div>
				</div>

				<div class="form-group">
					{{ Form::label("password", "Новый пароль") }}:
					{{ Form::password("password", ["class" => "form-control"]) }}
				</div>

				<div class="form-group">
					{{ Form::label("password_confirmation", "Повторите новый пароль") }}:
					{{ Form::password("password_confirmation", ["class" => "form-control"]) }}
				</div>

				<div class="form-group">
					{{ Form::label("captcha", "Код безопасности") }}:
					<div>
						{{ HTML::image(Captcha::img(), 'Captcha image', ["class" => "thumbnail", "style" => "margin-bottom:5px;"]) }}
						<div class="col-xs-3">
							{{ Form::text('captcha', null, ["class" => "form-control"]) }}
						</div>
					</div>
				</div>
		</div>

		<div class="panel-footer">
			<button type="submit" class="btn btn-success"><i class="fa fa-paper-plane-o"></i> Сменить пароль</button>
			<div class="btn-group pull-right">
				<a href="/login" class="btn btn-default"><i class="fa fa-sign-in"></i> Войти</a>
				<a href="/register" class="btn btn-default">Зарегистрироваться</a>
			</div>
		</div>

	</div>


{{ Form::close() }}
@stop