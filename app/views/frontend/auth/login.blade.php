@extends("frontend.particles.layout")

@section("content")

{{ Form::open() }}

	<div class="panel panel-default panel-noborder">
		<div class="panel-heading">Вход на сайт</div>
		<div class="panel-body">

			<div class="form-group">
				{{ Form::label("ident", "Логин или e-mail адрес") }}
				{{ Form::text("ident", Input::get("ident"), ["class" => "form-control"]) }}
			</div>


				{{ Form::label("password", "Пароль") }}
				{{ Form::password("password", ["class" => "form-control"]) }}

				<div class="help-block"><a href="/password/remind">Напомнить пароль</a></div>

		</div>

		<div class="panel-footer">
			<button type="submit" class="btn btn-success"><i class="fa fa-sign-in"></i> Войти</button>
			<a href="/register" class="btn btn-default pull-right">Зарегистрироваться</a>
		</div>

	</div>


{{ Form::close() }}
@stop