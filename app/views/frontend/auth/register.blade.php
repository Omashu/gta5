@extends("frontend.particles.layout")

@section("content")

{{ Form::open() }}

	<div class="panel panel-default panel-noborder">
		<div class="panel-heading">Регистрация на сайте</div>
		<div class="panel-body">

			<div class="form-group">
				{{ Form::label("username", "Логин") }}
				{{ Form::text("username", Input::get("username"), ["class" => "form-control"]) }}
			</div>

			<div class="form-group">
				{{ Form::label("email", "E-mail адрес") }}
				{{ Form::text("email", Input::get("email"), ["class" => "form-control"]) }}
			</div>

			<div class="form-group">
				{{ Form::label("password", "Пароль") }}
				{{ Form::password("password", ["class" => "form-control"]) }}
			</div>


				{{ Form::label("password_confirmation", "Пароль еще раз") }}
				{{ Form::password("password_confirmation", ["class" => "form-control"]) }}


		</div>

		<div class="panel-footer">
			<button type="submit" class="btn btn-success">Зарегистрироваться</button>
			<a href="/login" class="btn btn-default pull-right">Войти</a>
		</div>

	</div>

{{ Form::close() }}

@stop