@extends("frontend.particles.layout")

@section("content")

	<div class="btn-group" role="group" style="margin:10px;">
		<a href="#blogs" class="btn btn-default btn-sm">#Блоги</a>
		<a href="#blogTopics" class="btn btn-default btn-sm">#Топики</a>
		<a href="#users" class="btn btn-default btn-sm">#Пользователи</a>
	</div>

	<div class="panel panel-success" style="margin-bottom:0px;">
		<div class="panel-body">Если Вы не нашли ответа на свой вопрос, задайте его мне, через обратную связь или личные сообщения.</div>
	</div>

	<div class="panel panel-default" id="blogs" style="margin-bottom:0px;">
		<div class="panel-heading">Блоги</div>
		<div class="panel-body">
			<div class="faq-q"><b>Как создать свой блог?</b></div>
			<div class="faq-a">
				Пройдите по ссылке <a href="{{ route("newBlog") }}">{{ route("newBlog") }}</a>. Вы должны быть зарегистрированы и аккаунт не заблокированным.
			</div>
		</div>
		<div class="panel-body">
			<div class="faq-q"><b>Как формируется рейтинг блога?</b></div>
			<div class="faq-a">
				Рейтинг блога чисто символический и формируется на основе рейтинга его топиков.
			</div>
		</div>
		<div class="panel-body">
			<div class="faq-q"><b>Все читатели моего блога могут добавлять в него свои топики, как запретить это действие?</b></div>
			<div class="faq-a">
				Зайдите в раздел реактирования своего блога, в левом сайдбаре будет вкладка <b>Привилегии</b>, там можно детально настроить права подписчиков и персонала блога.
			</div>
		</div>
		<div class="panel-body">
			<div class="faq-q"><b>Я добавил в блог пользователя и выдал ему несколько прав, он стал указываться как модератор блога, почему?</b></div>
			<div class="faq-a">
				Система автоматически считает пользователя модератором блога, если у него активны возможности добавления, изменения и возврата топика автору (перенос из блога в его черновики).
			</div>
		</div>
	</div>

	<div class="panel panel-default" id="blogTopics" style="margin-bottom:0px;">
		<div class="panel-heading">Топики</div>
		<div class="panel-body">
			<div class="faq-q"><b>Как создать свой топик?</b></div>
			<div class="faq-a">
				Пройдите по ссылке <a href="{{ route("newTopic") }}">{{ route("newTopic") }}</a>. Вы должны быть зарегистрированы и аккаунт не заблокированным. Предварительно создайте свой блог или подпишитесь на публичные блоги других пользователей.
			</div>
		</div>
		<div class="panel-body">
			<div class="faq-q"><b>Как формируется рейтинг топика?</b></div>
			<div class="faq-a">
				Мнением читетелей. Посредством Лайков / Дизлайков. <span class="text-danger">Откровенная накрутка рейтинга топика <u>видна и будет зачищаться</u>.</span>
			</div>
		</div>
		<div class="panel-body">
			<div class="faq-q"><b>Что дает рейтинг топика?</b></div>
			<div class="faq-a">
				Заплюсованные топики дольше висят на главной, соответственно у них будет все больше и больше просмотров. В своих топиках Вы можете рекламировать свои проекты, в меру.
			</div>
		</div>
		<div class="panel-body">
			<div class="faq-q"><b>Можно ли запретить пользователям писать комментарии к моим топикам?</b></div>
			<div class="faq-a">
				Возможность отсутствует и вводить ее не планируется. Если под Вашим топиком развелся срач мнений используйте кнопку <b>жалоба</b> возле "плохого" комментария, этим действием Вы пригласите модератора. Администраторы и модераторы блога могут блокировать такие комментарии. Полезные, не содержащие спама и резких высказываний комментарии блокировать <b>запрещено</b> основными правилами сайта. Нарушение повлечет за собой предупржедение или бан (временный или перманентный на усмотрение модератора или администратора сайта).
			</div>
		</div>
	</div>

	<div class="panel panel-default" id="users" style="margin-bottom:0px;">
		<div class="panel-heading">Пользователи</div>
		<div class="panel-body">
			<div class="faq-q"><b>Как повышается мой рейтинг?</b></div>
			<div class="faq-a">
				За каждый + к комментарию, его автору прибавляется n рейтинга, за + к топику так же.
			</div>
		</div>
	</div>
@stop