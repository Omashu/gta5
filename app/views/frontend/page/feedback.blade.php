@extends("frontend.particles.layout")

@section("content")
	{{ Form::open() }}
	<div class="panel panel-default" style="margin-bottom:0px;">
		<div class="panel-heading">Обратная связь</div>
		<div class="panel-body">
			<div class="form-group">
				{{ Form::label("email", "Ваш e-mail")}}:
				{{ Form::text("email", Input::get("email", $curUser?$curUser->email:null), ["class" => "form-control"]) }}
			</div>
			<div class="form-group">
				{{ Form::label("theme", "Тема")}}:
				{{ Form::text("theme", Input::get("theme"), ["class" => "form-control"]) }}
			</div>
			<div class="form-group">
				{{ Form::label("message", "Сообщение")}}:
				{{ Form::textarea("message", Input::get("message"), ["class" => "form-control"]) }}
			</div>
			<div class="form-group">
				{{ Form::label("captcha", "Код безопасности") }}:
				<div>
					{{ HTML::image(Captcha::img(), 'Captcha image', ["class" => "thumbnail", "style" => "margin-bottom:5px;"]) }}
					<div class="col-xs-3">
						{{ Form::text('captcha', null, ["class" => "form-control"]) }}
					</div>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			{{ Form::submit("Отправить письмо", ["class" => "btn btn-success"]) }}
		</div>
	</div>
	{{ Form::close() }}
@stop