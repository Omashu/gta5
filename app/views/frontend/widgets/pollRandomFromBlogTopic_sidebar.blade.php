@if ($poll)
	<div class="panel panel-default panel-sidebar panel-sidebar_vote_random">
		<div class="panel-heading">Опрос: <u>{{ $poll->title }}</u></div>
		<div class="panel-body">
			
			{{ View::make("frontend.particles.pollBlogTopic", ["poll" => $poll]) }}

			<div style="padding:5px;text-align:center;"><b>Опрос в топике:</b> <a href="{{ route("showTopic", $topic->slug) }}">{{ $topic->title }}</a></div>
		</div>
	</div>
@endif