@foreach($blogs as $blog)
	<div class="thumbnail">
		@if ($blog->preview)
			<a href="{{ route("showBlog", $blog->slug )}}" title="{{ $blog->title }}">
				{{ $blog->preview->getHtmlImage($blog->preview->getUrl(427, 120, ["crop"]), $blog->preview->title, ["class" => ""]) }}
			</a>
		@endif

		<div class="caption">
			<h3><a href="{{ route("showBlog", $blog->slug )}}" title="{{ $blog->title }}">{{ $blog->title }}</a></h3>
		</div>
		<div class="caption-footer">
			<i class="fa fa-clock-o fa-fw"></i> {{ Date::parse($blog->updated_at)->diffForHumans() }}
			&nbsp;&nbsp;&nbsp;
			<i class="fa fa-th fa-fw"></i> {{ $blog->topics()->count() }}
			&nbsp;&nbsp;&nbsp;
			@if ($blog->topics()->count())
				<a title="Перейти к обсуждению {{ $blog->topics()->orderBy("created_at","desc")->first()->title }}" href="{{route("showTopic", $blog->topics()->orderBy("created_at","desc")->first()->slug )}}"><i class="fa fa-angle-right"></i> {{$blog->topics()->orderBy("created_at","desc")->first()->title}}</a>
			@endif
		</div>
	</div>
@endforeach