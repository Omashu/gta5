@foreach($topics as $topic)
	<div class="thumbnail">
		@if ($topic->preview)
			<a href="{{ route("showTopic", $topic->slug )}}" title="{{ $topic->title }}">
				{{ $topic->preview->getHtmlImage($topic->preview->getUrl(427, 200, ["crop"]), $topic->preview->title, ["class" => ""]) }}
			</a>
		@endif

		<div class="caption">
			<h3><a href="{{ route("showTopic", $topic->slug )}}" title="{{ $topic->title }}">{{ $topic->title }}</a></h3>
		</div>
		<div class="caption-footer">
			<i class="fa fa-clock-o fa-fw"></i> {{ Date::parse($topic->created_at)->diffForHumans() }}
			&nbsp;&nbsp;&nbsp;
			<i class="fa fa-comments fa-fw"></i> {{ $topic->comments()->count() }}
			&nbsp;&nbsp;&nbsp;
			<i class="fa fa-eye fa-fw"></i> {{ $topic->views }}
			&nbsp;&nbsp;&nbsp;
			<i class="fa fa-folder fa-fw"></i> <a href="{{ route("showBlog", $topic->blog->slug) }}" title="Блог {{{ $topic->blog->title }}}">{{ $topic->blog->title }}</a>
		</div>
	</div>
@endforeach