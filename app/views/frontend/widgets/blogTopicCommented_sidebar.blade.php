@if ($topics)
	<div class="panel panel-default panel-sidebar panel-sidebar_topic_commented">
		<div class="panel-heading">Обсуждаемые топики за месяц</div>
		<div class="panel-body">
			@foreach($topics as $topic)
				<div class="thumbnail">
					@if ($topic->preview)
						<a href="{{ route("showTopic", $topic->slug )}}" title="{{ $topic->title }}" class="thumbnail-link-image">
							{{ $topic->preview->getHtmlImage($topic->preview->getUrl(427, 200, ["crop"]), $topic->preview->title, ["class" => ""]) }}
						</a>
					@endif

					<div class="caption">
						<h3><a href="{{ route("showTopic", $topic->slug )}}" title="{{ $topic->title }}">{{ $topic->title }} (<i class="fa fa-comments fa-fw"></i> {{ $topic->comments()->count() }})</a></h3>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endif