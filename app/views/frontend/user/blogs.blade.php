@extends("frontend.particles.layout")

@section("content")

	<div class="page-header">
		<h1>
			@if ($curUser and $curUser->id == $user->id)
				Ваши блоги
			@else
				Блоги пользователя <b>{{ $user->username }}</b>
			@endif
		</h1>
	</div>

	@if ($blogDraft)

		<div class="media bg-success" style="margin-top: 0;padding: 10px;">
			@if ($blogDraft->preview)
			<div class="media-left pull-left">
				<a href="{{ route("showBlog", $blogDraft->slug )}}">
					{{ $blogDraft->preview->getHtmlImage($blogDraft->preview->getUrl(100,null,["crop"]), $blogDraft->title, ["class" => "media-object"]) }}
				</a>
			</div>
			@endif

			<div class="media-body" style="display:block;width:100%;">
				<h4 class="media-heading"> 
					<a href="{{ route("showBlog", $blogDraft->slug )}}">{{ $blogDraft->title }}</a>
					<small>{{ Date::parse($blogDraft->updated_at)->diffForHumans() }}</small>
				</h4>

				<div>Этот блог можете просматривать только Вы. @if ($blogDraft->topics->count()) Допишите <i>незаконченные статьи ({{$blogDraft->topics->count()}})</i> и смело публикуйте! @endif</div>
			</div>

			<div class="clearfix"></div>
		</div>
	@endif

	{{ $blogs->links() }}

		@if (!$blogs->count())
			@if ($curUser and $curUser->id == $user->id)
				<blockquote style="margin-bottom: 0;font-size: 14px;"><a href="{{ route("newBlog")}}">Создайте свой блог</a>, публикуйте в него свои <u>заметки по GTA5</u> или просто подключите свой канал YouTube и Ваши видео будут показываться нашим посетителям.</blockquote>
			@else
				<blockquote style="margin-bottom: 0;font-size: 14px;">У <a href="{{ route("showUserProfile", $user->username) }}"><b>{{ $user->username }}</b></a> нет своих публичных блогов.</blockquote>
			@endif
		@endif

		@foreach($blogs as $blog)
			{{ View::make("frontend/particles.blogMedia", ["blog"=>$blog]) }}
		@endforeach

	{{ $blogs->links() }}

@stop