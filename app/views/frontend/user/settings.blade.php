@extends("frontend.particles.layout")

@section("sidebarBefore")

<div class="sblock sblock_white sblock-nav">
	<div class="sblock-heading">
		<i class="fa fa-user fa-fw"></i> {{ $curUser->username }}</a>
	</div>
	<div class="sblock-body">
		<a href="{{ route("showUserProfile", $curUser->username) }}"><i class="fa fa-user fa-fw"></i> Обратно в профиль</a>
	</div>
</div>

@stop

@section("content")

@section("content")

	{{ Form::open(["files" => true]) }}
	<div class="panel panel-default panel-noborder">
		<div class="panel-heading">Редактирование профиля</div>
		<div class="panel-body">

			<div class="form-group">
				{{ Form::label("username", "Логин") }} <small>(изменить невозможно)</small>:
				{{ Form::text("username", $curUser->username, ["class" => "form-control disabled", "disabled"])}}
			</div>

			<div class="form-group">
				{{ Form::label("email", "E-mail") }}:
				{{ Form::text("email", Input::get("email", $curUser->email), ["class" => "form-control"])}}
			</div>

			<div class="form-group">
				{{ Form::label("first_name", "Имя") }}:
				{{ Form::text("first_name", Input::get("first_name", $curUser->first_name), ["class" => "form-control"])}}
			</div>

			<div class="form-group">
				{{ Form::label("last_name", "Фамилия") }}:
				{{ Form::text("last_name", Input::get("last_name", $curUser->last_name), ["class" => "form-control"])}}
			</div>

			<div class="form-group">
				{{ Form::label("sex", "Пол") }}:
				{{ Form::select("sex", $curUser->sex_array, Input::get("sex", $curUser->sex), ["class" => "form-control"])}}
			</div>

			<div class="form-group">
				{{ Form::label("about_myself", "О себе") }}:
				{{ Form::textarea("about_myself", Input::get("about_myself", $curUser->about_myself), ["rows" => 3, "class" => "form-control"])}}
			</div>

			<div class="form-group">
				{{ Form::label("avatar", "Аватар") }}:
				{{ Form::file("avatar", ["class" => "form-control"])}}

				@if ($curUser->getKey() AND $curUser->avatar)
					<div class="media" style="margin-top:15px;">
						<div class="media-left">
							{{ $curUser->avatar->getHtmlImage($curUser->avatar->getUrl(120, 70, ["crop"]), $curUser->avatar->title, ["class" => "media-object"]) }}
						</div>
						<div class="media-body">
							<div class="checkbox">
								<label>{{ Form::checkbox("remove_avatar", 1, !!Input::get("remove_avatar", false)) }} Удалить аватар</label>
							</div>
						</div>
					</div>
				@endif
			</div>

			<div class="form-group">
				<a href="javascript:void(0);" onclick="$('#new_password').toggle(300);" class="btn btn-primary"><i class="fa fa-key fa-fw"></i> Изменить пароль</a>
			</div>

			<div id="new_password" style="display:none;">
				<div class="form-group">
					{{ Form::label("last_password", "Старый пароль") }}:
					{{ Form::password("last_password", ["class" => "form-control"]) }}
					<div class="help-block">* не заполнять, если не хотите изменить пароль</div>
				</div>
				<div class="form-group">
					{{ Form::label("password", "Новый пароль") }}:
					{{ Form::password("password", ["class" => "form-control"]) }}
				</div>
				<div class="form-group">
					{{ Form::label("password_confirmation", "Новый пароль еще раз") }}:
					{{ Form::password("password_confirmation", ["class" => "form-control"]) }}
				</div>
			</div>

		</div>
		
		<div class="panel-footer">
			<button class="btn btn-success" type="submit"><i class="fa fa-check"></i> Сохранить</button>
			<a href="{{ route("showUserProfile", $curUser->username) }}" class="btn btn-default pull-right"><i class="fa fa-times"></i> Отменить</a>
		</div>
	</div>

	{{ Form::close() }}

@stop

@stop