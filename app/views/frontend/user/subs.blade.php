@extends("frontend.particles.layout")

@section("content")

	<div class="page-header">
		<h1>Вы подписаны на блоги</h1>
	</div>
		
	@forelse($subs as $sub)
		{{ View::make("frontend/particles.blogMedia", ["blog"=>$sub->subscribeable]) }}
	@empty
		<blockquote style="margin-bottom: 0;font-size: 14px;">У Вас нет подписок. <a href="{{ route("showBlogsPopular")}}">Подпишитесь</a> на интересные Вам блоги и новые топики будут появляться у Вас в ленте.</blockquote>
	@endforelse

	@if ($subs->count())
		<hr style="margin-bottom:0;">
	@endif

	{{ $subs->links() }}
@stop