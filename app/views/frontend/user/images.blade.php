@extends("frontend.particles.layout")

@section("content")
	

	<div class="page-header">
		<h1>
			@if ($curUser and $curUser->id == $user->id)
				Ваши загруженные изображения
			@else
				Изображения пользователя <b>{{ $user->username }}</b>
			@endif

			<small>({{ formatBytes($sumBytes) }})</small>
		</h1>
	</div>

	@if ($images->count())
		<div class="home-media">
			@foreach($images as $image)
				{{ View::make("frontend/particles.imageMedia", ["image" => $image]) }}
			@endforeach
		</div>
	@else
		<blockquote style="margin-bottom: 0;font-size: 14px;">
			@if ($curUser and $curUser->id == $user->id)
				У Вас нет загруженных изображений.
			@else
				У <a href="{{ route("showUserProfile", $user->username) }}"><b>{{ $user->username }}</b></a> нет загруженных изображений.
			@endif
		</blockquote>
	@endif

@stop