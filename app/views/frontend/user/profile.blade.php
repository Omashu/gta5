@extends("frontend.particles.layout")

@section("sidebarBefore")

<div class="sblock sblock_white sblock-nav">
	<div class="sblock-heading">
		<i class="fa fa-user fa-fw"></i> {{ $user->username }}</a>
	</div>
	<div class="sblock-body">
		@if ($curUser && $user->id == $curUser->id)
		<a href="{{ route("showUserProfileSettings") }}"><i class="fa fa-pencil fa-fw"></i> Редактировать профиль</a>
		@endif
	</div>
</div>

@stop

@section("content")

	@if ($curUser AND $curUser->id == $user->id AND (!$user->isWriteOnly() OR !$user->confirmed))
		<div style="padding:10px;">
		<div class="alert alert-danger" style="text-align:center;">
		<ul class="unstyled">
			@if (!$curUser->isWriteOnly())
			<li>
				<a href="/faq.html#users" data-toggle="tooltip" title="Только чтение">Аккаунт заблокирован! 
					@if ($curUser->read_only_to)
						До: <u>{{ Date::parse($curUser->read_only_to)->format("Y-m-d H:i")}}</u>
					@else
						Навсегда :)
					@endif
				</a>
			</li>
			@endif
			@if (!$curUser->confirmed)
				<li><a href="/faq.html#users" data-toggle="tooltip" title="В случае утери пароля, восстановить доступ будет проблематично">E-mail адрес не подтвержден!</a>
					
					<div><a href="{{ route("sendConfirm") }}" class="btn btn-default btn-xs" style="margin-top:10px;">Отправить письмо еще раз</a></div>
				</li>
			@endif
		</ul>
		</div>
		</div>
	@endif

	<div class="row">
		<div class="col-lg-8">
			<div style="padding:10px;">
				<div class="media" style="margin-top:0;">
					@if ($user->avatar)
					<div class="media-left">
						{{ $user->avatar->getHtmlImage($user->avatar->getUrl(70,null,["crop"]), $user->avatar->title, ["class" => "media-object"]) }}
					</div>
					@endif
					<div class="media-body">
						<h4 class="media-heading"> {{ $user->username }} <small>@if ($user->fullname) {{ $user->fullname }} @endif @if ($user->sex) <i class="fa fa-fw fa-{{$user->sex}}" title="{{$user->sex_lang}}" data-toggle="tooltip"></i> @endif</small></h4>
						<div style="text-indent:20px;">{{ $user->about_myself }}</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-lg-4">
			<div style="padding:10px;">

					<ul class="unstyled">
						<li><i class="fa fa-thumbs-o-up fa-fw"></i> Рейтинг: <b>{{ $user->rating }}</b></li>
						<li><i class="fa fa-users fa-fw"></i> Группа: <span class="group-{{ $user->group->name }}">{{ $user->group->title }}</span></li>
					</ul>

					@if (!$curUser OR $curUser->id != $user->id)
						<div class="list-group" style="margin-top:10px;">
							<a class="list-group-item" href="{{ route("showUserBlogs", $user->username) }}">Блоги <span class="badge pull-right">{{ $user->blogs->count() }}</span></a>
							<a class="list-group-item" href="{{ route("showUserTopics", $user->username) }}">Топики <span class="badge pull-right">{{ $user->blogTopics->count() }}</span></a>
						</div>
					@endif
			</div>
		</div>
	</div>

@stop