@extends("frontend.particles.layout")

@section("content")
	

	<div class="page-header">
		<h1>
			@if ($curUser and $curUser->id == $user->id)
				Ваши топики
			@else
				Топики пользователя <b>{{ $user->username }}</b>
			@endif
		</h1>
	</div>

	@if ($topics->count())
		{{ View::make("frontend/particles.blogTopicsRowTwoColumns", ["topics"=>$topics]) }}
	@else
		<blockquote style="margin-bottom: 0;font-size: 14px;">
			@if ($curUser and $curUser->id == $user->id)
				У Вас нет своих топиков.
			@else
				У <a href="{{ route("showUserProfile", $user->username) }}"><b>{{ $user->username }}</b></a> нет своих топиков.
			@endif
		</blockquote>
	@endif

@stop