@extends("frontend.particles.layout")

@section("content")
	
		<div class="page-header">
			<h1>Ваша лента топиков</h1>
		</div>
		
		@if ($topics->count())
			{{ View::make("frontend/particles.blogTopicsRowTwoColumns", ["topics"=>$topics]) }}
		@else
			<blockquote style="margin-bottom: 0;font-size: 14px;">У Вас нет подписок или в блогах нет топиков. <a href="{{ route("showBlogsPopular")}}">Подпишитесь</a> на интересные Вам блоги и новые топики будут появляться у Вас в ленте.</blockquote>
		@endif

@stop