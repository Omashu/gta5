@extends("frontend.particles.layout")

@section("content")
		<div class="page-header">
			<h1>
				{{{ $poll->title }}}
			</h1>
		</div>

	<div style="border-bottom:0;padding:5px 10px;">
		<i class="fa fa-fw fa-user"></i> <a href="{{ route("showUserProfile", $poll->user->username) }}" class="group-{{$poll->user->group->name}}">{{ $poll->user->username }}</a>
		&nbsp;&nbsp;&nbsp;
		<i class="fa fa-clock-o fa-fw"></i> {{ Date::parse($poll->created_at)->diffForHumans() }}
		&nbsp;&nbsp;&nbsp;
		<i class="fa fa-history fa-fw"></i> {{ $poll->getCountUnique() }}
		&nbsp;&nbsp;&nbsp;
		@if ($poll->pollable instanceof BlogTopic)
			<a title="Перейти к обсуждению {{ $poll->pollable->title }}" href="{{route("showTopic", $poll->pollable->slug )}}"><i class="fa fa-angle-right"></i> {{$poll->pollable->title}}</a>
		@endif
	</div>

	<div class="panel panel-default panel-noborder" style="border-bottom: 1px solid #ddd;">
		<div class="panel-body" style="padding:0;">
			@if ($poll->pollable instanceof BlogTopic)
				{{ View::make("frontend/particles.pollBlogTopic", ["poll"=>$poll]) }}
			@endif

			@if ($poll->pollable instanceof BlogTopic)
				<div style="margin:10px;"><a title="Перейти к обсуждению {{ $poll->pollable->title }}" href="{{route("showTopic", $poll->pollable->slug )}}" class="btn btn-success"><i class="fa fa-angle-right"></i> Перейти к топику <b>{{$poll->pollable->title}}</b></a></div>
			@endif
		</div>
	</div>

	<div class="panel panel-default" style="border-top:0;">
		<div class="panel-heading">График</div>
		<div class="panel-body">
			<div id="graphDataCountSels">
				
			</div>
		</div>
	</div>

<script>
jQuery(function()
{
	new Morris.Bar({
		element: $('#graphDataCountSels'),
		data: {{ json_encode($graphDataCountSels) }},
		xkey: 'answer',
		ykeys: ['count'],
		labels: ['Голосов'],
		grid : false
	});
});
</script>

@stop