@extends("frontend.particles.layout")

@section("sidebarBefore")





@stop

@section("content")

		<div class="page-header">
			<h1>Опросы на сайте
				<span class="badge pull-right">{{ $polls->count() }}</span>
			</h1>
		</div>

		<?php
		// разобьем на 2 колонки
		$column1 = [];
		$column2 = [];
		$curColumn = 2;

		foreach ($polls as $poll)
		{
			if ($curColumn === 2)
			{
				$column1[] = $poll;
				$curColumn = 1;
				continue;
			}

			$column2[] = $poll;
			$curColumn = 2;
		}
		?>

		@if (!$column1 AND !$column2)
			<blockquote style="margin-bottom: 0;font-size: 14px;">
				Опросов нет.
			</blockquote>
		@else
			<div class="row">
				<div class="col-lg-6">
					<div class="home-media">
						@foreach($column1 as $poll)
						{{ View::make("frontend/particles.pollMedia", ["poll"=>$poll]) }}
						@endforeach
					</div>
				</div>
				<div class="col-lg-6">
					<div class="home-media" style="border-right:0;">
						@foreach($column2 as $poll)
						{{ View::make("frontend/particles.pollMedia", ["poll"=>$poll]) }}
						@endforeach
					</div>
				</div>
			</div>
		@endif

	@if ($polls->count())
		<hr style="margin-bottom:0;">
	@endif
	{{ $polls->links() }}

@stop