@extends("frontend.particles.layout")

@section("sidebarBefore")

	@if ($blog->getKey())
		<div class="sblock sblock_white sblock-nav">
			<div class="sblock-heading">
				<i class="fa fa-user fa-fw"></i> <a class="group-{{ $blog->user->group->name }}" href="{{ route("showUserProfile", $blog->user->username) }}">{{ $blog->user->username }}</a>
			</div>
			<div class="sblock-body">
				<a href="{{ route("showBlogEditPermissions", $blog->slug) }}"><i class="fa fa-link fa-fw"></i> Настройка привилегий</a>

				@if ($blog->allowDelete($curUser))
				<a href="javascript:void(0);" onclick="return Space.Blog.deleteFast({{$blog->id}},$(this));"><span class="text-danger"><i class="fa fa-times fa-fw"></i> Удалить блог</span></a>
				@endif
			</div>
		</div>
	@endif

@stop

@section("content")

	{{ Form::open(["files" => true]) }}
	<div class="panel panel-default panel-noborder">
		<div class="panel-heading">Создание блога</div>
		<div class="panel-body">

				<div class="form-group">
					{{ Form::label("title", "Название") }}:
					{{ Form::text("title", Input::get("title", $blog->title), ["class" => "form-control"])}}
					<div class="help-block">Короткое, осмысленное название, о чем будут топики в этом блоге.</div>
				</div>

				<div class="form-group">
					{{ Form::label("description", "Описание") }}:
					{{ Form::textarea("description", Input::get("description", $blog->description), ["class" => "form-control"])}}
					<div class="help-block">Описание блога, используйте доступные html-теги для формирования приличного вида.</div>
				</div>

				<div class="form-group" style="margin-bottom:0px;">
					{{ Form::label("preview", "Превью") }}:
					{{ Form::file("preview", ["class" => "form-control"])}}
					<div class="help-block">Иконка Вашего блога.</div>
				</div>

				@if ($blog->getKey() AND $blog->preview)
					<div class="media" style="margin-top:15px;">
						<div class="media-left">
							{{ $blog->preview->getHtmlImage($blog->preview->getUrl(120, 70, ["crop"]), $blog->preview->title, ["class" => "media-object"]) }}
						</div>
						<div class="media-body">
							<div class="checkbox">
								<label>{{ Form::checkbox("remove_preview", 1, !!Input::get("remove_preview", false)) }} Открепить это превью</label>
							</div>
						</div>
					</div>
				@endif
		</div>
		
		<div class="panel-footer">
			<button class="btn btn-success" type="submit"><i class="fa fa-plus"></i> {{ $blog->getKey() ? "Обновить" : "Создать" }}</button>
		</div>
	</div>

	{{ Form::close() }}

@stop