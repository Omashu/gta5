@extends("frontend.particles.layout")

@section("sidebarBefore")

<div class="sblock sblock_white sblock-nav">
	<div class="sblock-heading">
		<i class="fa fa-user fa-fw"></i> <a class="group-{{ $blog->user->group->name }}" href="{{ route("showUserProfile", $blog->user->username) }}">{{ $blog->user->username }}</a>
	</div>
	<div class="sblock-body">
		<a href="{{ route("showBlogEdit", $blog->slug) }}"><i class="fa fa-pencil fa-fw"></i> Редактирование блога</a>
		
		@if ($blog->allowDelete($curUser))
		<a href="javascript:void(0);" onclick="return Space.Blog.deleteFast({{$blog->id}},$(this));"><span class="text-danger"><i class="fa fa-times fa-fw"></i> Удалить блог</span></a>
		@endif
	</div>
</div>

@stop

@section("content")

	<div class="media" style="margin-top: 0;padding: 10px;background: #F5F5F5;border-bottom:1px solid #ddd;">
		@if ($blog->user->avatar)
		<div class="media-left pull-left">
			<a href="{{ route("showBlog", $blog->user->username )}}" class="thumbnail" style="margin-bottom:0;">
				{{ $blog->user->avatar->getHtmlImage($blog->user->avatar->getUrl(50,50,["crop"]), $blog->user->title, ["class" => "media-object"]) }}
			</a>
		</div>
		@endif

		<div class="media-body" style="display:block;width:100%;">
			<h4 class="media-heading"> 
				<span class="text-danger">{{ $blog->user->username }}</span> @if($curUser->id==$blog->user_id) <small>это Вы</small>@endif
			</h4>
			<span class="text-muted"><i>Администратор блога</i></span>
		</div>

		<div class="clearfix"></div>
	</div>
	
	<div class="panel panel-default panel-noborder">
		<div class="panel-heading">Особые пользователи</div>
		<div class="panel-body" style="padding:0;">

			<table class="table" style="margin-bottom:0;">
				<thead>
					<tr>
						<th><abbr title="Ник пользователя, к которому хотим применить привилегии, например, как у Вас {{ $curUser->username}}">Ник <small>(начните ввод)</small></abbr></th>
						<th><abbr title="Публиковать топики в блог без учета минимально необходимого рейтинга подписчика">Публикатор</abbr></th>
						<th><abbr title="Возможность модерировать чужие топики в этом блоге (отправлять их авторам на доработку или исключать навсегда)">Модератор</abbr></th>
						<th></th>
					</tr>
				</thead>
				<tbody id="permissions">
					<tr style="display:none;">
						<td width="25%">
							<input type="text" class="form-control input-sm js-username">
							<input type="hidden" value="{{ $blog->getKey() }}" class="js-blog-id">
						</td>
						<td width="25%">
							<select class="form-control input-sm js-flag-write">
								<option value="1">Разрешить публикацию</option>
								<option value="0">Запретить публикацию</option>
							</select>
						</td>
						<td width="25%">
							<select class="form-control input-sm js-flag-moderate">
								<option value="1">Разрешить модерировать остальные топики</option>
								<option value="0">Запретить модерировать остальные топики</option>
							</select>
						</td>
						<td width="208px">
							<div class="btn-group">
								<button class="btn btn-sm btn-success js-save"><i class="fa fa-check fa-fw"></i> Сохранить</button>
								<button class="btn btn-sm btn-danger js-delete"><i class="fa fa-times fa-fw"></i> Удалить</button>
							</div>
						</td>
					</tr>

					@foreach($blog->permissions as $permission)
					<tr>
						<td width="25%">
							{{ Form::text("", $permission->user->username, ["class" => "form-control input-sm js-username"]) }}
							<input type="hidden" value="{{ $blog->getKey() }}" class="js-blog-id">
						</td>
						<td width="25%">
							{{ Form::select("", [1=>"Разрешить публикацию", 0=>"Запретить публикацию"], $permission->flag_write, ["class" => "form-control input-sm js-flag-write"]) }}
						</td>
						<td width="25%">
							{{ Form::select("", [1=>"Разрешить модерировать остальные топики", 0=>"Запретить модерировать остальные топики"], $permission->flag_moderate, ["class" => "form-control input-sm js-flag-moderate"]) }}
						</td>
						<td width="208px">
							<div class="btn-group">
								<button class="btn btn-sm btn-success js-save"><i class="fa fa-check fa-fw"></i> Сохранить</button>
								<button class="btn btn-sm btn-danger js-delete"><i class="fa fa-times fa-fw"></i> Удалить</button>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>
		<div class="panel-footer">
			<button class="btn btn-default" type="button" id="add_permission_button"><i class="fa fa-fw fa-plus"></i> Добавить поле</button>
		</div>
	</div>

	{{ Form::open() }}
	<div class="panel panel-default panel-noborder" style="border-top:1px solid #ddd;">
		<div class="panel-heading">Прочие настройки</div>
		<div class="panel-body">

			<div class="checkbox">
				<label>
					{{ Form::checkbox("permission_public", 1, !!Input::get("permission_public", $blog->permission_public)) }} Подписчики могут публиковать топики
				</label>
			</div>

			<div class="form-group" style="margin-bottom:0;">
				{{ Form::label("permission_public_min_rating", "Минимальный рейтинг подписчиков")}}:
				{{ Form::text("permission_public_min_rating", Input::get("permission_public_min_rating", $blog->permission_public_min_rating), ["class" => "form-control", "placeholder" => 0]) }}
			</div>

		</div>

		<div class="panel-footer">
			{{ Form::submit("Сохранить", ["class" => "btn btn-success"]) }}
		</div>
	</div>

	{{ Form::close() }}


	<script>
		jQuery(function()
		{
			Space.Blog.aEPermissions({
				btn : $('#add_permission_button'),
				cont : $('#permissions')
			});
		});
	</script>
@stop