@extends("frontend.particles.layout")

@section("sidebarBefore")

<div class="sblock sblock_white sblock-nav">
	<div class="sblock-heading">
		<i class="fa fa-user fa-fw"></i> <a class="group-{{ $blog->user->group->name }}" href="{{ route("showUserProfile", $blog->user->username) }}">{{ $blog->user->username }}</a>
	</div>
	<div class="sblock-body">
		@if ($blog->allowEdit($curUser))
			<a href="{{ route("showBlogEdit", $blog->slug) }}"><i class="fa fa-pencil fa-fw"></i> Настроить блог</a>
			<a href="{{ route("showBlogEditPermissions", $blog->slug) }}"><i class="fa fa-link fa-fw"></i> Настройка привилегий</a>
		@endif
		@if (!$blog->is_draft and $blog->description)
			<div class="sidebar-blog-description">{{ $blog->description }}</div>
		@endif
	</div>
</div>

@stop

@section("content")

	<div class="page-header">
		<h1>{{ $blog->title }}</h1>
	</div>

	@if ($topics->count())
		{{ View::make("frontend/particles.blogTopicsRowTwoColumns", ["topics"=>$topics]) }}
	@else
		@if ($blog->is_draft)
			<blockquote style="margin-bottom: 0;font-size: 14px;">В Ваших черновиках ничего нет :)</blockquote>
		@else
			<blockquote style="margin-bottom: 0;font-size: 14px;">
				Блог <b>{{ $blog->title }}</b> пуст. 
				@if ($blog->allowNewTopic($curUser))
					Вы можете <a href="{{ route("newTopic", ["blog_id"=>$blog->id]) }}">написать свой топик</a> в этот блог.
				@endif
			</blockquote>
		@endif
	@endif

@stop