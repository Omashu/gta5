@extends("frontend.particles.layout")

@section("content")

	<div class="page-header">
		<h1>Блоги <span class="badge pull-right">{{ $blogs->getTotal() }}</span></h1>
	</div>

	@foreach($blogs as $blog)
		{{ View::make("frontend/particles.blogMedia", ["blog"=>$blog, "blogRatings" => $blogRatings]) }}
	@endforeach

	<hr style="margin-bottom:0;">
	{{ $blogs->links() }}

@stop