<div class="panel-body" style="padding:0;">
	<div id="topic_polls">

		<?php $iter = 0; ?>
		@foreach($aPolls as $aPoll)
			<?php $iter++ ?>

			<div class="media" data-iter="{{$iter}}">
				{{ Form::hidden("_polls[".$iter."][id]", isset($aPoll["id"])?$aPoll["id"]:null) }}

				<div class="media-body" style="width: 100%; display: block;">
					<h4 class="media-heading" style="margin-bottom: 0px;">
						<div class="input-group">
							{{ Form::text("_polls[".$iter."][title]", $aPoll["title"], ["class" => "form-control", "placeholder" => "Вопрос"]) }}

							<div class="input-group-addon" style="padding: 0px 10px; border-left-width: 0px; border-right-width: 0px;">
								<div class="btn-group">

									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-users fa-fw"></i> Только пользователи</button>

									{{ Form::hidden("_polls[".$iter."][can_use]", $aPoll["can_use"]) }}

									<ul class="dropdown-menu" role="menu">
										<li><a href="javascript:void(0);" data-can-use="users"><i class="fa fa-users fa-fw"></i> Только пользователи</a></li>
										<li><a href="javascript:void(0);" data-can-use="users_and_ghosts"><i class="fa fa-user-secret fa-fw"></i> Пользователи и гости</a></li>
									</ul>
								</div>
							</div>
							<div class="input-group-addon" style="padding: 0px 10px;">
								<button type="button" class="btn btn-default btn-xs"><i class="fa fa-plus fa-fw"></i> Ответ</button>
							</div>
							<div class="input-group-addon" style="padding: 0px 10px;border-left:0;">
								{{ Form::hidden("_polls[".$iter."][multiple]", !!$aPoll["multiple"]) }}
								<button type="button" class="btn btn-default btn-xs {{ !!$aPoll["multiple"] ? "active" : ""}}"><i class="fa fa-check-square-o fa-fw"></i> Множественый</button>
							</div>
							<div class="input-group-addon" style="padding: 0px 10px;">
								<button type="button" class="btn btn-default btn-xs"><i class="fa fa-times fa-fw"></i></button>
							</div>
						</div>
					</h4>

					@foreach($aPoll["answers"] as $aAnswer)
						<div class="input-group media-answer-row" style="margin-top: 5px;">

							@if (is_array($aAnswer))
								<input type="text" value="{{{$aAnswer["answer"]}}}" name="_polls[{{$iter}}][answers][old][{{{$aAnswer["id"]}}}]" class="form-control" placeholder="Вариант ответа"/>
							@else
								<input type="text" value="{{{$aAnswer}}}" name="_polls[{{$iter}}][answers][new][]" class="form-control" placeholder="Вариант ответа"/>
							@endif
							<div class="input-group-addon" style="padding: 0px 10px;">
								<button type="button" class="btn btn-default btn-xs"><i class="fa fa-times fa-fw"></i></button>
							</div>
						</div>
					@endforeach

				</div>
			</div>
		@endforeach
	</div>
</div>