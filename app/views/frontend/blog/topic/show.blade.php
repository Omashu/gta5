@extends("frontend.particles.layout")

@section("sidebarBefore")
	@if ($topic->allowEdit($curUser))
		<div class="sblock sblock_white sblock-nav">
			<div class="sblock-heading">
				Управление
			</div>
			<div class="sblock-body">
				<a href="{{ route("showTopicEdit", $topic->id) }}"><i class="fa fa-pencil fa-fw"></i> Редактировать топик</a>
				@if ($topic->allowDelete($curUser))
					<a href="javascript:void(0);" onclick="return Space.BlogTopic.deleteFast({{ $topic->id}},$(this));"><i class="fa fa-trash fa-fw"></i> Удалить топик</a>
				@endif
			</div>
		</div>
	@endif
@stop

@section("content")
	<div class="page-header"><h1>{{{ $topic->title }}}</h1></div>

	<div class="topic-top_counters">
		<i class="fa fa-user fa-fw"></i> <a class="group-{{$topic->user->group->name}}" href="{{ route("showUserProfile", $topic->user->username) }}">{{ $topic->user->username }}</a> &nbsp;&mdash;&nbsp; <a href="{{ route("showBlog", $topic->blog->slug) }}" title="Блог {{{ $topic->blog->title }}}">{{ $topic->blog->title }}</a>

		<div class="pull-right"><i class="fa fa-eye fa-fw"></i> {{ $topic->views }}</div>
	</div>

	<div class="panel panel-default panel-noborder" style="border-bottom: 1px solid #ddd;">
		<div class="panel-body" style="padding:0 5px;">
			<div class="topic-content clearfix">{{ $topic->getDescription() }}</div>
		</div>

		@if ($topic->files->count())
		<div class="panel-body" style="padding:10px;padding-top:0;">			
			<div class="topic-files">
				<table class="table table-hover table-condensed" style="margin-bottom:0;">
					<thead>
						<tr>
							<th>Файлы:</th>
							<th></th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($topic->files as $file)
						<tr>
							<td width="100%">{{ $file->title }}</td>
							<td align="center">
								<a href="{{ route("fileDownload", $file->slug) }}" class="btn btn-xs btn-default"><i class="fa fa-download fa-fw"></i> Скачать <i>{{ formatBytes($file->bytes) }}</i></a>
							</td>
							<td align="center">
								<a class="btn btn-default btn-xs" href="#" style="font-weight:bold;"><small>Загрузок:</small> {{ $file->downloads()->count() }}</a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
		@endif

		@if ($topic->polls()->count())
			<div class="panel-body" style="padding:0;">
				@foreach($topic->polls as $poll)
					{{ View::make("frontend/particles.pollBlogTopic", ["poll"=>$poll]) }}
				@endforeach
			</div>
		@endif

		<div class="panel-body" style="border-top:1px solid #ddd;padding:10px;">
			<div class="topic-author">
				<div><i class="fa fa-user fa-fw"></i> <b>Автор:</b> <a class="group-{{$topic->user->group->name}}" href="{{ route("showUserProfile", $topic->user->username) }}">{{ $topic->user->username }}</a></div>
				<div><i class="fa fa-clock-o fa-fw"></i> <b>Дата:</b> {{ Date::parse($topic->created_at)->format("Y-m-d в H:i") }}</div>
			</div>

			<div class="topic-rating">
				@if ($topic->ratingAllowUseByUser($curUser))
				<a href="javascript:void(0);" onclick="return Space.Rating.down(_VALUES['url.ajaxRatingBlogTopicDown'], {{$topic->id}}, $(this), $(this).parent().find('.topic-rating-val'));" class="topic-rating-down {{($topic->ratingObjectByUser($curUser) and !$topic->ratingObjectByUser($curUser)->direction)?'topic-rating-active':'topic-rating-notactive'}}"><i class="fa fa-thumbs-o-down"></i></a>
				@endif

				<div class="topic-rating-val topic-rating-val_{{ $topic->rating > 0 ? "green" : ($topic->rating < 0 ? "red" : "gray") }}">{{ $topic->rating }}</div>
				
				@if ($topic->ratingAllowUseByUser($curUser))
				<a href="javascript:void(0);" onclick="return Space.Rating.up(_VALUES['url.ajaxRatingBlogTopicUp'], {{$topic->id}}, $(this), $(this).parent().find('.topic-rating-val'));" class="topic-rating-up {{($topic->ratingObjectByUser($curUser) and $topic->ratingObjectByUser($curUser)->direction)?'topic-rating-active':'topic-rating-notactive'}}"><i class="fa fa-thumbs-o-up"></i></a>
				@endif
			</div>

			<div class="clearfix"></div>
			@if ($topic->tagged->count())
				<div class="topic-tags" style="margin-bottom:0px;">
				 @foreach($topic->tagged as $tag)
				 	<div class="pull-left" style="margin-top:5px;"><a href="{{ route("showTag", $tag->tag_slug) }}" class="label label-default" style="font-size:11px;"><i class="fa fa-tag fa-fw"></i> {{ $tag->tag_name }}</a>&nbsp;</div>
					@endforeach
				</div>
			@endif
		</div>
	</div>
	
	@if ($curUser)
		<div class="panel panel-success panel-noborder" style="border-bottom: 1px solid #ddd;">
			<div class="panel-body"><div id="comment_form"></div></div>
		</div>
	@endif

	<div class="panel panel-default panel-noborder">
		<div class="panel-heading" style="background:#F7F7F7;font-weight:bold;">
			Всего комментариев: {{ $topic->comments->count() }}
		</div>
		<div class="panel-body" style="padding:5px;padding-top:0px;">
			@if (!$topic->comments->count())
				<div class="text-danger" style="padding:10px;">Комментариев нет. Твой будет первым!</div>
			@endif
			<div id="comments">
				@foreach($topic->comments()->with("user", "user.avatar")->withDepth()->defaultOrder()->get() as $comment)
					{{ View::make("frontend/comments.blogTopic", ["comment"=>$comment,"topic"=>$topic])->render() }}
				@endforeach
			</div>
		</div>
	</div>

@if ($curUser)
<script>
jQuery(function() {
	var CommentProccess = new Space.Comment.FormController({
			formTemplate : new Space.Comment.DefaultFormTmpl(),
			commentsContainer : $('#comments'),
			formContainer : $('#comment_form'),
			route : "ajaxCommentBlogTopic",
			id : {{$topic->id}},
			answerButtonSelector : '.comment .js-answer',
			formAnswerTemplate : new Space.Comment.DefaultFormTmpl()
	});
});
</script>
@endif

@stop