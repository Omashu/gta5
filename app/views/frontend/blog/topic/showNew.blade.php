@extends("frontend.particles.layout")

@section("content")

	<div class="page-header">
		<h1>Свежие топики <span class="badge pull-right">{{ $topics->getTotal() }}</span></h1>
	</div>

	@if ($topics->count())
		{{ View::make("frontend/particles.blogTopicsRowTwoColumns", ["topics"=>$topics]) }}
	@else
		<blockquote style="margin-bottom: 0;font-size: 14px;">
			Материалов нет.
		</blockquote>
	@endif
	
@stop