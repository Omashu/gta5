@extends("frontend.particles.layout")

@section("sidebarBefore")

	<div class="panel panel-default panel-sidebar panel-sidebar_image_upload">
		<div class="panel-heading">Загрузка изображений</div>
		<div class="panel-body">
			<div id="images_container">
				@foreach($oImages as $oImage)
					<div class="media">
						<div class="media-left">
							{{ $oImage->getHtmlLink($oImage->getUrl(), $oImage->getHtmlImage($oImage->getUrl(50,50,["crop"]), $oImage->title, ["class" => "thumbnail", "style" => "width:50px;height:50px;margin-bottom:0;"]), ["target" => "_blank"]) }}
						</div>
						<div class="media-body">
							<h4 class="media-heading">{{ $oImage->title }} <small>({{ formatBytes($oImage->bytes) }})</small></h4>
						</div>
						<div class="btn-group">
							<button class="btn btn-primary btn-xs js-preview @if($oImage->id==Input::old("preview_id", $topic->preview_id))active @endif" data-id="{{$oImage->id}}"><i class="fa fa-image"></i> Превью</button>
							<button class="btn btn-danger btn-xs js-delete" data-id="{{$oImage->id}}"><i class="fa fa-trash-o"></i> Удалить</button>
							<button class="btn btn-default btn-xs js-push" data-relurl="{{$oImage->getRelPath()}}"><i class="fa fa-arrow-circle-o-left"></i> Вставить</button>
						</div>
					</div>
				@endforeach

			</div>
			<div class="text-muted" style="text-align:center;" id="images_empty"><i class="fa fa-question"></i> Ничего не загружено</div>
		</div>
		<div class="panel-body" style="padding:10px;">
			<span class="btn btn-default btn-xs fileinput-button" style="display:block;">
				<i class="fa fa-picture-o"></i>
				<span>Выбрать изображение</span>
				<input type="file" name="image" id="input_image">
			</span>
		</div>
	</div>

	<div class="panel panel-default panel-sidebar panel-sidebar_file_upload">
		<div class="panel-heading">Загрузка файлов</div>
		<div class="panel-body">
			<div id="files_container">
				@foreach($oFiles as $oFile)
					<div class="media">
						<div class="media-body">
							<h4 class="media-heading">{{ $oFile->title }} <small>({{ formatBytes($oFile->bytes) }})</small></h4>
							<div><span class="text-muted" style="font-size:11px;">Дата: <b>{{ Date::parse($oFile->updated_at)->format("j F Y в H:i") }}</b></span></div>
							<div class="btn-group"><button class="btn btn-xs btn-primary js-edit" data-id="{{$oFile->id}}"><i class="fa fa-pencil"></i> Изменить</button><button class="btn btn-xs btn-danger js-delete" data-id="{{$oFile->id}}"><i class="fa fa-trash-o"></i> Удалить</button></div>
						</div>
					</div>
				@endforeach
			</div>
			<div class="text-muted" style="text-align:center;" id="files_empty"><i class="fa fa-question"></i> Ничего не загружено</div>
		</div>
		<div class="panel-body" style="padding:10px;">
			<span class="btn btn-default btn-xs fileinput-button" style="display:block;">
				<i class="fa fa-file-archive-o"></i>
				<span>Выбрать файл</span>
				<input type="file" name="file" id="input_file">
			</span>
		</div>
	</div>

@stop

@section("content")
	{{ Form::open(["style"=>"margin-bottom:0;"]) }}
	<div class="panel panel-default panel-noborder">
		<div class="panel-heading">
			{{{ $topic->title or "Создание топика" }}}
			@if (Blog::allowAdd($curUser))
				<a href="{{ route("newBlog") }}" class="pull-right btn btn-success btn-xs" style="position:relative;top:-3px;border-radius:3px;">Создать раздел</a>
			@endif
		</div>
		<div class="panel-body" style="padding-bottom:0;">
			
			<div id="image_ids">
				@foreach($oImages as $oImage)
					<input type="hidden" name="_image_ids[]" id="_image_id_{{{$oImage->id}}}" value="{{{$oImage->id}}}">
				@endforeach
			</div>

			<div id="file_ids">
				@foreach($oFiles as $oFile)
					<input type="hidden" name="_file_ids[]" id="_file_id_{{{$oFile->id}}}" value="{{{$oFile->id}}}">
				@endforeach
			</div>

				{{ Form::hidden("preview_id", $topic->preview_id, ["id" => "preview_id"]) }}

				<div class="form-group">
					{{ Form::label("blog_id", "В какой раздел опубликовать") }}:
					{{ Form::select("blog_id", $blogsAssoc, Input::get("blog_id", $topic->blog_id), ["class" => "form-control"])}}
					<div class="help-block">Создайте свой блог или вступите в публичные блоги для публикации топиков в них.</div>
				</div>

				<div class="form-group">
					{{ Form::label("title", "Заголовок") }}:
					{{ Form::text("title", Input::get("title", $topic->title), ["class" => "form-control"])}}
					<div class="help-block">Короткий, осмысленный заголовок, о чем будет топик.</div>
				</div>

				<div class="form-group">
					{{ Form::label("description", "Содержимое") }}:
					{{ Form::textarea("description", Input::get("description", $topic->description), ["class" => "form-control"])}}
					<div class="help-block">Содержимое топика, используйте доступные html-теги для формирования приличного вида. <span class="text-danger"><b>Используйте тег <u>CUT</u></b> (правая, крайняя кнопка, средней панели), чтобы разделить вводну часть от основной!</span> * Мы не запрещаем Вам указывать ссылки на свои проекты, главное что бы публикуемый материал был интересным.</div>
				</div>

				<div class="form-group">
					{{ Form::label("_tags", "Теги") }}:
					{{ Form::text("_tags", Input::get("_tags", implode(", ", $topic->tagged->lists("tag_name"))), ["class" => "form-control", "id" => "tags"])}}
					<div class="help-block">Короткие смысловые слова или фразы для общего разделения смысла топиков. Разделяйте запятой. Например, <i>gta5</i>, <i>системные требования</i>, <i>yandex</i>.</div>
				</div>


				<div class="form-group">
					<button class="btn btn-success" id="add_poll_button" type="button"><i class="fa fa-question"></i> Добавить в топик опрос</button>
					<div class="help-block bg-danger">В опросе должно быть минимум 2 варианта ответа, максимум 30. Максимальное кол-во опросов в одном топике не более 15-ти.</div>
				</div>

		</div>

		{{ View::make("frontend/blog.topic.polls", ["aPolls"=>$aPolls])}}

		<div class="panel-footer">
			{{ Form::hidden("_mode", "draft", ["id" => "_mode"]) }}
			<button class="btn btn-default btn-save" data-mode="draft" data-input="#_mode">Сохранить в черновиках</button>
			
			@if ($topic->getKey() AND $topic->allowDelete($curUser))
				<a class="btn btn-danger" href="javascript:void(0);" onclick="return Space.BlogTopic.deleteFast({{$topic->id}},$(this));">Удалить топик</a>
			@endif

			<button class="btn btn-success btn-save" data-mode="public" data-input="#_mode">Опубликовать в блог</button>
		</div>
	</div>

	{{ Form::close() }}

<script>
	jQuery(function()
	{
		Space.Tag.autocomplete($('#tags'));

		Space.BlogTopic.aEImages(
		{
			file : $('#input_image'),
			cont : $('#images_container'),
			textareaId : 'description',
			contInputs : $('#image_ids'),
			previewInput : $('#preview_id'),
			hiddenInputName : '_image_ids[]',
			emptyBlock : $('#images_empty')
		});

		Space.BlogTopic.aEFiles(
		{
			file : $('#input_file'),
			cont : $('#files_container'),
			contInputs : $('#file_ids'),
			hiddenInputName : '_file_ids[]',
			emptyBlock : $('#files_empty')
		});

		Space.BlogTopic.aEPolls(
		{
			btn : $("#add_poll_button"),
			cont : $("#topic_polls")
		});

		Space.CKEDITOR.replace("description", "BlogTopicAE");
		Space.AButtons.mode($(".btn-save"));
	});
</script>
@stop