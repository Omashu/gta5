@extends("frontend.particles.layout")

@section("content")

	<div class="page-header">
		<h1>Обсуждаемые топики <span class="badge pull-right">{{ $topics->getTotal() }}</span></h1>
	</div>

	<div class="row">
		<div class="col-lg-4">
			<div class="page-header" style="padding: 3px 14px;font-size: 12px;@if ($period=="day") background:#f1f1f1;font-weight:bold; @endif">
				<a href="{{ route("showCommentedTopics", "day") }}">За день</a>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="page-header" style="padding: 3px 14px;font-size: 12px;@if ($period=="week") background:#f1f1f1;font-weight:bold; @endif">
				<a href="{{ route("showCommentedTopics", "week") }}">За неделю</a>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="page-header" style="padding: 3px 14px;font-size: 12px;@if ($period=="month") background:#f1f1f1;font-weight:bold; @endif">
				<a href="{{ route("showCommentedTopics", "month") }}">За месяц</a>
			</div>
		</div>
	</div>

	@if ($topics->count())
		{{ View::make("frontend/particles.blogTopicsRowTwoColumns", ["topics"=>$topics]) }}
	@else
		<blockquote style="margin-bottom: 0;font-size: 14px;">
			Обсуждаемых материалов за этот период нет.
		</blockquote>
	@endif

	{{ $topics->links() }}

@stop