@extends("frontend.particles.layout")

@section("content")

<div class="row">
	<div class="col-lg-6">
		<div class="page-header">
			<h1>Свежее</h1>
		</div>
		
		<div class="home-media">@blogTopicNew("frontend.widgets.blogTopicNew_home", ["limit" => 10])</div>
	</div>
	<div class="col-lg-6">
		<div class="page-header">
			<h1>Заплюсованное</h1>
		</div>

		<div class="home-media" style="border-right:0;">@blogTopicNewAndUseRating("frontend.widgets.blogTopicNew_home", ["limit" => 10])</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-6">
		<div class="page-header">
			<h1>Популярные блоги</h1>
		</div>
		
		<div class="home-media">@blogPopular("frontend.widgets.blogPopular_home", ["limit" => 10])</div>
	</div>
	<div class="col-lg-6">
		<div class="page-header">
			<h1>Новые блоги</h1>
		</div>

		<div class="home-media" style="border-bottomright:0;">@blogNew("frontend.widgets.blogNew_home", ["limit" => 10])</div>
	</div>
</div>


@stop