@extends("frontend.particles.layout")

@section("content")
	<div class="page-header">
		<h1>Участники <span class="badge pull-right">{{ $users->getTotal() }}</span></h1>
	</div>

	@if (!$users->count())
		<blockquote style="margin-bottom: 0;font-size: 14px;">
			Пусто...
		</blockquote>
	@else
		<div style="padding:12px 0;">
		@foreach($users as $user)
			<div class="media media-gray">
				<div class="media-gray-wrapper">
					<div class="media-left">
						<a href="{{ route("showUserProfile", $user->username) }}" title="{{$user->username}}">
							@if ($user->avatar)
								{{ $user->avatar->getHtmlImage($user->avatar->getUrl(48,48,["crop"]), $user->avatar->title, []) }}
								@else
									<img src="/app/frontend/images/avatar_male_48x48.png" alt="{{$user->username}}" style="width:48px;height:48px;">
								@endif
						</a>
					</div>
					<div class="media-body">
						<h4 class="media-heading">
							<i title="{{ Date::parse($user->updated_at)->format("j F Y в H:i") }}" data-toggle="tooltip" class="fa fa-circle fa-fw" style="font-size:12px;color:{{$user->isOnline('green','red')}};"></i>
							<a href="{{ route("showUserProfile", $user->username) }}" title="{{$user->username}}">{{ $user->username }}</a> @if ($user->fullname)<small>{{ $user->fullname }}</small>@endif 
							@if ($user->sex) <i style="font-size:12px;" class="fa fa-fw fa-{{$user->sex}}" title="{{$user->sex_lang}}" data-toggle="tooltip"></i> @endif
						</h4>
						{{ $user->isWriteOnly(null, '<b style="color:red;">Заблокирован</b> &nbsp; ') }}<b>Блогов:</b> {{$user->blogs()->where("is_draft",false)->count() }} &nbsp; <b>Топиков:</b> {{ $user->blogTopics_countPublished() }}
					</div>
				</div>
			</div>
		@endforeach
		</div>
	@endif


	{{ $users->links() }}
@stop