<?php $nicknameStyles = ["padding:0 5px;border-radius:5px;"];
$nicknameTitle = $comment->user->username;

if ($curUser AND $curUser->id == $comment->user_id) {
	// это ваш комментарий
	$nicknameStyles[] = "background: rgba(255, 222, 135, 0.53);";
	$nicknameTitle .= " это Вы";
} else if ($comment->user_id == $topic->user_id) {
	// это комментарий автора топика
	$nicknameStyles[] = "background: rgba(174, 255, 135, 0.53);";
	$nicknameTitle .= " это автор топика";
} else if ($comment->user_id == $topic->blog->user_id) {
	// это комментарий админа блога
	$nicknameStyles[] = "background: rgba(135, 255, 250, 0.53);";
	$nicknameTitle .= " это администратор блога";
}

$nicknameStyles = implode("", $nicknameStyles);
?>

<div class="comment @if($comment->depth) comment_answer @endif" id="comment_{{$comment->id}}" style="margin-left:{{ $comment->depth*35 }}px;">
	@if($comment->depth)
	<div class="comment-arrow"></div>
	@endif

	<div class="comment-ava">
		
		<a href="{{ route("showUserProfile", $comment->user->username) }}">
			@if ($comment->user->avatar)
			{{ $comment->user->avatar->getHtmlImage($comment->user->avatar->getUrl(32,32,["crop"]), $comment->user->avatar->title, []) }}
			@else
				<img src="/app/frontend/images/avatar_male_48x48.png" alt="" style="width:32px;height:32px;">
			@endif
		</a>
	</div>

	<div class="comment-heading">
		<div><a title="{{$nicknameTitle}}" data-toggle="tooltip" style="{{ $nicknameStyles }}" href="{{ route("showUserProfile", $comment->user->username) }}">{{ $comment->user->username }}</a> &mdash; <span class="text-muted">{{ Date::parse($comment->created_at)->diffForHumans() }}</span> <a href="#comment_{{$comment->id}}">#</a></div>

		<div style="font-size:11px;margin-top:3px;">
			<span class="group-{{ $comment->user->group->name }}">{{ $comment->user->group->title }}</span>
			@if ($curUser)
				<span style="font-size: 9px;position: relative;top: -1px;margin: 0 4px;"><i class="fa fa-dot-circle-o"></i></span> <a href="javascript:void(0);" class="js-answer" data-id="{{{ $comment->id }}}">Ответить</a>
				@if ($comment->allowUpSpamLevel($curUser) && (!$comment->spam OR ($comment->spam && $comment->allowSetSpam($curUser))))
					<span style="font-size: 9px;position: relative;top: -1px;margin: 0 4px;"><i class="fa fa-dot-circle-o"></i></span> 

					<a href="javascript:void(0);" onclick="return Space.Comment.upSpamLevel({{$comment->id}}, $(this));">
						@if ($comment->allowSetSpam($curUser))
							@if ($comment->spam)
								Восстановить
							@else
								Скрыть
							@endif
						@else
							Это спам
						@endif
					</a>
				@endif
			@endif
		</div>

		<div class="comment-rating">
			@if ($comment->ratingAllowUseByUser($curUser))
				<a href="javascript:void(0);" onclick="return Space.Rating.down(_VALUES['url.ajaxRatingCommentDown'], {{$comment->id}}, $(this), $(this).parent().find('.comment-rating-val'));" class="comment-rating-down {{($comment->ratingObjectByUser($curUser) and !$comment->ratingObjectByUser($curUser)->direction)?'comment-rating-active':'comment-rating-notactive'}}"><i class="fa fa-thumbs-o-down"></i></a>
			@endif

			<span class="comment-rating-val comment-rating-val_{{ $comment->rating > 0 ? "green" : ($comment->rating < 0 ? "red" : "gray") }}">{{ $comment->rating }}</span>

			@if ($comment->ratingAllowUseByUser($curUser))
				<a href="javascript:void(0);" onclick="return Space.Rating.up(_VALUES['url.ajaxRatingCommentUp'], {{$comment->id}}, $(this), $(this).parent().find('.comment-rating-val'));" class="comment-rating-up {{($comment->ratingObjectByUser($curUser) and $comment->ratingObjectByUser($curUser)->direction)?'comment-rating-active':'comment-rating-notactive'}}"><i class="fa fa-thumbs-o-up"></i></a>
			@endif
		</div>
	</div>

	<div class="comment-message" id="comment_message_{{$comment->id}}">
		@if ($comment->spam)
			<div style="color: gray;">Комментарий удален</div>
		@else
			{{ $comment->message }}
		@endif
	</div>

	<div id="comment_{{$comment->id}}_answer_form"></div>
</div>

<div id="comment_{{$comment->id}}_answers"></div>