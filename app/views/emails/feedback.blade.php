<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Обратная связь: {{ $feedback->theme }}</h2>

		<div>
			@if ($user)
				<b>От пользователя:</b> <a href="{{ URL::route("showUserProfile", [$user->username], true)}}">{{ $user->username}}</a> <br/>
			@endif
			<b>Дата:</b> {{ Date::parse($feedback->created_at)->format("j F Y H:i") }}<br/>
			<b>E-mail:</b> {{ $feedback->email }}
		</div>

		<div>
			<br/>
			<b>Сообщение:</b>
			<br/>
			{{ $feedback->message }}
		</div>

	</body>
</html>
