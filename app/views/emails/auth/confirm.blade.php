<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Подтверждение e-mail адреса, {{ $username }}</h2>

		<div>
			<b>Используйте эту ссылку для подтверждения своего e-mail адреса:</b> <a href="{{ $confirmUrl }}">{{ $confirmUrl }}</a>
		</div>
	</body>
</html>