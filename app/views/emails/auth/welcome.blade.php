<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Добро пожаловать, {{ $username }}!</h2>

		<div>
			<b>Используйте эту ссылку для подтверждения своего e-mail адреса:</b> <a href="{{ $confirmUrl }}">{{ $confirmUrl }}</a><br/><br/>
		</div>

		<div>
			<b>Настроить свой профиль можно на этой странице:</b> <a href="{{ URL::to('settings') }}">{{ URL::to('settings') }}</a><br/><br/>
		</div>

		<div>
			<b>Спасибо за регистрацию! Публикуйте свои посты, делитесь впечатлениями от игры :)</b>
		</div>
	</body>
</html>