<!DOCTYPE html>
<html lang="ru">
	<head>
		<meta charset="utf-8">
	</head>
	<body>
		<h2>Сброс пароля</h2>

		<div>
			Для сброса пароля пройдите по этой ссылке: {{ URL::to('password/reset', array($token)) }}.<br/>
			Ссылка действительна в течение {{ Config::get('auth.reminder.expire', 60) }} минут.
		</div>
	</body>
</html>
