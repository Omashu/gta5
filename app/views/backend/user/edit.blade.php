@extends("backend.particles.layout")

@section("content")

{{ Form::open(["files" => true]) }}
<div class="panel panel-primary">
	<div class="panel-heading">
		{{ $user->username }}
	</div>
	<div class="panel-body">
		<div class="form-group">
			{{ Form::label("username", "Логин") }}:
			{{ Form::text("username", Input::get("username", $user->username), ["class" => "form-control"]) }}
		</div>
		<div class="form-group">
			{{ Form::label("email", "E-mail") }}:
			{{ Form::text("email", Input::get("email", $user->email), ["class" => "form-control"]) }}
		</div>
		<div class="form-group">
			{{ Form::label("password", "Пароль") }}:
			{{ Form::text("password", null, ["class" => "form-control"]) }}
			<div class="help-block">* не заполнять, если не хотите изменить пароль</div>
		</div>
		<div class="form-group">
			{{ Form::label("group_id", "Группа") }}:
			{{ Form::select("group_id", $groups, Input::get("group_id", $user->group_id), ["class" => "form-control"]) }}
		</div>
		<hr>
		<div class="form-group">
			{{ Form::label("first_name", "Имя") }}:
			{{ Form::text("first_name", Input::get("first_name", $user->first_name), ["class" => "form-control"]) }}
		</div>
		<div class="form-group">
			{{ Form::label("last_name", "Фамилия") }}:
			{{ Form::text("last_name", Input::get("last_name", $user->last_name), ["class" => "form-control"]) }}
		</div>
		<div class="form-group">
			{{ Form::label("about_myself", "О себе") }}:
			{{ Form::textarea("about_myself", Input::get("about_myself", $user->about_myself), ["class" => "form-control"]) }}
		</div>
		<div class="form-group">
			{{ Form::label("avatar", "Аватар") }}:
			{{ Form::file("avatar", ["class" => "form-control"]) }}
			@if ($user->avatar_id)
				<div class="media">
					{{ $user->avatar->getHtmlLink($user->avatar->getUrl(50, 50), $user->avatar->getHtmlImage($user->avatar->getUrl(50, 50)), ["class" => "media-left", "target" => "_blank"]) }}
					<div class="media-body">
						<h4 class="media-heading">{{ $user->avatar->title }}</h4>

					</div>
				</div>
			@endif
		</div>
		<hr>
		<div class="panel panel-danger">
			<div class="panel-heading">Запрет на публикации</div>
			<div class="panel-body">
				<div class="checkbox">
					<label>
						{{ Form::checkbox("write_only", false, !$user->write_only) }} Только чтение
					</label>
				</div>
				<div class="form-group">
					{{ Form::label("read_only_to", "По") }}:
					{{ Form::text("read_only_to", Input::get("read_only_to"), ["class" => "form-control js-datepicker", "data-date-format" => "yyyy-mm-dd", "placeholder" => "год-месяц-день час:минута"]) }}
					<div class="help-block">* оставьте поле пустым, если хотите, чтобы запрет действовал всегда</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-footer">
		{{ Form::submit("Сохранить", ["class" => "btn btn-primary"]) }}
	</div>
</div>
{{ Form::close() }}

@stop