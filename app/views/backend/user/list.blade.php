@extends("backend.particles.layout")

@section("sidebarBefore")
	@include("backend.user.nav")
@stop

@section("content")

{{ Form::open(["route" => "BackendUserShowList", "method" => "get"])}}

<div class="panel panel-default panel-filter">
	<div class="panel-heading">
		Фильтры
		{{ Form::text("per_page", $fPerPage, ["class" => "form-control pull-right input-sm"])}}
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-lg-4 panel-filter-col">
				{{ Form::label("group_id", "Группа:") }}
				{{ Form::select("group_id", $fGroups, $fGroupId, ["class" => "form-control"]) }}
			</div>
			<div class="col-lg-4 panel-filter-col">
				{{ Form::label("by", "Сортировать по:") }}
				{{ Form::select("by", $fSortColumns, $fSortColumn, ["class" => "form-control"]) }}
			</div>
			<div class="col-lg-4 panel-filter-col">
				{{ Form::label("way", "Направление сортировки:") }}
				{{ Form::select("way", $fSortWays, $fSortWay, ["class" => "form-control"]) }}
			</div>
		</div>
		<hr>
		<div class="form-inline">
			<div class="checkbox">
				<label for="write_only">
					{{ Form::checkbox("write_only", true, $fWriteOnly, ["id" => "write_only"]) }} Write-only статус
				</label>
			</div>

			<div class="checkbox">
				<label for="read_only">
					{{ Form::checkbox("read_only", true, $fReadOnly, ["id" => "read_only"]) }} Read-only статус
				</label>
			</div>

			<div class="checkbox">
				<label for="show_avatar">
					{{ Form::checkbox("show_avatar", true, $fShowAvatar, ["id" => "show_avatar"]) }} Показывать аватары
				</label>
			</div>

		</div>
	</div>
</div>

{{ Form::close() }}

{{ $users->links() }}

<div class="panel panel-primary">
	<div class="panel-heading">
		Пользователи <span class="badge pull-right">по фильтру: {{ $users->count() }}</span>
	</div>
	<div class="panel-body">
		<table class="table table-striped table-hover" style="margin-bottom:0;">
			<thead>
				<tr>
					<th><abbr title="Идентификатор">#</abbr></th>
					@if ($fShowAvatar)
					<th>Аватар</th>
					@endif
					<th>Логин</th>
					<th>E-mail</th>
					<th>Группа</th>
					<th>Последний вход</th>
					<th>Подтвержден</th>
					<th>Read-only</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
				<tr>
					<td>
						<span class="text-muted">{{ $user->id }}</span>
					</td>
					@if ($fShowAvatar)
					<td>Аватар</td>
					@endif
					<td>
						<a href="{{ route('BackendUserEdit', $user->id) }}">
						@if ($curUser->id == $user->id)
							<abbr title="Это вы">{{ $user->username }}</abbr>
						@else
							{{ $user->username }}
						@endif
						</a>
					</td>
					<td>{{ $user->email }}</td>
					<td>{{ $user->group->title }}</td>
					<td>{{ $user->updated_at }}</td>
					<td>{{ $user->confirmed ? '<abbr title="'.($user->confirmation_at).'"><span class="text-success" style="font-weight:bold;">Да</span></abbr>' : '<span class="text-danger" style="font-weight:bold;">Нет</span>' }}</td>
					<td>
						{{ $user->write_only 
							? '<span class="text-success" style="font-weight:bold;">Нет</span>' 
							: ('<abbr title="'.($user->read_only_from ? 'По: '.$user->read_only_to : 'Навсегда').'"><span class="text-danger" style="font-weight:bold;">Да</span></abbr>')}}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

{{ $users->links() }}

@stop