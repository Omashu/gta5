@extends("backend.particles.layout")

@section("content")

<div class="row">
	<div class="col-lg-6 col-l">
		<div class="panel panel-default">
			<div class="panel-heading">График регистраций (последние 30 дней)</div>
			<div class="panel-body">
				<div id="graph_new_users" style="height:200px;"></div>
			</div>
		</div>
	</div>
	<div class="col-lg-6 col-r">
		<div class="panel panel-default">
			<div class="panel-heading">Последние пользователи</div>
			<div class="panel-body">
				<div class="media">
				<a class="media-left" href="#">
				<img src="..." alt="...">
				</a>
				<div class="media-body">
				<h4 class="media-heading">Media heading</h4>
				...
				</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	new Dashboard.IndexGraphs({
		newUsers : '#graph_new_users'
	});
</script>

@stop