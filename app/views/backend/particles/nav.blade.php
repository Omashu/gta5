<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-3">
				<div class="navbar-header">
					<a class="navbar-brand" href="{{ url(Config::get("backend.prefix")) }}">Vobler Admin</a>
				</div>
			</div>
			<div class="col-lg-4">
				<form class="navbar-form navbar-left" role="search">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Запрос">
					</div>
					<button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Искать</button>
				</form>
			</div>
			<div class="col-lg-5">
				<ul class="nav navbar-nav navbar-right">
					<li class="dropdown">
						<a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ $curUser->username }} <span class="caret"></span></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li class="divider"></li>
							<li><a href="#">Separated link</a></li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
</nav>