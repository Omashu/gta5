<!DOCTYPE html>
<html lang="ru_RU">
<head>
	<meta charset="UTF-8">

	{{ Front::getHtml() }}
</head>
<body>
	
	@include("backend.particles.nav")	

	<div class="row" style="padding:0 20px;">
		<div class="col-lg-2 col-l">
			
			@yield("sidebarBefore")

			<div class="panel panel-primary panel-nav">
				<div class="panel-heading">Навигация</div>
				<div class="panel-body">
					<div class="list-group">
						<a href="{{ route("BackendUserShowList") }}" class="list-group-item">Пользователи <span class="badge">{{ User::count() }}</span></a>
						<a href="{{ route("BackendGroupShowList") }}" class="list-group-item">Группы <span class="badge">{{ Group::count() }}</span></a>
						<a href="{{ route("BackendBlogShowList") }}" class="list-group-item">Блоги <span class="badge">{{ Blog::count() }}</span></a>
						<a href="#" class="list-group-item">Воблеры <span class="badge">0</span></a>
						<a href="#" class="list-group-item">Изображения <span class="badge">0</span></a>
					</div>
				</div>
			</div>

			@yield("sidebarAfter")
		</div>
		<div class="col-lg-10 col-r">
			<div>{{ Message::getView() }}</div>
			@yield("content")
		</div>
	</div>

</body>
</html>