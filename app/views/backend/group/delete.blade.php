@extends("backend.particles.layout")

@section("sidebarBefore")
	@include("backend.group.navPersonal", ["active"=>"delete"])
@stop

@section("content")

{{ Form::open() }}
<div class="panel panel-danger">
	<div class="panel-heading">
		Удаление группы: {{ $group->title }}
	</div>
	<div class="panel-body">
		<div class="form-group">
			{{ Form::label("id", "В какую группу перенести пользователей") }}:
			{{ Form::select("id", $groupTransfList, Config::get("app.auth.registerGroupId"), ["class" => "form-control"]) }}
		</div>
	</div>
	<div class="panel-footer">
		{{ Form::submit("Подтвердить удаление", ["class" => "btn btn-danger"]) }}
		<a href="{{ route('BackendGroupEdit', $group->id) }}" class="btn btn-default">Отменить</a>
	</div>
</div>
{{ Form::close() }}

@stop