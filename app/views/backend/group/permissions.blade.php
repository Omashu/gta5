@extends("backend.particles.layout")

@section("sidebarBefore")
	@include("backend.group.navPersonal", ["active"=>"permissions"])
@stop

@section("content")

{{ Form::open() }}
<div class="panel panel-primary">
	<div class="panel-heading">
		{{ $group->title }}
	</div>
	<div class="panel-body">
	
		<table class="table table-hover" style="margin-bottom:0;">
			<thead>
				<tr>
					<th></th>
					<th>Имя</th>
					<th>Название</th>
					<th>Описание</th>
				</tr>
			</thead>
			<tbody>
				@foreach($permissions as $permission)
					<tr style="cursor:pointer;" onclick="$(this).find('input').prop('checked', !!!$(this).find('input').prop('checked'));">
						<td>{{ Form::checkbox("permissions[]", $permission->id, $group->allow($permission->name)) }}</td>
						<td>{{ $permission->name }}</td>
						<td>{{ $permission->title }}</td>
						<td>{{ $permission->description }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>

	</div>
	<div class="panel-footer">
		{{ Form::submit("Сохранить", ["class" => "btn btn-primary"]) }}
	</div>
</div>
{{ Form::close() }}

@stop