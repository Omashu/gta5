<div class="panel panel-primary panel-nav">
	<div class="panel-body">
		<div class="list-group">
			<a href="{{ route('BackendGroupEdit', $group->id) }}" class="list-group-item @if($active=='edit')active @endif">Настроить группу</a>
			<a href="{{ route('BackendGroupDelete', $group->id) }}" class="list-group-item @if($active=='delete')active @endif">Удалить группу</a>
			<a href="{{ route('BackendGroupPermissions', $group->id) }}" class="list-group-item @if($active=='permissions')active @endif">Настроить права</a>
		</div>
	</div>
</div>