@extends("backend.particles.layout")

@section("content")

{{ Form::open() }}
<div class="panel panel-primary">
	<div class="panel-heading">
		Добавление группы
	</div>
	<div class="panel-body">
		<div class="form-group">
			{{ Form::label("name", "Уникальное имя (лат.)") }}:
			{{ Form::text("name", Input::get("name"), ["class" => "form-control"]) }}
			<div class="help-block">admin, user, moder, etc.</div>
		</div>
		<div class="form-group">
			{{ Form::label("title", "Название") }}:
			{{ Form::text("title", Input::get("title"), ["class" => "form-control"]) }}
			<div class="help-block">Админ, Пользователь, Модератор, etc.</div>
		</div>
		<div class="form-group">
			{{ Form::label("title", "Описание (необяз)") }}:
			{{ Form::textarea("description", Input::get("description"), ["class" => "form-control"]) }}
		</div>
	</div>
	<div class="panel-footer">
		{{ Form::submit("Добавить", ["class" => "btn btn-primary"]) }}
	</div>
</div>
{{ Form::close() }}

@stop