@extends("backend.particles.layout")

@section("sidebarBefore")
	@include("backend.group.nav")
@stop

@section("content")

<div class="panel panel-primary">
	<div class="panel-heading">
		Группы <span class="badge pull-right">{{ Group::count() }}</span>
	</div>
	<div class="panel-body">
		<table class="table table-striped table-hover" style="margin-bottom:0;">
			<thead>
				<tr>
					<th><abbr title="Идентификатор">#</abbr></th>
					<th>Uniq. name</th>
					<th>Название</th>
					<th>Кол-во пользователей</th>
					<th><abbr title="Доступ в админ-панель">Доступ в АП</abbr></th>
					<th>По умолчанию</th>
				</tr>
			</thead>
			<tbody>
				@foreach($groups as $group)
				<tr>
					<td>
						<span class="text-muted">{{ $group->id }}</span>
					</td>
					<td>
						{{ $group->name }}
					</td>
					<td><a href="{{ route("BackendGroupEdit", $group->id)}}" title="Редактировать">{{ $group->title }}</a></td>
					<td><a href="{{ route("BackendUserShowList", ["group_id"=>$group->id]) }}">{{ $group->users->count() }}</a></td>
					<td>{{ $group->allow("dashboard.index", '<span class="text-danger" style="font-weight:bold;">Да</span>', '<span class="text-success" style="font-weight:bold;">Нет</span>') }}</td>
					<td>{{ Config::get("app.auth.registerGroupId")==$group->id?"Да":"Нет" }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

@stop