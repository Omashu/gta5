@extends("backend.particles.layout")

@section("sidebarBefore")

@stop

@section("content")

<div class="panel panel-primary">
	<div class="panel-heading">
		Изображения <span class="badge pull-right">{{ $images->count() }}</span>
	</div>
	<div class="panel-body">
		<table class="table table-striped table-hover" style="margin-bottom:0;">
			<thead>
				<tr>
					<th><abbr title="Идентификатор">#</abbr></th>
					<th>Slug</th>
					<th>Название</th>
					<th>Путь</th>
					<th>Размер</th>
					<th>Загрузил</th>
				</tr>
			</thead>
			<tbody>
				@foreach($images as $image)
				<tr>
					<td>
						<span class="text-muted">{{ $image->id }}</span>
					</td>
					<td>
						{{ $image->slug }}
					</td>
					<td>
						<a href="{{ route("BackendImageEdit", $image->id)}}" title="Редактировать">{{ $image->title }}</a>
					</td>
					<td>
						{{ $image->getRelPath() }}
					</td>
					<td>
					 {{ $image->width}}x{{$image->height}}	<code>{{ formatBytes($image->bytes) }}</code>
					</td>
					<td>
						<a href="{{ route('BackendUserEdit', $image->user->id) }}">
						@if ($curUser->id == $image->user->id)
							<abbr title="Это вы">{{ $image->user->username }}</abbr>
						@else
							{{ $image->user->username }}
						@endif
						</a>
						 : <small>{{$image->user->group->title}}</small>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

@stop