@extends("backend.particles.layout")

@section("content")

<div class="panel panel-primary">
	<div class="panel-heading">Удаление всех миниатюр</div>
	<div class="panel-body">
		<div>
			<b>Максимально возможное число миниатюр:</b> {{ $maxPossibleThumbs ?: "нет ограничения"}}
		</div>
		<div>
			Миниатюры создаются на лету, удалять их есть смысл если Вы изменили шаблоны, а новые миниатюры не создаются по причине превышения количественного ограничения.
		</div>

	</div>
	<div class="panel-footer">
		{{ Form::open() }}
		{{ Form::hidden("action", "delete_thumbs") }}
		{{ Form::submit("Удалить миниатюры", ["class" => "btn btn-primary"]) }}
		{{ Form::close() }}
	</div>
</div>

@stop