@extends("backend.particles.layout")

@section("sidebarBefore")
@stop

@section("content")

<div class="panel panel-default">
	<div class="panel-heading">
		Информация
	</div>
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>Изображение</th>
					<th>Информация</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $image->getHtmlLink($image->getUrl(), $image->getHtmlImage($image->getUrl(200, 200))) }}</td>
					<td>
						<ul class="unstyled">
							<li><b>ID:</b> {{ $image->id }}</li>
							<li><b>Title:</b> {{ $image->title }}</li>
							<li><b>Dirty Title:</b> {{ $image->dirty_title }}</li>
							<li><b>Slug:</b> {{ $image->slug }}</li>
							<li><b>User Id:</b> {{ $image->user_id }} <a href="{{ route('BackendUserEdit', $image->user->id) }}">@if ($curUser->id == $image->user->id)<abbr title="Это вы">{{ $image->user->username }}</abbr>@else{{ $image->user->username }}@endif</a> <small>({{$image->user->group->title}})</small></li>
							<li><b>Filename:</b> {{ $image->filename }}</li>
							<li><b>Bytes:</b> {{ $image->bytes }} <small>({{ formatBytes($image->bytes) }})</small></li>
							<li><b>Width:</b> {{ $image->width }}</li>
							<li><b>Height:</b> {{ $image->height }}</li>
							<li><b>Format:</b> {{ $image->format }}</li>
							<li><b>Mime Type:</b> {{ $image->mime_type }}</li>
							<li><b>Path:</b> {{ $image->path }}</li>
							<li><b>Created at:</b> {{ $image->created_at }}</li>
							<li><b>Updated at:</b> {{ $image->updated_at }}</li>
						</ul>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

{{ Form::open() }}
<div class="panel panel-primary">
	<div class="panel-heading">
		{{ $image->title }}
	</div>
	<div class="panel-body">
		<div class="form-group">
			{{ Form::label("title", "Название") }}:
			{{ Form::text("title", Input::get("title", $image->title), ["class" => "form-control"]) }}
		</div>
		<div class="form-group">
			{{ Form::label("title", "Описание") }}:
			{{ Form::textarea("description", Input::get("description", $image->description), ["class" => "form-control"]) }}
		</div>
	</div>
	<div class="panel-footer">
		{{ Form::submit("Сохранить", ["class" => "btn btn-primary"]) }}
	</div>
</div>
{{ Form::close() }}


<div class="panel panel-default">
	<div class="panel-heading">Привязки к блогам</div>
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>ID блога:</th>
					<th>Название</th>
					<th>Администратор</th>
				</tr>
			</thead>
			<tbody>
				@forelse($image->blogs()->with("user", "user.group")->get() as $blog)
				<tr>
					<td>{{ $blog->getKey() }}</td>
					<td>{{ $blog->title }}</td>
					<td><a href="{{ route('BackendUserEdit', $blog->user->id) }}">@if ($curUser->id == $blog->user->id)<abbr title="Это вы">{{ $blog->user->username }}</abbr>@else{{ $blog->user->username }}@endif</a> <small>({{$blog->user->group->title}})</small></td>
				</tr>
				@empty
				<tr>
					<td colspan="3">Привязок к блогам нет</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Привязки к топикам</div>
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>ID топика:</th>
					<th>Название</th>
					<th>Автор топика</th>
				</tr>
			</thead>
			<tbody>
				@forelse($image->blogTopics()->with("user", "user.group")->get() as $topic)
				<tr>
					<td>{{ $topic->getKey() }}</td>
					<td>{{ $topic->title }}</td>
					<td><a href="{{ route('BackendUserEdit', $topic->user->id) }}">@if ($curUser->id == $topic->user->id)<abbr title="Это вы">{{ $topic->user->username }}</abbr>@else{{ $topic->user->username }}@endif</a> <small>({{$topic->user->group->title}})</small></td>
				</tr>
				@empty
				<tr>
					<td colspan="3">Привязок к топикам нет</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>

<div class="panel panel-default">
	<div class="panel-heading">Привязки к пользователям</div>
	<div class="panel-body">
		<table class="table">
			<thead>
				<tr>
					<th>ID пользователя:</th>
					<th>Никнейм</th>
					<th>Роль</th>
				</tr>
			</thead>
			<tbody>
				@forelse($image->users()->with("group")->get() as $user)
				<tr>
					<td>{{ $user->getKey() }}</td>
					<td><a href="{{ route('BackendUserEdit', $user->id) }}">@if ($curUser->id == $user->id)<abbr title="Это вы">{{ $user->username }}</abbr>@else{{ $user->username }}@endif</a> <small>({{$user->group->title}})</small></td>
					<td>
						{{ $user->avatar_id == $image->id ? "Аватар" : "Не известно" }}
					</td>
				</tr>
				@empty
				<tr>
					<td colspan="3">Привязок к топикам нет</td>
				</tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>

@stop