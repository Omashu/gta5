@extends("backend.particles.layout")

@section("content")

{{ Form::open() }}
<div class="panel panel-primary">
	<div class="panel-heading">
		Добавление блога
	</div>
	<div class="panel-body">
		<div class="form-group">
			{{ Form::label("title", "Название") }}:
			{{ Form::text("title", Input::get("title"), ["class" => "form-control"]) }}
		</div>
		<div class="form-group">
			{{ Form::label("title", "Описание (необяз)") }}:
			{{ Form::textarea("description", Input::get("description"), ["class" => "form-control"]) }}
		</div>
	</div>
	<div class="panel-footer">
		{{ Form::submit("Добавить", ["class" => "btn btn-primary"]) }}
	</div>
</div>
{{ Form::close() }}

@stop