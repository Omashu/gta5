@extends("backend.particles.layout")

@section("sidebarBefore")
<div class="panel panel-primary panel-nav">
	<div class="panel-body">
		<div class="list-group">
			<a href="{{ route("BackendBlogAdd") }}" class="list-group-item"><i class="fa fa-plus"></i> Добавить блог</a>
		</div>
	</div>
</div>
@stop

@section("content")

{{ Form::open(["route" => "BackendBlogShowList", "method" => "get"])}}

<div class="panel panel-default panel-filter">
	<div class="panel-heading">
		Фильтры
		{{ Form::text("per_page", $fPerPage, ["class" => "form-control pull-right input-sm"])}}
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-lg-6 panel-filter-col">
				{{ Form::label("by", "Сортировать по:") }}
				{{ Form::select("by", $fSortColumns, $fSortColumn, ["class" => "form-control"]) }}
			</div>
			<div class="col-lg-6 panel-filter-col">
				{{ Form::label("way", "Направление сортировки:") }}
				{{ Form::select("way", $fSortWays, $fSortWay, ["class" => "form-control"]) }}
			</div>
		</div>
	</div>
</div>

{{ Form::close() }}

{{ $blogs->links() }}

<div class="panel panel-primary">
	<div class="panel-heading">
		Блоги <span class="badge pull-right">по фильтру: {{ $blogs->count() }}</span>
	</div>
	<div class="panel-body">
		<table class="table table-striped table-hover" style="margin-bottom:0;">
			<thead>
				<tr>
					<th><abbr title="Идентификатор">#</abbr></th>
					<th>Название</th>
					<th>Slug</th>
					<th>Создал</th>
					<th>Кол-во топиков</th>
				</tr>
			</thead>
			<tbody>
				@foreach($blogs as $blog)
				<tr>
					<td>
						<span class="text-muted">{{ $blog->id }}</span>
					</td>
					<td>
						{{ $blog->title }}
					</td>
					<td>{{ $blog->slug }}</td>
					<td><a href="#">{{ $blog->user->username }}</a></td>
					<td><a href="#">{{ $blog->topics()->count() }}</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

{{ $blogs->links() }}

@stop