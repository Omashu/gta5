<?php namespace Frontend;

use View;

class HomeController extends BaseController {

	public function showIndex()
	{
		// Front::title(Lang::get("frontend/front.home.index"));

		$this->layout = View::make("frontend/home.index");
	}
}
