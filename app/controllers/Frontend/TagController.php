<?php namespace Frontend;

use Front;
use App;
use View;

class TagController extends BaseController {

	// BlogTopics
	public function showBlogTopics($slug)
	{
		$tag = \Conner\Tagging\Tag::where("slug", $slug)->first();
		if (!$tag) App::abort(404);

		$topics = \BlogTopic::withAnyTag($tag->name)->whereHas("blog", function($q)
		{
			$q->where("is_draft", false);
		})->orderBy("created_at", "desc")->paginate(10);

		Front::title($tag->name);
		Front::description("Топиков найдено: " . $topics->count());
		Front::description($tag->name);
		Front::keywords($tag->name);

		$this->layout = View::make("frontend/tag.blogTopics",
		[
			"tag" => $tag,
			"topics" => $topics,
		]);
	}
}
