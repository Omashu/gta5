<?php namespace Frontend;

use View, Front, Lang;
use User, App, BlogTopic, Blog;
use Auth;

class UserController extends BaseController {

	// подписки текущего авторизованного юзера
	public function showSubs()
	{
		Front::title("Мои подписки");
		$subs = Auth::user()->subscribes()->with("subscribeable")->paginate(10);
		$this->layout = View::make("frontend/user.subs", ["subs" => $subs]);
	}

	// лента ! топиков blogTopics
	public function showStream()
	{
		Front::title("Моя лента");

		$topics = BlogTopic::orderBy("created_at", "desc")
			->with("user", "user.group", "blog")
			->whereHas("blog", function($q)
			{
				$q->where("is_draft", false);
				$q->whereRaw("(SELECT COUNT(id) FROM subscribes WHERE user_id = ".Auth::id()." AND subscribeable_type = 'Blog' AND subscribeable_id = blog_topics.blog_id)");
			})
			->paginate(10);

		Auth::user()->subscribes()->where("subscribeable_type", "Blog")
			->where("new", ">", 0)
			->update(["new" => 0]);

		$this->layout = View::make("frontend/user.stream_blogTopics")->with("topics", $topics);
	}

	public function showProfile($slug)
	{
		$user = User::where("username", $slug)->first();
		if (!$user) App::abort(404);
		Front::title($user->username);
		$this->layout = View::make("frontend/user.profile", ["user" => $user]);
	}

	public function showProfileSettings()
	{
		Front::title("Настройка профиля");
		$this->layout = View::make("frontend/user.settings");
	}

	public function postProfileSettings()
	{
		$values = \Input::only(
			[
				"email",
				"first_name",
				"last_name",
				"sex",
				"about_myself",
				"password",
				"password_confirmation"
			]);

		$values["username"] = Auth::user()->username;
		$values["group_id"] = Auth::user()->group_id;

		$validator = \Validator::make($values, User::getRules("update", ["id" => Auth::id()]));
		if ($validator->fails())
		{
			\Message::danger($validator);
			return redirectBackOrHome()->withInput($values);
		}

		\Message::success("Данные обновлены");

		if (\Input::get("last_password"))
		{
			// меняем пароль
			if (Auth::attempt(['id' => Auth::id(), 'password' => \Input::get("last_password")]))
			{
				Auth::user()->password = \Hash::make($values["password"]);
			} else
			{
				\Message::danger("Пароль не был изменен: Неверный старый пароль");
			}
		}

		// открепление старого аватара
		if ((bool) \Input::get("remove_avatar", false))
			Auth::user()->avatar_id = null;

		// загрузка нового аватара
		$avatar = \Input::file("avatar");

		if ($avatar) {
			$image = \Image::upload($avatar);

			if (!$image) {
				\Message::danger(\Image::getValidator());
			} else {
				Auth::user()->avatar_id = $image->id;
			}
		}

		Auth::user()->email = $values["email"];
		Auth::user()->update($values);
		Auth::user()->imagesSyncFromArray();

		return redirectBackOrHome();
	}

	public function showBlogs($slug)
	{
		$user = User::where("username", $slug)->first();
		if (!$user) App::abort(404);

		Front::title(Lang::get("frontend/front.user.blogs", ["username" => $user->username]));

		$blogDraft = null;
		if ((int) Auth::id() === (int) $user->id)
			$blogDraft = Blog::where("is_draft", true)->where("user_id", $user->id)->first();

		$blogs = Blog::orderBy("created_at", "desc")->where("user_id", $user->id)->where("is_draft", false)->paginate(20);
		$this->layout = View::make("frontend/user.blogs", ["user" => $user, "blogs" => $blogs, "blogDraft" => $blogDraft]);
	}

	public function showTopics($slug)
	{
		$user = User::where("username", "=", $slug)->first();
		if (!$user) App::abort(404);

		Front::title(Lang::get("frontend/front.user.topics", ["username" => $user->username]));

		$topics = BlogTopic::where("user_id", $user->id);

		// если кто-то чужой смотрит профиль, скроем черновые топики
		if ((int) Auth::id() !== (int) $user->id)
		{
			$topics->whereHas("blog", function($q)
			{
				$q->where("is_draft", false);
			});
		}

		$topics = $topics->orderBy("created_at", "desc")->paginate(10);
		$this->layout = View::make("frontend/user.topics", ["user" => $user, "topics" => $topics]);
	}

	public function showImages($slug)
	{
		$user = User::where("username", "=", $slug)->first();
		if (!$user) App::abort(404);

		Front::title(Lang::get("frontend/front.user.images", ["username" => $user->username]));

		$images = \Image::where("user_id", $user->id)->orderBy("created_at", "desc")
			->where("block", false)->paginate(25);

		$sumBytes = \Image::where("user_id", $user->id)->where("block", false)->sum("bytes");
		$this->layout = View::make("frontend/user.images", ["user" => $user, "images" => $images, "sumBytes" => $sumBytes]);
	}
}
