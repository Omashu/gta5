<?php namespace Frontend;

use Front;
use View;
use Config;
use Auth;
use Session;

class BaseController extends \BaseController {

	public function __construct()
	{
		Front::title(Config::get("frontend.title"));
		Front::description(Config::get("frontend.description"));
		Front::keywords(Config::get("frontend.keywords"));
		Front::scripts(Config::get("frontend.scripts", []));
		Front::styles(Config::get("frontend.styles", []));
		Front::favicon(asset("favicon.ico"));

		View::share("curUser", Auth::user());

		Front::js("token", Session::token());
		Front::js("userId", Auth::id());

		Front::js("url.ajaxUploadImage", route("ajaxUploadImage"));
		Front::js("url.ajaxUploadFile", route("ajaxUploadFile"));
		
		Front::js("url.ajaxImageDelete", route("ajaxImageDelete"));
		Front::js("url.ajaxFileDelete", route("ajaxFileDelete"));
		
		Front::js("url.ajaxCommentBlogTopic", route("ajaxCommentBlogTopic"));
		Front::js("url.ajaxSubscribeToggleBlog", route("ajaxSubscribeToggleBlog"));
		Front::js("url.ajaxBlogDelete", route("ajaxBlogDelete"));
		Front::js("url.ajaxBlogTopicDelete", route("ajaxBlogTopicDelete"));
		
		Front::js("url.ajaxRatingBlogTopicDown", route("ajaxRatingBlogTopicDown"));
		Front::js("url.ajaxRatingBlogTopicUp", route("ajaxRatingBlogTopicUp"));

		Front::js("url.ajaxRatingCommentDown", route("ajaxRatingCommentDown"));
		Front::js("url.ajaxRatingCommentUp", route("ajaxRatingCommentUp"));

		Front::js("url.ajaxCommentSpamUp", route("ajaxCommentSpamUp"));
		
		Front::js("url.ajaxPoll", route("ajaxPoll"));
		Front::js("url.ajaxPollResults", route("ajaxPollResults"));

		Front::js("url.ajaxTagComplete", route("ajaxTagComplete"));
		Front::js("url.ajaxUserComplete", route("ajaxUserComplete"));

		Front::js("url.ajaxBlogPermission", route("ajaxBlogPermission"));
		Front::js("url.ajaxBlogPermissionDelete", route("ajaxBlogPermissionDelete"));

		
		Front::js("url.home", route("home"));

		if (Auth::check()) \DB::table("users")->where("id", Auth::id())->update(["updated_at"=>\Date::now()]);
	}

}