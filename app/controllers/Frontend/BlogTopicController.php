<?php namespace Frontend;

use View, Front, Message, Redirect, Lang, Blog, Auth, Input, BlogTopic, Validator;
use App;
use Image;
use Request;
use Cache;

class BlogTopicController extends BaseController {
	public function showCreate()
	{
		Front::title(Lang::get("frontend/front.blog.topic.create"));

		// список блогов, созданных юзером, публикация в них
		$userBlogs = Blog::where("user_id", "=", Auth::user()->id)->where("is_draft", false)->get()->getAssoc();

		// получаем блоги, на которые подписаны и в которые можем опубликовать
		$userSubsBlogsAndWriteOnly = Blog::getUserSubsAndWriteOnly(Auth::user())->get()->getAssoc();

		$blogsAssoc = [
			"Мои блоги: " . (count($userBlogs) ?: "пусто") => $userBlogs,
			"Подписки на блоги: " . (count($userSubsBlogsAndWriteOnly)?: "пусто") => $userSubsBlogsAndWriteOnly];

		// вернем список не прикрепленных изображений
		$oImages = Image::userNonUsed(Auth::user())->get();

		// вернем не прикрепленные файлы юзера, пусть скрепляет, либо удаляет
		$oFiles = \FileModel::userNonUsed(Auth::user())->get();

		// вернем опросы
		$aPolls = \Poll::validateArrayAll(Input::old("_polls", []));

		$this->layout = View::make("frontend/blog.topic.create",
		[
			// заглушка
			"topic" => new BlogTopic(),

			// селектор блогов
			"blogsAssoc" => $blogsAssoc,

			// смежные данные
			"oImages" => $oImages,
			"oFiles" => $oFiles,
			"aPolls" => $aPolls,
		]);
	}

	public function postCreate()
	{
		$topicData = Input::only([
			"blog_id",
			"title",
			"description",
			"preview_id",

			"_image_ids",
			"_file_ids",
			"_tags",
			"_polls",
		]);

		$topicData["user_id"] = Auth::user()->id;

		// в черновики сохраним
		if (Input::get("_mode") === "draft")
			$topicData["blog_id"] = Blog::getUserDraft(Auth::user()->id)->id;

		$topicRules = BlogTopic::getRules("create");
		$topicValidator = Validator::make($topicData, $topicRules);

		if ($topicValidator->fails())
		{
			Message::danger($topicValidator);
			return redirectBackOrHome()->withInput($topicData);
		}

		// проверка, может ли юзер публиковать в этот блог
		$oBlog = Blog::findOrFail($topicData["blog_id"]);

		try
		{
			\Allow\Blog::newTopic(Auth::user(), $oBlog, true);
		} catch (\System\AllowException $e)
		{
			Message::danger($e->getMessage());
			return redirectBackOrHome()->withInput($topicData);
		}

		// окей, создаем топик
		$topic = new BlogTopic($topicData);
		$topic->user_id = $topicData["user_id"];
		$topic->blog_id = $topicData["blog_id"];
		$topic->description = $topicData["description"];
		$topic->preview_id = (int)$topicData["preview_id"]?:null;
		$topic->save();

		// уведомляем
		Message::success($topic->title . " успешно создан и опубликован в блог " .$topic->blog->title);

		// прикрепляем теги
		$topic->tag(tagNamesChecker($topicData["_tags"]));

		// синхронизируем прикрепленные изображения
		$topic->imagesSyncFromArray($topicData["_image_ids"]);

		// прикрепит все эти файлы к топику, файлы должны быть не прикрепленными
		$topic->filesSyncFromArray($topicData["_file_ids"]);

		// создаем опросы
		$topic->pollsSyncFromArray(Input::get("_polls", []));

		return Redirect::route("showTopic", $topic->slug);
	}

	public function showEdit($id)
	{
		$topic = BlogTopic::findOrFail($id);

		// редактировать может автор или пользователь с правами blogTopic.editAll
		if (!$topic->allowEdit(Auth::user()))
		{
			Message::danger("Недостаточно прав для редактирования топика");
			return Redirect::route("showTopic", $topic->slug);
		}

		// сгенерируем селектор выбора блога
		// если топик редактирует не автор, должны быть варианты публикации в свои блоги и блоги автора

		// список блогов, созданных юзером
		$userBlogs = Blog::where("user_id", "=", Auth::user()->id)->where("is_draft", false)->get()->getAssoc();

		// получаем блоги, на которые подписаны и в которые можем опубликовать
		$userSubsBlogsAndWriteOnly = Blog::getUserSubsAndWriteOnly(Auth::user())->get()->getAssoc();

		$blogsAssoc = [
			"Мои блоги: " . (count($userBlogs) ?: "пусто") => $userBlogs,
			"Подписки на блоги: " . (count($userSubsBlogsAndWriteOnly) ?: "пусто") => $userSubsBlogsAndWriteOnly];

		if ((int) Auth::id() !== (int) $topic->user_id)
		{
			// редактирует не автор, подставим в форму блоги автора топика
			$topicUser = \User::find($topic->user_id);

			$topicUserBlogs = Blog::where("user_id", "=", $topicUser->id)->where("is_draft", false)->get()->getAssoc();
			$topicUserSubsBlogsAndWriteOnly = Blog::getUserSubsAndWriteOnly($topicUser)->get()->getAssoc();

			$blogsAssoc["Блоги " . $topicUser->username .": " . (count($topicUserBlogs) ?: "пусто")] = $topicUserBlogs;
			$blogsAssoc["Подписки " . $topicUser->username .": " . (count($topicUserSubsBlogsAndWriteOnly) ?: "пусто")] = $topicUserSubsBlogsAndWriteOnly;
		} else {
			// если топик опубликован в тот блог, в который автор теперь не имеет доступа (напр. модератор туда запихнул топик), даем автору возможность обновлять топик в этом блоге
			$oBlog = Blog::findOrFail($topic->blog_id);

			if (!$oBlog->allowNewTopic(Auth::user()))
			{
				// топик в блоге, в который не может писать автор более, разрешим (действует до тех пор, пока кто-то не перенесет топик в другой блог)
				$blogsAssoc["Редакция"] = [$oBlog->id => $oBlog->title];
			}
		}

		$aPolls = \Poll::validateArrayAll(Input::old("_polls", []));
		if (!$aPolls) $aPolls = $topic->pollsConvertCurrentToArray();

		// вернем список загруженных, но не используемых изображений и изображения топика
		$oImages = Image::userNonUsed(Auth::user())->orWhereIn("id", $topic->images->modelKeys())->get();

		// вернем список файлов топика + список файлов загруженных, но не прикрепленных
		$oFiles = \FileModel::userNonUsed(Auth::user())->orWhereIn("id", $topic->files->modelKeys())->get();

		$this->layout = View::make("frontend/blog.topic.create",
			[
				"topic" => $topic,
				"blogsAssoc" => $blogsAssoc,
				"tagsString" => implode(", ", $topic->tagged->lists("tag_name")),

				"aPolls" => $aPolls,
				"oImages" => $oImages,
				"oFiles" => $oFiles,
			]);
	}

	public function postEdit($id)
	{
		$topic = BlogTopic::findOrFail($id);

		if (!$topic->allowEdit(Auth::user()))
		{
			Message::danger("Недостаточно прав для редактирования топика");
			return Redirect::route("showTopic", $topic->slug);
		}

		$topicUser = \User::findOrFail($topic->user_id);

		$topicData = Input::only([
			"blog_id",
			"title",
			"description",
			"preview_id",

			"_image_ids",
			"_file_ids",
			"_tags",
			"_polls",
		]);
		
		$topicData["user_id"] = $topic->user_id;

		// в черновики автору
		if (Input::get("_mode") === "draft")
			$topicData["blog_id"] = Blog::getUserDraft($topicUser->id)->id;

		$topicRules = BlogTopic::getRules("update");
		$topicValidator = Validator::make($topicData, $topicRules);

		if ($topicValidator->fails())
		{
			Message::danger($topicValidator);
			return redirectBackOrHome()->withInput($topicData);
		}

		/**
		 * Может если он подписан или это его блог
		 * Не автор (с правами редактирования чужого) может в свои блоги и подписки, в блоги и подписки автора
		 * Автор может обновить топик, в блоге к которому он раньше имел доступ, но более не имеет, например, админ блога повысил необходимый рейтинг для публикации
		 */

		// проверка, может ли юзер публиковать в этот блог
		$oBlog = Blog::findOrFail($topicData["blog_id"]);

		// в блог автора топика или в свой блог/подписку (для модеров/админов)
		// или в тот блог, в котором сейчас этот топик находится
		if ((int) $topic->blog_id !== (int) $oBlog->id AND !$oBlog->allowNewTopic($topicUser) AND !$oBlog->allowNewTopic(Auth::user()))
		{
			$topicData["blog_id"] = null;
			Message::danger(Lang::get("frontend/message.topicCreateBlogAccessDenied",
				[
					"title" => $oBlog->title,
				]));
			return redirectBackOrHome()->withInput($topicData);
		}

		$topic->preview_id = (int)$topicData["preview_id"]?:null;
		$topic->user_id = $topicData["user_id"];
		$topic->blog_id = $topicData["blog_id"];
		$topic->description = $topicData["description"];
		$topic->title = $topicData["title"];
		$topic->save();

		// открпеляем все теги
		if (!tagNamesChecker($topicData["_tags"])) $topic->untag();

		// цепляем новые теги
		$topic->retag(tagNamesChecker($topicData["_tags"]));

		// sync images
		$topic->imagesSyncFromArray($topicData["_image_ids"]);

		// прикрепит все эти файлы к топику, файлы должны быть не прикрепленными
		$topic->filesSyncFromArray($topicData["_file_ids"]);

		// создаем, обновляем, удаляем опросы
		$topic->pollsSyncFromArray(Input::get("_polls", []));

		Message::success("Топик ".$topic->title." успешно обновлен.");

		return Redirect::route("showTopicEdit", $topic->id);
	}

	public function show($slug)
	{
		$topic = BlogTopic::findBySlug($slug);
		if (!$topic) App::abort(404);

		// топик в черновиках, просмотр доступен только автору топика
		if ($topic->blog->is_draft AND (int) Auth::id() !== (int) $topic->user_id)
			App::abort(404);

		Front::title($topic->title);
		Front::description([$topic->blog->title, $topic->description]);
		Front::keywords([$topic->tagged->lists("tag_name"), $topic->blog->title, $topic->title]);

		$cacheName = "blogTopicVisitor_".$topic->id."_".Request::getClientIp();
		Cache::rememberForever($cacheName, function() use ($topic)
		{
			$topic->views++;
			$topic->save();
			return true;
		});

		$this->layout = View::make("frontend/blog.topic.show",
			[
				"topic" => $topic,
			]);
	}

	// свежее
	public function showNew()
	{
		Front::title("Свежее");
		Front::keywords("свежие публикации");
		Front::description("Свежие публикации GTA5");

		$topics = BlogTopic::with("blog", "user", "user.group")
			->orderBy("created_at", "desc")
			->whereHas("blog", function($q)
			{
				$q->where("is_draft", false);
			})->paginate(10);

		$this->layout = View::make("frontend/blog.topic.showNew",
			[
				"topics" => $topics,
			]);
	}

	// комментируемов
	public function showCommented($period=null)
	{
		Front::title("Обсуждаемое");
		Front::keywords("обсуждаемые публикации");
		Front::description("Обсуждаемые публикации GTA5");

		if (!$period) $period = "week";

		$topics = BlogTopic::whereHas("comments", function($q) use ($period)
			{
				$q->where("created_at", ">=", \Date::now()->parse("-1 ".$period));
			})
			->whereHas("blog", function($q)
			{
				$q->where("is_draft", false);
			})
			->with("blog", "user", "user.group")
			->orderByRaw("(SELECT COUNT(*) AS count FROM comments WHERE comments.commentable_id = blog_topics.id AND comments.commentable_type = 'BlogTopic') DESC")
			->remember(10)
			->paginate(10);

		$this->layout = View::make("frontend/blog.topic.showCommented",
			[
				"topics" => $topics,
				"period" => $period,
			]);
	}
}
