<?php namespace Frontend;

use Password, View, Front, Input, Redirect, Lang, Message, App;
use Validator;

class AuthRemindersController extends BaseController {

	/**
	 * Display the password reminder view.
	 *
	 * @return Response
	 */
	public function getRemind()
	{
		Front::title(Lang::get("frontend/front.remind"));
		return View::make('frontend/auth.remind');
	}

	/**
	 * Handle a POST request to remind a user of their password.
	 *
	 * @return Response
	 */
	public function postRemind()
	{
		if (!is_string(Input::get("ident")))
		{
			Message::danger("Неверный логин или e-mail адрес. Повторите ввод.");
			return Redirect::back();
		}

		$column = preg_match("/@/", Input::get("ident")) ? "email" : "username";

		switch ($response = Password::remind([$column => Input::get("ident")]))
		{
			case Password::INVALID_USER:
				Message::danger(Lang::get($response));
				return Redirect::back();

			case Password::REMINDER_SENT:
				Message::success(Lang::get($response));
				return Redirect::back();
		}
	}

	/**
	 * Display the password reset view for the given token.
	 *
	 * @param  string  $token
	 * @return Response
	 */
	public function getReset($token = null)
	{
		if (is_null($token)) App::abort(404);
		Front::title(Lang::get("frontend/front.reset"));

		return View::make('frontend/auth.reset')->with('token', $token);
	}

	/**
	 * Handle a POST request to reset a user's password.
	 *
	 * @return Response
	 */
	public function postReset()
	{
		if (!is_string(Input::get("ident")))
		{
			Message::danger("Неверный логин или e-mail адрес. Повторите ввод.");
			return Redirect::back();
		}

		$rules = array('captcha' => array('required', 'captcha'));
		$validator = Validator::make(Input::all(), $rules);

		if ($validator->fails())
		{
			Message::danger("Неверный код безопасности.");
			return Redirect::back();
		}

		$credentials = Input::only(
			'password', 'password_confirmation', 'token'
		);

		$column = preg_match("/@/", Input::get("ident")) ? "email" : "username";
		$credentials[$column] = Input::get("ident");

		$response = Password::reset($credentials, function($user, $password)
		{
			$user->password = \Hash::make($password);

			$user->save();
		});

		switch ($response)
		{
			case Password::INVALID_PASSWORD:
			case Password::INVALID_TOKEN:
			case Password::INVALID_USER:
				Message::danger(Lang::get($response));
				return Redirect::back();

			case Password::PASSWORD_RESET:
				Message::success("Установлен новый пароль. Попробуйте войти.");
				return Redirect::to("/login");
		}
	}

}
