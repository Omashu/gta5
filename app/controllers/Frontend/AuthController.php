<?php namespace Frontend;

use Front, Lang, View, Input, Auth, Message, Redirect;
use Config;
use Validator;
use User;
use Hash;
use Group;
use Blog;

class AuthController extends BaseController {
	public function getConfirm($code)
	{
		$user = User::where("confirmation_code", $code)->where("confirmed", false)->first();
		if (!$user) \App::abort(404);

		$user->confirmed = true;
		$user->confirmation_at = \Date::now();
		$user->save();

		Message::success("E-mail адрес успешно подвержден");
		return Redirect::home();
	}

	public function sendConfirmMail()
	{
		if (Auth::user()->confirmed) {
			Message::danger("E-mail адрес уже подтвержден");
			return redirectBackOrHome();
		}

		Auth::user()->sendConfirmMail();
		Message::success("Сообщение повторно отправлено на Ваш e-mail адрес");
		return redirectBackOrHome();
	}

	public function getLogin()
	{
		Front::title(Lang::get("frontend/front.login"));
		$this->layout = View::make("frontend/auth.login");
	}

	public function postLogin()
	{
		$ident = Input::get("ident");
		$bEmail = !!preg_match("/@/", $ident);

		$aAttempt = ["password" => Input::get("password")];
		$aAttempt[($bEmail ? "email" : "username")] = $ident;

		$bOk = Auth::attempt($aAttempt, true);
		if ($bOk)
		{
			Message::success(Lang::get("frontend/message.loginSuccess",
				[
					"username" => Auth::user()->username
				]));

			return Redirect::home();
		}

		Message::danger(Lang::get("frontend/message.loginDenied"));
		return redirectBackOrHome();
	}

	public function getRegister()
	{
		Front::title(Lang::get("frontend/front.register"));

		if (!Config::get("app.auth.registerEnabled"))
		{
			Message::danger(Lang::get("frontend/message.registerDisabled"));
			return Redirect::home();
		}

		$this->layout = View::make("frontend/auth.register");
	}

	public function postRegister()
	{
		if (!Config::get("app.auth.registerEnabled"))
		{
			Message::danger(Lang::get("frontend/message.registerDisabled"));
			return redirectBackOrHome();
		}

		$oGroup = Group::find((int) Config::get("app.auth.registerGroupId"));
		if (!$oGroup)
		{
			Message::danger(Lang::get("frontend/message.registerDefaultGroupIdIsNotDefined"));
			return redirectBackOrHome();
		}

		// создаем юзера
		$values = Input::only(["username", "email", "password", "password_confirmation"]);
		$values["group_id"] = $oGroup->id;

		$rules = User::getRules("create");
		$validator = Validator::make($values, $rules);

		if ($validator->fails())
		{
			Message::danger($validator);
			return redirectBackOrHome()->withInput($values);
		}

		$user = new User();
		$user->username = $values["username"];
		$user->email = $values["email"];
		$user->password = Hash::make($values["password"]);
		$user->group_id = $oGroup->id;
		$user->save();

		// авторизовываем
		Auth::login($user, true);

		// создадим юзеру блог по умолчанию, чтобы потом не было большого кол-ва запросов при удалении блога
		Blog::getUserDraft(Auth::user()->id)->id;

		return Redirect::home();
	}

}
