<?php namespace Frontend;

use View, Front, User, Config, Input;

class PeopleController extends BaseController {

	public function show()
	{
		Front::title("Участники");
		Front::keywords("Участники");
		Front::description("Участники");

		$type = Input::get("t", "last");
		$sex = Input::get("sex");

		if (!in_array($type, ["new","last"]))
			\App::abort(404);

		if ($sex !== null AND !in_array($sex, array_keys(Config::get("app.user.sex", []))))
			\App::abort(404);

		$users = User::with("avatar", "group");

		if ($type == "new") $users->orderBy("created_at", "desc");
		else $users->orderBy("updated_at", "desc");

		if ($sex) $users->where("sex", $sex);

		$users = $users->paginate(25);
		$this->layout = View::make("frontend/people.show", ["users" => $users]);
	}
}
