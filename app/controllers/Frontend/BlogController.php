<?php namespace Frontend;

use View;
use Front, Lang;
use Input, Blog, Image;
use Validator, Message, Redirect;
use Auth;
use DB;
use App;
use BlogTopic;
use Subscribe;

class BlogController extends BaseController {

	// блоги по рейтингу, страница /blogs
	public function showIndexPopular()
	{
		Front::title(Lang::get("frontend/front.blog.indexPopular"));

		$blogs = Blog::where("is_draft", false)
			// сумма рейтинга топиков блога, топик не удален
			->with("preview", "user", "user.group")
			->orderByRaw(DB::raw("(SELECT SUM(blog_topics.rating) FROM blog_topics WHERE blog_topics.blog_id = blogs.id  AND deleted_at IS NULL) DESC"))
			->paginate(25);

		$blogRatings = [];
		$blogIds = [];

		foreach ($blogs as $blog) {
			$blogIds[] = $blog->id;
			$blogRatings[$blog->id] = 0;
		}

		if ($blogIds) {
			$blogsRatingsQuery =  DB::table('blog_topics')->select(DB::raw("SUM(rating) AS summary"), "blog_id")->whereIn("blog_id", $blogIds)->groupBy("blog_id")->remember(15)->get();

			foreach ($blogsRatingsQuery as $blogRating) {
				$blogRatings[$blogRating->blog_id] = $blogRating->summary;
			}
		}

		$this->layout = View::make("frontend/blog.indexPopular",
		[
			"blogs" => $blogs,
			"blogRatings" => $blogRatings,
		]);
	}

	public function showCreate() {
		Front::title(Lang::get("frontend/front.blog.create"));
		$this->layout = View::make("frontend/blog.create", ["blog" => new Blog()]);
	}

	public function postCreate() {
		$values = Input::only(["title", "description"]);
		$values["user_id"] = Auth::id();

		$rules = Blog::getRules("create");
		$validator = Validator::make($values, $rules);

		if ($validator->fails())
		{
			Message::danger($validator);
			return Redirect::back()->withInput($values);
		}

		$blog = new Blog($values);
		$blog->user_id = Auth::id();

		// сохраняем для получения pk
		$blog->save();

		// загрузка и прикрепляем первью, если есть
		$preview = Input::file("preview");

		if ($preview) {
			$image = Image::upload($preview);

			if (!$image) {
				Message::danger(Image::getValidator());
				// отправим на редактирование
				return Redirect::route("showBlogEdit", [$blog->slug]);
			} else {
				// ассоциируем превью и делаем видимые связи
				$blog->preview_id = $image->id;
			}
		}

		// сохраняем еще раз
		$blog->save();

		// синхронизируем превью
		$blog->imagesSyncFromArray();

		Message::success("Блог ".$blog->title." создан можно публиковать топики");
		return Redirect::route("newTopic", ["blog_id" => $blog->getKey()]);
	}

	public function showEdit($slug)
	{
		$blog = Blog::findBySlug($slug);
		if (!$blog) App::abort(404);

		Front::title("Редактирование блога " . $blog->title);

		// бросит исключение, если нет прав, отловится в filters.php
		\Allow\Blog::edit(Auth::user(), $blog, true);

		$this->layout = View::make("frontend/blog.create",
		[
			"blog" => $blog,
		]);
	}

	public function postEdit($slug)
	{
		$blog = Blog::findBySlug($slug);
		if (!$blog) App::abort(404);

		// бросит исключение, если нет прав, отловится в filters.php
		\Allow\Blog::edit(Auth::user(), $blog, true);

		$values = Input::only(["title", "description"]);
		$values["user_id"] = $blog->user_id;

		$rules = Blog::getRules("update");
		$validator = Validator::make($values, $rules);

		if ($validator->fails())
		{
			Message::danger($validator);
			return redirectBackOrHome()->withInput($values);
		}

		$blogPreview = $blog->preview;

		// открепление старого
		if ((bool) Input::get("remove_preview", false))
			$blog->preview_id = null;

		// загрузка нового первью
		$preview = Input::file("preview");

		if ($preview) {
			$image = Image::upload($preview);

			if (!$image) {
				Message::danger(Image::getValidator());
			} else {
				$blog->preview_id = $image->id;
			}
		}

		$blog->title = $values["title"];
		$blog->description = $values["description"];
		$blog->save();

		$blog->imagesSyncFromArray();

		// удалим изображение, если нигде более оно не испольузется
		if ($blogPreview AND $blogPreview->getKey() != $blog->preview_id)
			$blogPreview->deleteIfNonUsed();

		Message::success("Информация блога обновлена");
		return Redirect::route("showBlogEdit", [$blog->slug]);
	}

	public function showEditPermissions($slug)
	{
		$blog = Blog::findBySlug($slug);
		if (!$blog) App::abort(404);

		Front::title("Привилегии " . $blog->title);

		// бросит исключение, если нет прав, отловится в filters.php
		\Allow\Blog::edit(Auth::user(), $blog, true);

		$this->layout = View::make("frontend/blog.editPermissions",
		[
			"blog" => $blog,
		]);
	}

	public function postEditPermissions($slug)
	{
		$blog = Blog::findBySlug($slug);
		if (!$blog) App::abort(404);

		// бросит исключение, если нет прав, отловится в filters.php
		\Allow\Blog::edit(Auth::user(), $blog, true);

		$blog->permission_public = (bool) Input::get("permission_public", false);
		$blog->permission_public_min_rating = (float) Input::get("permission_public_min_rating", 1.00);
		$blog->save();

		Message::success("Возможности подписчиков обновлены");
		return Redirect::back();
	}

	public function show($slug)
	{
		$blog = Blog::findBySlug($slug);
		if (!$blog) App::abort(404);

		Front::title($blog->title);
		Front::description([$blog->title, $blog->description]);
		Front::keywords([$blog->title, $blog->description]);

		if ($blog->is_draft) {
			if (!Auth::check() OR (int) Auth::id() !== (int) $blog->user_id) {
				Message::danger("Это черновой блог и Вам он не принадлежит...");
				return redirectBackOrHome();
			}
		}

		$topics = $blog->topics()
			->orderBy("created_at", "desc")
			->paginate(10);

		$this->layout = View::make("frontend/blog.show",
			[
				"blog" => $blog,
				"topics" => $topics,
			]);
	}
}