<?php namespace Frontend;

use View, Front;

class PollController extends BaseController {
	public function showNew()
	{
		// Получаем опросы
		$polls = \Poll::orderBy("created_at", "desc")->with("pollable", "user", "user.group")->paginate(50);

		Front::title("Опросы на сайте");
		Front::description($polls->lists("title"));
		Front::keywords($polls->lists("title"));

		$this->layout = View::make("frontend/poll.showNew",
		[
			"polls" => $polls,
		]);
	}

	public function show($slug)
	{
		$poll = \Poll::findBySlug($slug);
		if (!$poll) \App::abort(404);

		Front::title("Опрос: " . $poll->title);
		Front::description($poll->title);
		Front::keywords($poll->title);

		$graphDataCountSels = [];

		foreach ($poll->answers as $answer)
		{
			$graphDataCountSels[] = [
				"answer" => $answer->answer,
				"count" => $answer->sels()->count(),
			];
		}

		$this->layout = View::make("frontend/poll.show",
		[
			"poll" => $poll,
			"graphDataCountSels" => $graphDataCountSels,
		]);
	}
}
