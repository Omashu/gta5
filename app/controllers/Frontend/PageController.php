<?php namespace Frontend;

use Front;
class PageController extends BaseController {
	public function faq()
	{
		Front::title("FAQ");
		Front::keywords("FAQ, помощь, ответы на вопросы");
		Front::description("FAQ, помощь, ответы на вопросы");

		$this->layout = \View::make("frontend/page.faq");
	}

	public function feedback()
	{
		Front::title("Обратная связь");
		Front::keywords("Обратная связь");
		Front::description("Обратная связь");

		$this->layout = \View::make("frontend/page.feedback");
	}

	public function postFeedback()
	{
		$values = \Input::only(["email", "theme", "message", "captcha"]);
		$rules = \Feedback::getRules();
		$rules["captcha"] = ["required", "captcha"];
		$validator = \Validator::make($values, $rules);

		if ($validator->fails())
		{
			\Message::danger($validator);
			return redirectBackOrHome()->withInput($values);
		}

		$feedback = new \Feedback([
			"theme" => $values["theme"],
			"email" => $values["email"],
			"message" => $values["message"]]);

		$feedback->saveAndSend();

		\Message::success("Ваше сообщение отправлено.");
		return \Redirect::home();
	}
}
