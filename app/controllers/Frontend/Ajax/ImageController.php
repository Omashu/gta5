<?php namespace Frontend;

use Image, Input;

class AjaxImageController extends AjaxBaseController {

	// изображение удаляется полностью только если оно нигде больше не используется
	public function delete()
	{
		// только загрузивший изображение может его удалить
		$image = Image::where("user_id", \Auth::id())->where("id", Input::get("id"))->first();

		if (!$image)
		{
			return [
				"bOk" => false,
				"message" => "Неверный запрос",
			];
		}

		if (!$image->deleteIfNonUsed())
		{
			return [
				"bOk" => false,
				"message" => "Изображение используется в других материалах",
			];
		}

		return [
			"bOk" => true,
			"message" => "Изображение удалено",
		];
	}

	public function update()
	{
		// только загрузивший изображение может изменять его название, описание
		$image = Image::where("user_id", \Auth::id())->where("id", Input::get("id"))->first();

		if (!$image)
		{
			return [
				"bOk" => false,
				"message" => "Неверный запрос",
			];
		}

		$image->update(["title" => Input::get("title"), "description" => Input::get("description")]);

		return [
			"bOk" => true,
			"message" => "Изображение обновлено",
			"image" => [
				"id" => $image->id,
				"title" => $image->title,
				"description" => $image->description,
			],
		];
	}
}
