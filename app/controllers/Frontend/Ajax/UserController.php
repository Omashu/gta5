<?php namespace Frontend;

class AjaxUserController extends AjaxBaseController {

	public function getCompleteFromString()
	{
		$string = \Input::get("query");
		if (!is_string($string) OR mb_strlen($string) < 1)
			return ["query" => null, "suggestions" => []];

		$users = \User::where("username", "like", "%".$string."%")->get();
		$res = ["query" => $string, "suggestions" => []];

		foreach ($users as $user) {
			$res["suggestions"][] = $user->username;
		}

		return $res;
	}
}