<?php namespace Frontend;

use Input, BlogTopic;
use Auth;

class AjaxBlogTopicController extends AjaxBaseController {

	public function delete()
	{
		$topic = BlogTopic::withTrashed()->findOrFail(Input::get("id"));

		if ($topic->trashed()) {
			return [
				"bOk" => false,
				"message" => "Топик уже удален.",
			];
		}

		if (!$topic->allowDelete(Auth::user())) {
			return [
				"bOk" => false,
				"message" => "Недостаточно прав для удаления этого топика.",
			];
		}

		$topic->delete();

		return [
			"blogTopicTitle" => $topic->title,
			"message" => "Топик отправлен в корзину",
			"bOk" => true,
		];
	}

}
