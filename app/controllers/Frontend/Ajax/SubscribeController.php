<?php namespace Frontend;

use Input, Blog, Auth;
use Subscribe;

class AjaxSubscribeController extends AjaxBaseController {

	public function toggleBlog()
	{
		$id = Input::get("id");
		$blog = Blog::findOrFail($id);

		$subscribe = $blog->userIsSubscribed(Auth::user(), false);

		if ($subscribe)
		{
			$subscribe->delete();

			return [
				"bOk" => true,
				"message" => "Подписка удалена",
				"toggleType" => "delete",
			];
		}

		$subscribe = new Subscribe();
		$subscribe->user_id = Auth::id();

		$blog->subscribes()->save($subscribe);

		return [
			"bOk" => true,
			"message" => "Подписка создана",
			"toggleType" => "create",
		];
		
	}

}