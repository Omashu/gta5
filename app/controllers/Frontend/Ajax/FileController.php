<?php namespace Frontend;

use FileModel, Input;

class AjaxFileController extends AjaxBaseController {
	public function delete()
	{
		// удаляем и открепляем файл
		// удалить файл может только тот, кто его загрузил
		$file = FileModel::where("user_id", \Auth::id())
			->where("id", Input::get("id"))
			->first();

		if (!$file)
		{
			return [
				"bOk" => false,
				"message" => "Неверный запрос",
			];
		}

		$file->delete();

		return [
			"bOk" => true,
			"message" => "Файл удален",
		];
	}
}
