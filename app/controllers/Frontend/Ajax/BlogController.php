<?php namespace Frontend;

use Input, Blog, Auth;

class AjaxBlogController extends AjaxBaseController {

	/**
	 * Ajax запрос на удаление блога
	 */
	public function delete()
	{
		$blog = Blog::findOrFail(Input::get("id"));

		try
		{
			// бросит исключение, если нельзя удалить
			\Allow\Blog::delete(Auth::user(), $blog, true);
		} catch (\System\AllowException $e)
		{
			return [
				"bOk" => false,
				"message" => $e->getMessage(),
			];
		}

		// считаем кол-во топиков
		$movedTopicsCount = $blog->topics()->count();

		// удаляем блог, там будут удалены и топики
		$blog->delete();

		return [
			"movedTopicsCount" => $movedTopicsCount,
			"blogTitle" => $blog->title,
			"bOk" => true,
		];
	}

}
