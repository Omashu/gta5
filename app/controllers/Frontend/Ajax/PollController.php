<?php namespace Frontend;

use PollAnswer, Poll, Input, PollAnswerSel, Auth, Request;
use View;

class AjaxPollController extends AjaxBaseController {

	public function poll()
	{
		$poll = Poll::findOrFail(Input::get("id"));
		$answerIds = Input::get("ids");

		// массив ответов (выбранных) или объект (1 ответ), зависит от multiple
		$answers = null;

		if (is_array($answerIds) AND count($answerIds) AND count($answerIds) <= 50)
		{
			if (!$poll->multiple)
			{
				// берем первый ответ
				$answers = $poll->answers()->where("id", (int) array_shift($answerIds))->first();
			} else
			{
				$answers = $poll->answers()->whereIn("id", $answerIds)->get();
			}
		}

		// может ли голосовать
		if ($answers AND $poll->allow(Auth::user(), Request::getClientIp()))
		{
			// может
			foreach (($poll->multiple?$answers:[$answers]) as $answer) {
				// голосуем
				$sel = new PollAnswerSel();
				$sel->user_id = Auth::id();
				$sel->ip_address = Request::getClientIp();
				$answer->sels()->save($sel);
			}

			return [
				"bOk" => true,
				"message" => "Ваш голос засчитан",
				"results" => View::make("frontend/particles.pollResults", ["poll" => $poll])->render(),
			];
		}

		return [
			"bOk" => false,
			"message" => "Проголосовать неудалось, вероятно Вы уже голосовали",
			"results" => View::make("frontend/particles.pollResults", ["poll" => $poll])->render(),
		];
	}

	public function getResults()
	{
		$poll = Poll::findOrFail(Input::get("id"));

		return [
			"bOk" => true,
			"message" => "На",
			"results" => View::make("frontend/particles.pollResults", ["poll" => $poll])->render(),
		];
	}
}