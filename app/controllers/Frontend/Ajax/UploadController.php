<?php namespace Frontend;

use Image, Input;

class AjaxUploadController extends AjaxBaseController {

	public function file()
	{
		$file = Input::file("file");
		$bOk = false;

		$aResults = [
			"bOk" => &$bOk,
			"message" => null,
			"file" => []
		];

		$check = \DB::table("files")
			->where("user_id", \Auth::id())
			->where("fileable_type", "")
			->count();

		if ($check >= \Config::get("app.uploads.maxFilesOnOneTarget"))
		{
			$aResults["message"] = "У Вас {$check} неиспользуемых файлов, перейдите в свой профиль и удалите их, либо прикрепите к какому-то материалу.";
			return $aResults;
		}

		if ($file)
		{
			$model = \FileModel::upload($file);

			if (!$model)
			{
				// error
				$aResults["message"] = "Файл не является архивом, либо его размеры слишком велики";
				return $aResults;
			}

			$bOk = true;
			$aResults["file"] =
			[
				"id" => $model->id,
				"title" => $model->title,
				"relUrl" => $model->getRelPath(),
				"absUrl" => $model->getUrl(),
			];

			$aResults["message"] = "Файл загружен";
			return $aResults;
		}

		$aResults["message"] = "Файл не передан";
		return $aResults;
	}

	public function image()
	{
		$file = Input::file("image");
		$bOk = false;

		$aResults = [
			"bOk" => &$bOk,
			"message" => null,
			"file" => []
		];

		$check = \DB::table("images")
			->where("user_id", \Auth::id())
			->whereNotExists(function($q)
			{
				$q->select(\DB::raw(1))
					->from('imageables')
					->whereRaw('images.id = imageables.image_id');
			})
			->count();

		if ($check >= \Config::get("app.uploads.maxImagesOnOneTarget"))
		{
			$aResults["message"] = "У Вас {$check} неиспользуемых изображений, перейдите в свой профиль и удалите их, либо прикрепите к какому-то материалу.";
			return $aResults;
		}

		if ($file)
		{
			$model = Image::upload($file);
			if (!$model)
			{
				// error
				$aResults["message"] = "Файл не является изображением, либо его размеры слишком велики";
				return $aResults;
			}

			$bOk = true;
			$aResults["file"] =
			[
				"id" => $model->id,
				"title" => $model->title,
				"relUrl" => $model->getRelPath(),
				"absUrl" => $model->getUrl(),
			];
			$aResults["message"] = "Изображение загружено";
			return $aResults;
		}

		$aResults["message"] = "Файл не передан";
		return $aResults;
	}
}
