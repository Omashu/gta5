<?php namespace Frontend;

use Input, Comment, Auth, View;
use Validator;

class AjaxCommentController extends AjaxBaseController {

	public function postBlogTopic()
	{
		$topic = \BlogTopic::findOrFail(Input::get("id"));
		$parentId = Input::get("parent_id");
		$parent = null;

		if ($parentId)
			$parent = $topic->comments()->findOrFail($parentId);

		$sMessage = trim(Input::get("message"));

		if (!$sMessage) {
			return [
				"bOk" => false,
				"message" => "Введите сообщение!",
				"parent_id" => $parent ? $parent->id : null
			];
		}

		$node = new Comment(Input::only(["message"]));
		$node->user_id = Auth::id();

		$topic->comments()->save($node);

		if (!$parent) $node->saveAsRoot();
		else $parent->append($node);

		$newCommentId = $node->id;
		$comment = Comment::with("user", "user.avatar")->withDepth()->findOrFail($newCommentId);

		return [
			"bOk" => true,
			"message" => "Комментарий успешно добавлен",
			"parent_id" => $parent ? $parent->id : null,
			"view" => View::make("frontend/comments.blogTopic", ["parent_id" => $parentId, "comment" => $comment, "topic" => $topic])->render()
		];
	}

	public function upSpamLevel()
	{
		$comment = Comment::findOrFail(Input::get("id"));

		if ($comment->allowUpSpamLevel(Auth::user()))
		{
			// если есть право на установку флага-спама, помечаем как спам, если права нет значит просто повышаем уровень
			if ($comment->allowSetSpam(Auth::user()))
			{
				// уже спам, у юзера глоб права, восстановим коммент
				if ($comment->spam)
				{
					$comment->spam = false;
					$comment->spam_level = 0;
					$message = "Комментарий восстановлен";
				} else
				{
					$comment->spam = true;
					$message = "Комментарий спрятан";
				}
			} else
			{
				if ($comment->spam_level < 1000 and !$comment->spam)
					$comment->spam_level++;

				$message = "Модератор проверит этот комментарий";
			}

			$comment->save();
			return [
				"bOk" => true,
				"allowSetSpam" => $comment->allowSetSpam(Auth::user()),
				"message" => $message,
				"commentId" => $comment->id,
				"commentSpamLevel" => (int) $comment->spam_level,
				"commentSpam" => (bool) $comment->spam,
				"commentMessage" => $comment->message,
			];
		}

		return [
			"bOk" => false,
			"allowSetSpam" => false,
			"message" => "Недостаточно прав",
			"commentId" => $comment->id,
			"commentSpamLevel" => (int) $comment->spam_level,
			"commentSpam" => (bool) $comment->spam,
			"commentMessage" => $comment->message,
		];
	}
}
