<?php namespace Frontend;

use Rating;
use Auth, Input;
use Date;
use App;
use RatingException;

// в течении месяца свой голос можно отменить

class AjaxRatingController extends AjaxBaseController {

	protected $bonus = [
		"comment" => 0.5,
		"blogTopic" => 1,
	];

	public function blogTopicUp()
	{
		$topic = \BlogTopic::findOrFail(Input::get("id"));
		$res = $this->handler($topic, 1, 1);

		if ($res["bOk"]) {
			$topic->user->rating += $this->bonus["blogTopic"];
			$topic->user->save();
		}

		return $res;
	}

	public function blogTopicDown()
	{
		$topic = \BlogTopic::findOrFail(Input::get("id"));
		$res = $this->handler($topic, 0, 1);

		if ($res["bOk"]) {
			$topic->user->rating -= $this->bonus["blogTopic"];
			$topic->user->save();
		}

		return $res;
	}

	public function commentUp()
	{
		$comment = \Comment::findOrFail(Input::get("id"));
		$res = $this->handler($comment, 1, 1);

		if ($res["bOk"]) {
			$comment->user->rating += $this->bonus["comment"];
			$comment->user->save();
		}

		return $res;
	}

	public function commentDown()
	{
		$comment = \Comment::findOrFail(Input::get("id"));
		$res = $this->handler($comment, 0, 1);

		if ($res["bOk"]) {
			$comment->user->rating -= $this->bonus["comment"];
			$comment->user->save();
		}

		return $res;
	}

	protected function handler($model, $direction, $value)
	{
		$aRes = [
			"bOk" => false,
			"message" => null,
			"id" => $model->id,
			// можно ли юзануть рейтинг
			"active" => false,
			"value" => $model->rating,
		];

		try {
			$model->ratingAllowUseByUserThrow(Auth::user());
		} catch (RatingException $e) {
			$aRes["message"] = $e->getMessage();
			return $aRes;
		}

		$ratingModel = $model->ratingObjectByUser(Auth::user(), null, true);

		// если есть модель и не "удалена" откатим голос
		if ($ratingModel and !$ratingModel->trashed()) {
			if ($direction === (int) $ratingModel->direction) {
				if ($direction === 1) {
					// пробует ставить +, тогда как уже ставил +
					$aRes["message"] = "Здесь Вы уже ставили плюс";
					return $aRes;
				} else {
					// пробует ставить -, тогда как уже ставил -
					$aRes["message"] = "Здесь Вы уже ставили минус";
					return $aRes;
				}
			}

			// отнимаем плюс или минус
			if ((int) $ratingModel->direction === 1) {
				// ставил плюс, отнимаем его
				$model->rating -= $ratingModel->value;
				$aRes["message"] = "Вы убрали свой плюс";
			} else {
				// ставил минус, отнимаем его
				$model->rating += $ratingModel->value;
				$aRes["message"] = "Вы убрали свой минус";
			}

			// сохраняем рейтинг
			$model->save();

			// выполняем удаление модели рейтинга, сохраняя даты первого использования
			$ratingModel->delete();

			// ok / можно поставить плюс или минус
			$aRes["bOk"] = true;
			$aRes["active"] = true;
			$aRes["value"] = $model->rating;

			return $aRes;
		}

		// создаем модель
		$ratingModel = $ratingModel ? $ratingModel : new Rating();

		// direction
		$ratingModel->direction = $direction;

		// плюс или минус
		$ratingModel->value = $value;

		// пользователь
		$ratingModel->user_id = Auth::id();

		// восстанавливаем или связываем
		if ($ratingModel->trashed()) $ratingModel->restore();
		else $model->ratings()->save($ratingModel);

		// сохраняем
		if ($direction === 1) $model->rating += $ratingModel->value;
		else $model->rating -= $ratingModel->value;

		$model->save();

		// можно убрать свой голос
		$aRes["active"] = true;
		$aRes["bOk"] = true;
		$aRes["message"] = $direction === 1 ? "Ваш плюс засчитан" : "Ваш минус засчитан";
		$aRes["value"] = $model->rating;

		return $aRes;
	}
}