<?php namespace Frontend;

use Input, Blog;
use Auth;

class AjaxBlogPermissionController extends AjaxBaseController {

	public function updateOrNew()
	{
		$username = Input::get("username");
		$blogId = Input::get("blog_id");
		$flagWrite = Input::get("flag_write");
		$flagModerate = Input::get("flag_moderate");

		$blog = Blog::findOrFail($blogId);
		if (!$blog->allowEdit(Auth::user())) {
			return [
				"bOk" => false,
				"message" => "Недостаточно прав для редактирования этого блога",
			];
		}

		$user = \User::where("username", $username)->first();
		if (!$user) {
			return [
				"bOk" => false,
				"message" => "Пользователь не найден",
			];
		}

		$permission = $blog->permissions()->where("user_id", $user->getKey())->first();
		if (!$permission)
			$permission = new \BlogPermission();

		$permission->user_id = $user->getKey();
		$permission->flag_write = (bool) $flagWrite;
		$permission->flag_moderate = (bool) $flagModerate;

		$blog->permissions()->save($permission);

		return [
			"username" => $username,
			"flagWrite" => $permission->flag_write,
			"flagModerate" => $permission->flag_moderate,
			"blogId" => $permission->blog_id,
			"message" => "Привилегии обновлены",
			"bOk" => true,
		];
	}

	public function delete()
	{
		$username = Input::get("username");
		$blogId = Input::get("blog_id");

		$blog = Blog::findOrFail($blogId);
		if (!$blog->allowEdit(Auth::user())) {
			return [
				"bOk" => false,
				"message" => "Недостаточно прав для редактирования этого блога",
			];
		}

		$user = \User::where("username", $username)->first();
		if (!$user) {
			return [
				"bOk" => false,
				"message" => "Пользователь не найден",
			];
		}

		$permission = $blog->permissions()->where("user_id", $user->getKey())->first();
		if (!$permission) {
			return [
				"bOk" => false,
				"message" => "Пользователь не найден",
			];
		}

		$permission->delete();
		return [
			"username" => $username,
			"message" => "Привилегия удалена",
			"bOk" => true,
		];
	}
}
