<?php namespace Frontend;

class AjaxTagController extends AjaxBaseController {

	public function getCompleteFromString()
	{
		$string = \Input::get("query");
		if (!is_string($string) OR mb_strlen($string) < 1)
			return ["query" => null, "suggestions" => []];

		$tags = \Conner\Tagging\Tag::where("name", "like", "%".$string."%")->get();
		$res = ["query" => $string, "suggestions" => []];

		foreach ($tags as $tag) {
			$res["suggestions"][] = $tag->name;
		}

		return $res;
	}
}