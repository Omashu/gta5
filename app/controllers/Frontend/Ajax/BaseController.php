<?php namespace Frontend;

use View, Auth;

class AjaxBaseController extends \BaseController {

	public function __construct()
	{
		View::share("curUser", Auth::user());
	}

}