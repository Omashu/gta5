<?php namespace Backend;

use Front;
use Lang;
use View;
use Config;
use Message;
use Auth;
use Session;
use Option;
use App;
use Permission;

class BaseController extends \BaseController {

	public function __construct()
	{
		if (!Auth::check() OR !Auth::user()->allow("dashboard.index"))
		{
			// access denied
			App::abort(403);
		}

		// синхронизируем уровни
		$permissions = Permission::get()->lists("id", "name");
		foreach (Config::get("permissions") as $values) {
			if (!isset($permissions[$values["name"]])) {
				// этого уровня нет, его нужно создать
				$permission = new Permission($values);
				$permission->name = $values["name"];
				$permission->save();
			}
		}

		Front::title(Config::get("backend.title"));
		Front::description(Config::get("backend.description"));
		Front::keywords(Config::get("backend.keywords"));
		Front::scripts(Config::get("backend.scripts", []));
		Front::styles(Config::get("backend.styles", []));
		Front::favicon(asset("favicon.ico"));

		Front::js("token", Session::token());
		View::share("curUser", Auth::user());
		
		Front::js("route.as.BackendAjaxGraphGetNewUsersMonth", route("BackendAjaxGraphGetNewUsersMonth"));
	}

}