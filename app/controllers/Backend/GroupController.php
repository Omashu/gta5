<?php namespace Backend;

use Front, Message;
use Lang, View;
use Group, User;
use Input, Validator, Redirect;
use Option;
use Permission;

class GroupController extends BaseController {

	public function showList()
	{
		Front::title(Lang::get("backend/front.group.list"));

		$groups = Group::all();

		$this->layout = View::make("backend/group.list",
		[
			"groups" => $groups,
		]);
	}

	public function getAdd()
	{
		Front::title(Lang::get("backend/front.group.add"));

		$this->layout = View::make("backend/group.add",
		[
		]);
	}

	public function postAdd()
	{
		$rules = Group::getRules("create");
		$values = Input::only(["name", "title", "description"]);

		$validator = Validator::make($values, $rules);

		if ($validator->fails())
		{
			Message::danger($validator);
			return Redirect::back()->withInput($values);
		}

		$group = new Group($values);
		$group->name = $values["name"];
		$group->save();

		Message::success(Lang::get("backend/message.groupCreated", ["title" => $group->title]));
		return Redirect::route("BackendGroupEdit", $group->id);
	}

	public function getEdit($id)
	{
		$group = Group::findOrFail($id);
		Front::title($group->title);

		$this->layout = View::make("backend/group.edit",
		[
			"group" => $group,
		]);
	}

	public function postEdit($id)
	{
		$group = Group::findOrFail($id);

		$rules = Group::getRules("update", ["id" => $group->id]);
		$values = Input::only(["name", "title", "description"]);

		$validator = Validator::make($values, $rules);

		if ($validator->fails())
		{
			Message::danger($validator);
			return Redirect::back()->withInput($values);
		}

		$group->name = $values["name"];
		$group->update($values);

		Message::success(Lang::get("backend/message.groupUpdated", ["title" => $group->title]));

		$bDef = (bool) Input::get("default");
		if ($bDef) {
			Option::set("defaultGroupId", $group->id);
			Message::success(Lang::get("backend/message.groupDefault", ["title" => $group->title]));
		}

		return Redirect::back();
	}

	public function getDelete($id)
	{
		$group = Group::findOrFail($id);
		Front::title($group->title);

		$groupTransfList = Group::all()->getAssoc();
		unset($groupTransfList[$group->id]);

		$this->layout = View::make("backend/group.delete",
		[
			"group" => $group,
			"groupTransfList" => $groupTransfList,
		]);
	}

	public function postDelete($id)
	{
		$group = Group::findOrFail($id);
		$groupTransf = Group::find(Input::get("id"));

		if (!$groupTransf) {
			Message::success(Lang::get("backend/message.groupTransfUndefined"));
			return Redirect::back();
		}

		User::where("group_id", "=", $group->id)->update(["group_id"=>$groupTransf->id]);
		$group->delete();

		Message::success(Lang::get("backend/message.groupDeleted", ["title" => $group->title]));
		return Redirect::route("BackendGroupShowList");
	}

	public function getPermissions($id)
	{
		$group = Group::findOrFail($id);
		Front::title(Lang::get("backend/front.group.permissions", ["title"=>$group->title]));

		$permissions = Permission::all();

		$this->layout = View::make("backend/group.permissions",
		[
			"group" => $group,
			"permissions" => $permissions
		]);
	}

	public function postPermissions($id)
	{
		$group = Group::findOrFail($id);
		Front::title(Lang::get("backend/front.group.permissions", ["title"=>$group->title]));

		$permissions = Input::get("permissions");
		if (!is_array($permissions))
		{
			Message::danger("Ошибка");
			return Redirect::back();
		}

		$group->permissions()->sync($permissions);

		return Redirect::back();
	}

}