<?php namespace Backend;

use Front, Message;
use Lang, View;
use Input;
use Redirect;

class HomeController extends BaseController {

	public function showIndex()
	{
		// Front::title(Lang::get("backend/front.home.index"));

		$this->layout = View::make("backend/home.index");
	}

}