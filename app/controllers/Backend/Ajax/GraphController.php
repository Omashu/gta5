<?php namespace Backend;

use Date;
use User;

class AjaxGraphController extends BaseController {

	public function getNewUsersMonth()
	{
		$date30days = Date::now()->parse('-30 day')->format("Y-m-d");
		$iStart = 31;

		$values = [];
		for ($i=$iStart; $i > 0; $i--) { 
			$date = Date::now()->parse('-'.$i.' day')->format("Y-m-d");
			$values[$date] = [
				"date" => $date,
				"count" => 0
			];
		}

		$users = User::where("created_at", ">=", $date30days)->get();
		foreach ($users as $user) {
			$date = Date::parse($user->created_at)->format("Y-m-d");
			$values[$date]["date"] = $date;
			if (isset($values[$date]["count"])) $values[$date]["count"]++;
			else $values[$date]["count"] = 1;
		}

		return [
			"data" => array_values($values),
			"count" => $users->count(),
			"_tm" => time()
		];
	}
}