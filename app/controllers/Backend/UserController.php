<?php namespace Backend;

use Front, Message;
use Lang, View;
use Input;
use Group;
use User, Image;
use Option;
use Validator;
use Redirect;
use Hash;

class UserController extends BaseController {

	public function showList()
	{
		Front::title(Lang::get("backend/front.user.list"));

		$perPage = (int) Input::get("per_page", 50);
		if ($perPage > 500) $perPage = 500;
		else if ($perPage < 10) $perPage = 10;

		$groups = Group::all()->getAssoc("id", "title", [null => "Все"]);
		$groupId = Input::get("group_id", null);
		if (!isset($groups[$groupId])) $groupId = null;

		$sortColumns = User::getSortColumns();
		$sortColumn = Input::get("by");
		if (!isset($sortColumns[$sortColumn])) $sortColumn = "id";

		$sortWays = ["desc" => "Убывание", "asc" => "Возрастание"];
		$sortWay = Input::get("way");
		if (!isset($sortWays[$sortWay])) $sortWay = "desc";

		$bWriteOnly = (bool) Input::get("write_only");
		$bReadOnly = (bool) Input::get("read_only");
		$bShowAvatar = (bool) Input::get("show_avatar");

		$users = User::orderBy($sortColumn, $sortWay);
		if ($groupId) $users->where("group_id", "=", $groupId);
		if ($bWriteOnly AND !$bReadOnly) $users->where("write_only", "=", true);
		if ($bReadOnly AND !$bWriteOnly) $users->where("write_only", "=", false);

		$users = $users->paginate($perPage)->appends([
			"group_id" => $groupId,
			"by" => $sortColumn,
			"way" => $sortWay,
			"write_only" => $bWriteOnly,
			"read_only" => $bReadOnly,
			"show_avatar" => $bShowAvatar,
		]);

		$this->layout = View::make("backend/user.list",
			[
				"users" => $users,

				"fPerPage" => $perPage,
				"fGroupId" => $groupId,
				"fGroups" => $groups,
				"fSortColumns" => $sortColumns,
				"fSortColumn" => $sortColumn,
				"fSortWays" => $sortWays,
				"fSortWay" => $sortWay,
				"fWriteOnly" => $bWriteOnly,
				"fReadOnly" => $bReadOnly,
				"fShowAvatar" => $bShowAvatar,
			]);
	}

	public function getEdit($id)
	{
		$user = User::with("avatar")->findOrFail($id);
		Front::title($user->username);

		$groups = Group::all()->getAssoc("id", "title");
		$this->layout = View::make("backend/user.edit",
		[
			"user" => $user,
			"groups" => $groups,
		]);
	}

	public function postEdit($id)
	{
		$user = User::findOrFail($id);

		// загрузка аватара
		$avatar = Input::file("avatar");
		$bAvatarUploaded = false;
		if ($avatar)
		{
			$image = Image::upload($avatar);
			if (!$image)
			{
				Message::danger(Image::getValidator());
			} else
			{
				$bAvatarUploaded = true;
				$user->avatar_id = $image->id;

				Message::success("Аватар обновлен");
			}
		}

		$rules = User::getRules("update", ["id" => $user->id]);
		$values = Input::only(["first_name", "last_name", "about_myself", "username", "password", "email", "group_id"]);

		$validator = Validator::make($values, $rules);
		if ($validator->fails())
		{
			if ($bAvatarUploaded) $user->save();
			Message::danger($validator);
			return Redirect::back()->withInput($values);
		}

		$user->username = $values["username"];
		if ($values["password"]) $user->password = Hash::make($values["password"]);
		$user->email = $values["email"];
		$user->group_id = $values["group_id"];

		$user->update($values);
		Message::success(Lang::get("backend/message.userUpdated", ["username" => $user->username]));
		return Redirect::back();
	}

}