<?php namespace Backend;

use Front, Config, View, Input, Message;
class ImageController extends BaseController {

	public function showList()
	{
		Front::title("Список загруженных изображений");

		$images = \Image::orderBy("created_at", "desc")
			->with("user", "user.group")
			->paginate(100);

		$this->layout = View::make("backend/image.list",
			[
				"images" => $images,
			]);
	}

	public function showActions()
	{
		Front::title("Действия над изображениями");

		$maxPossibleThumbs = Config::get("croppa::config.max_crops");
		if ($maxPossibleThumbs)
			$maxPossibleThumbs *= \Image::count();

		$this->layout = View::make("backend/image.action",
			[
				"maxPossibleThumbs" => $maxPossibleThumbs,
			]);
	}

	public function postActions()
	{
		$action = Input::get("action");

		switch ($action) {
			case 'delete_thumbs':
				foreach (\Image::all() as $image)
					$image->resetCrops();

				Message::success("Все возможные миниатюры были удалены...");
				break;
			
			default:
				Message::danger("Неизвестное действие...");
				break;
		}

		return redirectBackOrHome();
	}

	public function showEdit($id)
	{
		$image = \Image::findOrFail($id);
		Front::title("Изображение: ".$image->title);

		$this->layout = View::make("backend/image.edit",
			[
				"image" => $image,
			]);
	}
}