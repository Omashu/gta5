<?php namespace Backend;

use Front, Message, Lang, View, Input;
use Blog, Auth;
use Validator, Redirect;

class BlogController extends BaseController {

	public function showList()
	{
		Front::title(Lang::get("backend/front.blog.list"));

		$perPage = (int) Input::get("per_page", 50);
		if ($perPage > 500) $perPage = 500;
		else if ($perPage < 10) $perPage = 10;

		$sortColumns = Blog::getSortColumns();
		$sortColumn = Input::get("by");
		if (!isset($sortColumns[$sortColumn])) $sortColumn = "id";

		$sortWays = ["desc" => "Убывание", "asc" => "Возрастание"];
		$sortWay = Input::get("way");
		if (!isset($sortWays[$sortWay])) $sortWay = "desc";

		$blogs = Blog::with("user")->paginate($perPage)->appends(
		[
			"per_page" => $perPage,
			"by" => $sortColumn,
			"way" => $sortWay
		]);

		$this->layout = View::make("backend/blog.list",
		[
			"blogs" => $blogs,

			"fPerPage" => $perPage,
			"fSortColumns" => $sortColumns,
			"fSortColumn" => $sortColumn,
			"fSortWays" => $sortWays,
			"fSortWay" => $sortWay,
		]);
	}

	public function getAdd()
	{
		Front::title(Lang::get("backend/front.blog.add"));
		$this->layout = View::make("backend/blog.add");
	}

	public function postAdd()
	{
		$values = Input::only("title", "description");
		$values["user_id"] = Auth::user()->id;

		$rules = Blog::getRules("create");
		$validator = Validator::make($values, $rules);

		if ($validator->fails())
		{
			Message::danger($validator);
			return Redirect::back()->withInput($values);
		}

		$blog = new Blog($values);
		$blog->user_id = $values["user_id"];
		$blog->save();

		Message::success(Lang::get("backend/message.blogCreated", ["title" => $blog->title]));
		return Redirect::route("BackendBlogEdit", $blog->id);
	}
}