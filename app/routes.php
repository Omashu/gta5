<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::pattern("slug", "[a-zA-Z0-9\-]+");
Route::pattern("id", "\d+");

Route::group([
	"before" => [],
	"namespace" => "Frontend",
], function()
{

	/**
	 * Создание блога
	 */
	Route::get("/newBlog.html", [
		"as" => "newBlog",
		"before" => [
			"auth",
			"allow.blog.add"
		], "uses" => "BlogController@showCreate"]);

	Route::post("/newBlog.html", [
		"as" => "newBlogPost",
		"before" => [
			"auth",
			"csrf",
			"allow.blog.add"
		], "uses" => "BlogController@postCreate"]);

	/**
	 * Создание нового топики
	 */
	Route::get("/newTopic.html", [
		"as" => "newTopic", 
		"before" => [
			"auth",
			"allow.blogTopic.add"
		], "uses" => "BlogTopicController@showCreate"]);

	Route::post("/newTopic.html", [
		"as" => "newTopicPost", 
		"before" => [
			"auth", 
			"csrf",
			"allow.blogTopic.add"
		], "uses" => "BlogTopicController@postCreate"]);

	/**
	 * Статичные страницы
	 */
	Route::get("/faq.html", "PageController@faq");

	/**
	 * Фидбек
	 */
	Route::get("/feedback.html", ["as" => "feedback", "uses" => "PageController@feedback"]);
	Route::post("/feedback.html", ["before" => "csrf", "uses" => "PageController@postFeedback"]);

	/**
	 * Главная страница
	 */
	Route::get("/", ["as" => "home", "uses" => "HomeController@showIndex"]);

	/**
	 * Авторизация на сайте
	 * только для гостей
	 */
	Route::get("/login", ["before" => "guest", "uses" => "AuthController@getLogin"]);
	Route::post("/login", ["before" => "guest", "uses" => "AuthController@postLogin"]);

	/**
	 * Регистрация на сайте
	 * только для гостей
	 */
	Route::get("/register", ["before" => "guest", "uses" => "AuthController@getRegister"]);
	Route::post("/register", ["before" => "guest", "uses" => "AuthController@postRegister"]);

	/**
	 * Выход с аккаунта
	 * только для авторизованных
	 */
	Route::get("/logout", ["as" => "logout", "before" => "auth", function()
		{
			Auth::logout();
			Message::success("Вы вышли из системы.");
			return Redirect::home();
		}]);

	/**
	 * Подтверждение e-mail адреса
	 */
	Route::get("/confirm", ["as" => "sendConfirm", "uses" => "AuthController@sendConfirmMail"]);
	Route::get("/confirm/{slug}", ["as" => "confirm", "uses" => "AuthController@getConfirm"]);

	/**
	 * Восстановление пароля
	 */
	Route::controller('password', 'AuthRemindersController');

	/**
	 * Просмотр топика
	 */
	Route::get("/topic/{slug}.html", ["as" => "showTopic", "uses" => "BlogTopicController@show"]);

	/**
	 * Редактирование топика
	 * только для авторизованных
	 */
	Route::get("/topic/{id}/edit", ["as" => "showTopicEdit", "before" => "auth", "uses" => "BlogTopicController@showEdit"]);
	Route::post("/topic/{id}/edit", ["before" => ["auth","csrf"], "uses" => "BlogTopicController@postEdit"]);

	/**
	 * Список блогов по рейтингу его топиков
	 */
	Route::get("/blogs", ["as" => "showBlogsPopular", "uses" => "BlogController@showIndexPopular"]);

	/**
	 * Просмотр топиков блога
	 */
	Route::get("/blog/{slug}", ["as" => "showBlog", "uses" => "BlogController@show"]);

	/**
	 * Редактирование блога
	 * только для авторизованных
	 */
	Route::get("/blog/{slug}/edit", ["before" => "auth", "as" => "showBlogEdit", "uses" => "BlogController@showEdit"]);
	Route::post("/blog/{slug}/edit", ["before" => ["auth","csrf"], "as" => "showBlogEditPost", "uses" => "BlogController@postEdit"]);
	Route::get("/blog/{slug}/editPermissions", ["before" => "auth", "as" => "showBlogEditPermissions", "uses" => "BlogController@showEditPermissions"]);
	Route::post("/blog/{slug}/editPermissions", ["before" => ["auth","csrf"], "as" => "showBlogEditPermissionsPost", "uses" => "BlogController@postEditPermissions"]);

	/**
	 * Просмотр профиля юзера
	 */
	Route::get("/user/{username}", ["as" => "showUserProfile", "uses" => "UserController@showProfile"]);

	/**
	 * Редактирование профиля
	 */
	Route::get("/settings", ["before" => "auth", "as" => "showUserProfileSettings", "uses" => "UserController@showProfileSettings"]);
	Route::post("/settings", ["before" => ["auth","csrf"], "uses" => "UserController@postProfileSettings"]);

	/**
	 * Просмотр блогов юзера
	 */
	Route::get("/user/{username}/blogs", ["as" => "showUserBlogs", "uses" => "UserController@showBlogs"]);

	/**
	 * Просмотр топиков юзера
	 */
	Route::get("/user/{username}/topics", ["as" => "showUserTopics", "uses" => "UserController@showTopics"]);

	/**
	 * Изображения юзера
	 */
	Route::get("/user/{username}/images", ["as" => "showUserImages", "uses" => "UserController@showImages"]);

	/**
	 * Подписки юзера (на блоги)
	 * только для авторизованных
	 */
	Route::get("/subs", ["before" => "auth", "as" => "showUserSubs", "uses" => "UserController@showSubs"]);

	/**
	 * Лента топиков по подпискам на блоги
	 * только для авторизованных
	 */
	Route::get("/stream", ["before" => "auth", "as" => "showUserStream", "uses" => "UserController@showStream"]);

	/**
	 * Просмотр топиков по тегу
	 */
	Route::get("/tag/{tagName}", ["as" => "showTag", "uses" => "TagController@showBlogTopics"]);

	/**
	 * Просмотр новых топиков
	 */
	Route::get("/new", ["as" => "showNewTopics", "uses" => "BlogTopicController@showNew"]);

	/**
	 * Просмотр комментируемых топиков
	 * по умолчанию = week
	 */
	Route::get("/commented/{period?}", ["as" => "showCommentedTopics", "uses" => "BlogTopicController@showCommented"])
		->where(["period" => "(day|week|month)"]);

	/**
	 * Страница с опросами, с пагинацией итп
	 */
	Route::get("/polls", ["as" => "showNewPolls", "uses" => "PollController@showNew"]);
	Route::get("/poll/{slug}.html", ["as" => "showPoll", "uses" => "PollController@show"]);

	/**
	 * Страница с списком людей
	 */
	Route::get("/people", ["as" => "showUsers", "uses" => "PeopleController@show"]);

	/**
	 * Отдача файла
	 * 
	 */
	Route::get("/file/{slug}/download", ["as" => "fileDownload", function($slug)
	{
		$file = FileModel::findBySlug($slug);
		if (!$file) App::abort(404);

		// check
		if (Auth::id())
			$check = $file->downloads()->where(function($q)
			{
				$q->where("user_id", Auth::id());
				$q->orWhere("ip_address", Request::getClientIp());
			})->count();
		else
			$check = $file->downloads()->where("ip_address", Request::getClientIp())->count();

		if (!$check)
		{
			$fileDownload = new FileModelDownload();
			$fileDownload->user_id = Auth::id();
			$fileDownload->ip_address = Request::getClientIp();
			$file->downloads()->save($fileDownload);
		}

		$fileName = $file->slug.".".$file->format;

		return Response::download($file->getAbsPath(), $fileName,
		[
		]);
	}]);
});

Route::group([
	"before" => [],
	"namespace" => "Frontend",
	"prefix" => "ajax",
	"before" => ["csrf"],
], function()
{

	/**
	 * Загрузка изображения/файла
	 * только авторизованным
	 */
	Route::post("uploadImage", ["before" => "auth", "as" => "ajaxUploadImage", "uses" => "AjaxUploadController@image"]);
	Route::post("uploadFile", ["before" => "auth", "as" => "ajaxUploadFile", "uses" => "AjaxUploadController@file"]);

	/**
	 * Удаление изображения
	 */
	Route::post("imageDelete", ["before" => "auth", "as" => "ajaxImageDelete", "uses" => "AjaxImageController@delete"]);

	/**
	 * Удаление файла
	 */
	Route::post("fileDelete", ["before" => "auth", "as" => "ajaxFileDelete", "uses" => "AjaxFileController@delete"]);

	/**
	 * Autocomplete tags
	 */
	Route::get("tagComplete", ["as" => "ajaxTagComplete", "uses" => "AjaxTagController@getCompleteFromString"]);

	/**
	 * Autocomplete users
	 */
	Route::get("userComplete", ["as" => "ajaxUserComplete", "uses" => "AjaxUserController@getCompleteFromString"]);

	// adding comments
	Route::post("newComment?BlogTopic", ["before" => "auth", "as" => "ajaxCommentBlogTopic", "uses" => "AjaxCommentController@postBlogTopic"]);

	Route::post("toggleSubscribe?Blog", ["before" => "auth", "as" => "ajaxSubscribeToggleBlog", "uses" => "AjaxSubscribeController@toggleBlog"]);

	Route::post("blogDelete", ["before" => "auth", "as" => "ajaxBlogDelete", "uses" => "AjaxBlogController@delete"]);
	Route::post("blogTopicDelete", ["before" => "auth", "as" => "ajaxBlogTopicDelete", "uses" => "AjaxBlogTopicController@delete"]);

	/**
	 * Привилегии пользователей у блога
	 * Создание/обновление
	 */
	Route::post("blogPermission", ["before" => "auth", "as" => "ajaxBlogPermission", "uses" => "AjaxBlogPermissionController@updateOrNew"]);

	/**
	 * Удаление привилегии
	 */
	Route::post("blogPermissionDelete", ["before" => "auth", "as" => "ajaxBlogPermissionDelete", "uses" => "AjaxBlogPermissionController@delete"]);


	// comments
	Route::post("commentSpamUp", ["before" => "auth", "as" => "ajaxCommentSpamUp", "uses" => "AjaxCommentController@upSpamLevel"]);

	// ratings
	Route::post("ratingDown?BlogTopic", ["before" => "auth", "as" => "ajaxRatingBlogTopicDown", "uses" => "AjaxRatingController@blogTopicDown"]);
	Route::post("ratingUp?BlogTopic", ["before" => "auth", "as" => "ajaxRatingBlogTopicUp", "uses" => "AjaxRatingController@blogTopicUp"]);

	Route::post("ratingDown?Comment", ["before" => "auth", "as" => "ajaxRatingCommentDown", "uses" => "AjaxRatingController@commentDown"]);

	Route::post("ratingUp?Comment", ["before" => "auth", "as" => "ajaxRatingCommentUp", "uses" => "AjaxRatingController@commentUp"]);

	// опросы
	Route::post("poll", ["as" => "ajaxPoll", "uses" => "AjaxPollController@poll"]);
	Route::get("pollResults", ["as" => "ajaxPollResults", "uses" => "AjaxPollController@getResults"]);
});

/** BACKEND ***/

// admin
Route::group([
	"before" => ["auth"],
	"namespace" => "Backend",
	"prefix" => Config::get("backend.prefix")
], function()
{
	Route::get("/", "HomeController@showIndex");

	/* Users */
	Route::get("/users", ["as" => "BackendUserShowList", "uses" => "UserController@showList"]);

	// add
	Route::get("/user/add", "UserController@getAdd");
	Route::post("/user/add", ["before" => "csrf", "uses" => "UserController@postAdd"]);

	// edit
	Route::get("/user/{id}/edit", ["as" => "BackendUserEdit", "uses" => "UserController@getEdit"]);
	Route::post("/user/{id}/edit", ["before" => "csrf", "uses" => "UserController@postEdit"]);

	/* Groups */
	Route::get("/groups", ["as" => "BackendGroupShowList", "uses" => "GroupController@showList"]);
	Route::get("/group/add", ["as" => "BackendGroupAdd", "uses" => "GroupController@getAdd"]);
	Route::post("/group/add", ["as" => "BackendGroupAddPost", "before" => "csrf", "uses" => "GroupController@postAdd"]);

	Route::get("/group/{id}/edit", ["as" => "BackendGroupEdit", "uses" => "GroupController@getEdit"]);
	Route::post("/group/{id}/edit", ["before" => "csrf", "uses" => "GroupController@postEdit"]);

	Route::get("/group/{id}/permissions", ["as" => "BackendGroupPermissions", "uses" => "GroupController@getPermissions"]);
	Route::post("/group/{id}/permissions", ["before" => "csrf", "uses" => "GroupController@postPermissions"]);

	Route::get("/group/{id}/delete", ["as" => "BackendGroupDelete", "uses" => "GroupController@getDelete"]);
	Route::post("/group/{id}/delete", ["before" => "csrf", "uses" => "GroupController@postDelete"]);

	/* Blogs */
	Route::get("/blogs", ["as" => "BackendBlogShowList", "uses" => "BlogController@showList"]);
	Route::get("/blog/add", ["as" => "BackendBlogAdd", "uses" => "BlogController@getAdd"]);
	Route::post("/blog/add", ["as" => "BackendBlogAddPost", "before" => "csrf", "uses" => "BlogController@postAdd"]);

	/**
	 * Список загруженных изображений
	 */
	Route::get("/images", ["as" => "BackendImageShowList", "uses" => "ImageController@showList"]);

	/**
	 * Спец. возможности по работе с изображениями
	 */
	Route::get("/images/actions", ["as" => "BackendImageShowActions", "uses" => "ImageController@showActions"]);
	Route::post("/images/actions", ["before" => "csrf", "as" => "BackendImageShowActions", "uses" => "ImageController@postActions"]);

	Route::get("/image/{id}/edit", ["as" => "BackendImageEdit", "uses" => "ImageController@showEdit"]);
});

Route::group([
	"before" => ["auth", "csrf"],
	"namespace" => "Backend",
	"prefix" => Config::get("backend.prefix")."/ajax"
], function()
{
	/* Graphs */
	Route::get("graph/newUsersMonth.json", [
		"as" => "BackendAjaxGraphGetNewUsersMonth",
		"uses" => "AjaxGraphController@getNewUsersMonth",
	]);

});
