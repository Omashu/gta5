<?php

class GroupTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$groups = [
			[
				"name" => "admin",
				"title" => "Admin",
				"created_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s"),
			],
			[
				"name" => "user",
				"title" => "User",
				"created_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s"),
			]
		];

		DB::table('groups')->delete();
		DB::table('groups')->insert($groups);
	}

}