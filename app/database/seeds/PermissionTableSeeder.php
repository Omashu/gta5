<?php

class PermissionTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$permissions = [
			[
				"name" => "dashboard.index",
				"title" => "Access to dashboard",
				"created_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s"),
			],
			[
				"name" => "auth.login",
				"title" => "Permitted authorization",
				"created_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s"),
			],
			[
				"name" => "blog.editAll",
				"title" => "Редактирование любого блога",
				"created_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s"),
			],
			[
				"name" => "blog.editMy",
				"title" => "Редактирование своего блога",
				"created_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s"),
			],
			[
				"name" => "blog.deleteAll",
				"title" => "Удаление любого блога",
				"created_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s"),
			],
			[
				"name" => "blog.deleteMy",
				"title" => "Удаление своего блога",
				"created_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s"),
			],
		];

		DB::table('permissions')->delete();
		DB::table('permissions')->insert($permissions);

		// adding admin permissions
		$permissionsAll = Permission::all();

		$permissionsUser = ["blog.editMy", "auth.login"];

		$arrays = ["admin" => [], "user" => []];

		$adminGroupId = Group::where("name", "=", "admin")->first()->id;
		$userGroupId = Group::where("name", "=", "user")->first()->id;

		foreach ($permissionsAll as $permission) {
			if (in_array($permission->name, $permissionsUser)) {
				$arrays["user"][] = ["permission_id" => $permission->id, "group_id" => $userGroupId];
			}

			$arrays["admin"][] = ["permission_id" => $permission->id, "group_id" => $adminGroupId];
		}

		foreach ($arrays as $array) {
			if (!$array) continue;
			DB::table("group_permission")->insert($array);
		}
	}

}