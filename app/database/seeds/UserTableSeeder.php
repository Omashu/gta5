<?php

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$users = [
			[
				"username" => "admin",
				"email" => "admin@admin.adm",
				"password" => Hash::make("admin1234"),
				"group_id" => Group::where("name", "=", "admin")->first()->id,
				"created_at" => date("Y-m-d H:i:s"),
				"updated_at" => date("Y-m-d H:i:s"),
			],
		];

		DB::table('users')->delete();
		DB::table('users')->insert($users);
	}

}