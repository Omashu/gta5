<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoteAnswerSelsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("vote_answer_sels", function(Blueprint $table)
		{
			$table->increments('id');

			// к какому опросу принадлежит
			$table->integer("vote_answer_id")->unsigned();
			$table->foreign("vote_answer_id")->references('id')->on('vote_answers')->onDelete("cascade")->onUpdate("cascade");

			// гость по IP, юзер по id
			$table->integer("user_id")->unsigned()->nullable()->default(null);
			$table->foreign("user_id")->references('id')->on('users')->onDelete("cascade")->onUpdate("cascade");

			$table->string("ip_address", 16);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("vote_answer_sels");
	}
}
