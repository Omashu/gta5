<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameVotesTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// удаляем votes таблицы, создаем polls
		Schema::drop('vote_answer_sels');
		Schema::drop('vote_answers');
		Schema::drop('votes');

		Schema::create("polls", function(Blueprint $table)
		{
			$table->increments('id');

			// кто создал опрос
			$table->integer("user_id")->unsigned();
			$table->foreign("user_id")->references('id')->on('users')->onDelete("restrict")->onUpdate("cascade");

			// к чему привязан опрос (к топику или может опрос в комментариях)
			$table->morphs('pollable');

			// кто может отвечать, юзеры, юзеры и гости
			$table->enum('can_use', ["users", "users_and_ghosts"]);

			// тема опроса
			$table->string("title", 254);
			$table->string("dirty_title", 254);

			// uri, для страницы с графиками итп (мб будет мб нет)
			$table->string("slug")->unique();

			// checkbox or radio
			$table->boolean("multiple")->default(false);

			$table->timestamps();
		});

		Schema::create("poll_answers", function(Blueprint $table)
		{
			$table->increments('id');

			// к какому опросу принадлежит
			$table->integer("poll_id")->unsigned();
			$table->foreign("poll_id")->references('id')->on('polls')->onDelete("cascade")->onUpdate("cascade");

			// тема опроса
			$table->string("answer", 254);

			$table->timestamps();
		});

		Schema::create("poll_answer_sels", function(Blueprint $table)
		{
			$table->increments('id');

			// к какому опросу принадлежит
			$table->integer("poll_answer_id")->unsigned();
			$table->foreign("poll_answer_id")->references('id')->on('poll_answers')->onDelete("cascade")->onUpdate("cascade");

			// гость по IP, юзер по id
			$table->integer("user_id")->unsigned()->nullable()->default(null);
			$table->foreign("user_id")->references('id')->on('users')->onDelete("cascade")->onUpdate("cascade");

			$table->string("ip_address", 16);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// удалем polls
		Schema::drop('poll_answer_sels');
		Schema::drop('poll_answers');
		Schema::drop('polls');

		// возвращем таблицы
		Schema::create("votes", function(Blueprint $table)
		{
			$table->increments('id');

			// кто создал опрос
			$table->integer("user_id")->unsigned();
			$table->foreign("user_id")->references('id')->on('users')->onDelete("restrict")->onUpdate("cascade");

			// к чему привязан опрос (к топику или может опрос в комментариях)
			$table->morphs('voteable');

			// кто может отвечать, юзеры, юзеры и гости
			$table->enum('can_use', ["users", "users_and_ghosts"]);

			// тема опроса
			$table->string("title", 254);
			$table->string("dirty_title", 254);

			// uri, для страницы с графиками итп (мб будет мб нет)
			$table->string("slug")->unique();

			// checkbox or radio
			$table->boolean("multiple")->default(false);

			$table->timestamps();
		});

		Schema::create("vote_answers", function(Blueprint $table)
		{
			$table->increments('id');

			// к какому опросу принадлежит
			$table->integer("vote_id")->unsigned();
			$table->foreign("vote_id")->references('id')->on('votes')->onDelete("cascade")->onUpdate("cascade");

			// тема опроса
			$table->string("answer", 254);

			$table->timestamps();
		});

		Schema::create("vote_answer_sels", function(Blueprint $table)
		{
			$table->increments('id');

			// к какому опросу принадлежит
			$table->integer("vote_answer_id")->unsigned();
			$table->foreign("vote_answer_id")->references('id')->on('vote_answers')->onDelete("cascade")->onUpdate("cascade");

			// гость по IP, юзер по id
			$table->integer("user_id")->unsigned()->nullable()->default(null);
			$table->foreign("user_id")->references('id')->on('users')->onDelete("cascade")->onUpdate("cascade");

			$table->string("ip_address", 16);

			$table->timestamps();
		});
	}

}
