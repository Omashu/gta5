<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration {

	public function up()
	{
		Schema::create("feedbacks", function(Blueprint $table)
		{
			$table->increments('id');

			// Был ли это авторизованный пользователь
			$table->integer("user_id")->unsigned()->nullable()->default(null);
			$table->foreign("user_id")->references('id')->on('users')->onDelete("restrict")->onUpdate("cascade");

			// IP адрес клиента
			$table->string("ip_address", 16);

			// E-mail адрес клиента
			$table->string("email", 254);

			// Тема
			$table->string("theme", 254);

			// Сообщение
			$table->text("message");

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop("feedbacks");
	}

}
