<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoteAnswersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("vote_answers", function(Blueprint $table)
		{
			$table->increments('id');

			// к какому опросу принадлежит
			$table->integer("vote_id")->unsigned();
			$table->foreign("vote_id")->references('id')->on('votes')->onDelete("cascade")->onUpdate("cascade");

			// тема опроса
			$table->string("answer", 254);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("vote_answers");
	}

}
