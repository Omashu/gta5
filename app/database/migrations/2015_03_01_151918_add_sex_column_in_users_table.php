<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSexColumnInUsersTable extends Migration {

	public function up()
	{
		Schema::table("users", function(Blueprint $table)
		{
			$table->string("sex")->nullable()->default(null);
		});
	}

	public function down()
	{
		Schema::table("users", function(Blueprint $table)
		{
			$table->dropColumn("sex");
		});
	}

}
