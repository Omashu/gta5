<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("blogs", function(Blueprint $table)
		{
			$table->timestamps();
			$table->increments('id');
			$table->string("title", 254);
			$table->string("dirty_title", 254);
			$table->string("slug")->unique();
			$table->boolean("is_draft")->default(false);

			$table->text("description");
			$table->integer("user_id")->unsigned()->index();
			$table->foreign("user_id")->references('id')->on('users')->onDelete("restrict")->onUpdate("cascade");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("blogs");
	}

}
