<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVoteTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("votes", function(Blueprint $table)
		{
			$table->increments('id');

			// кто создал опрос
			$table->integer("user_id")->unsigned();
			$table->foreign("user_id")->references('id')->on('users')->onDelete("restrict")->onUpdate("cascade");

			// к чему привязан опрос (к топику или может опрос в комментариях)
			$table->morphs('voteable');

			// кто может отвечать, юзеры, юзеры и гости
			$table->enum('can_use', ["users", "users_and_ghosts"]);

			// тема опроса
			$table->string("title", 254);
			$table->string("dirty_title", 254);

			// uri, для страницы с графиками итп (мб будет мб нет)
			$table->string("slug")->unique();

			// checkbox or radio
			$table->boolean("multiple")->default(false);

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("votes");
	}

}
