<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogTopicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("blog_topics", function(Blueprint $table)
		{
			$table->timestamps();
			$table->increments('id');
			$table->integer("views")->default(0);
			$table->string("title", 254);
			$table->string("dirty_title", 254);
			$table->string("slug")->unique();
			$table->integer("rating")->index()->default(0);

			$table->text("description");
			$table->text("short");
			$table->string("cut_title", 64)->nullable()->default(null);
			
			$table->integer("user_id")->unsigned()->index();
			$table->foreign("user_id")->references('id')->on('users')->onDelete("restrict")->onUpdate("cascade");

			$table->integer("blog_id")->unsigned()->index();
			$table->foreign("blog_id")->references('id')->on('blogs')->onDelete("restrict")->onUpdate("cascade");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("blog_topics");
	}

}
