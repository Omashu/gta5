<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMessageRawColumnInCommentsTable extends Migration {

	public function up()
	{
		Schema::table("comments", function(Blueprint $table)
		{
			$table->text("message_raw");
		});
	}

	public function down()
	{
		Schema::table("comments", function(Blueprint $table)
		{
			$table->dropColumn("message_raw");
		});
	}

}
