<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBlogsPermissionPublicAndPermissionPublicMinRatingColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table("blogs", function(Blueprint $table)
		{
			$table->boolean("permission_public")->default(true);
			$table->float("permission_public_min_rating")->default(1);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table("blogs", function(Blueprint $table)
		{
			$table->dropColumn("permission_public");
			$table->dropColumn("permission_public_min_rating");
		});
	}

}
