<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("images", function(Blueprint $table)
		{
			$table->timestamps();
			$table->increments('id');
			$table->string("title", 254);
			$table->string("dirty_title", 254);
			$table->string("slug")->unique();

			// кто загрузил изображение
			$table->integer("user_id")->unsigned()->index();
			$table->foreign("user_id")->references('id')->on('users')->onDelete("restrict")->onUpdate("cascade");

			// описания изображения
			$table->text("description");
			$table->longText("shorts");
				
			// имя файла
			$table->string("filename", 254);
			// размер файла
			$table->integer("bytes");
			$table->integer("width");
			$table->integer("height");
			// формат файла
			$table->string("format", 16);
			$table->string("mime_type", 24);
			// физический путь
			$table->string("path", 254);
		});

		Schema::create("imageables", function(Blueprint $table)
		{
			$table->integer("image_id")->unsigned()->index();
			$table->foreign("image_id")->references('id')->on('images')->onDelete("cascade")->onUpdate("cascade");

			$table->integer("imageable_id")->unsigned()->index();
			$table->string("imageable_type", 254)->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("imageables");
		Schema::drop("images");
	}

}
