<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("permissions", function(Blueprint $table)
		{
			$table->timestamps();
			$table->increments('id');
			$table->string("name", 64)->unique();

			$table->string("title", 254);
			$table->text("description");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("permissions");
	}

}
