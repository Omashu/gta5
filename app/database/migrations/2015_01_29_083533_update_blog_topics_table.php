<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBlogTopicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table("blog_topics", function(Blueprint $table)
		{
			$table->integer("preview_id")->unsigned()->nullable()->default(null);
			$table->foreign("preview_id")->references('id')->on('images')->onDelete("SET NULL")->onUpdate("cascade");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table("blog_topics", function(Blueprint $table)
		{
			$table->dropForeign("blog_topics_preview_id_foreign");
		});
	}

}
