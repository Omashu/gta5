<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatinsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("ratings", function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer("user_id")->unsigned();
			$table->foreign("user_id")->references('id')->on('users')->onDelete("restrict")->onUpdate("cascade");
			$table->integer("value");
			$table->tinyInteger("direction");

			// morph
			$table->integer("ratingable_id")->unsigned()->index();
			$table->string("ratingable_type", 254)->index();

			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("ratings");
	}

}
