<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateBlogsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table("blogs", function(Blueprint $table)
		{
			$table->integer("preview_id")->unsigned()->nullable()->default(null);
			$table->foreign("preview_id")->references('id')->on('images')->onDelete("SET NULL")->onUpdate("cascade");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table("blogs", function(Blueprint $table)
		{
			$table->dropForeign("blogs_preview_id_foreign");
			$table->dropColumn("preview_id");
		});
	}

}
