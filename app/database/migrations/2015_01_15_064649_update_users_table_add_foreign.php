<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTableAddForeign extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table("users", function(Blueprint $table)
		{
			$table->foreign("avatar_id")->references('id')->on('images')->onDelete("SET NULL")->onUpdate("cascade");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table("users", function(Blueprint $table)
		{
			$table->dropForeign("users_avatar_id_foreign");
		});
	}

}
