<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("files", function(Blueprint $table)
		{
			$table->timestamps();
			$table->increments('id');

			$table->string("title", 254);
			$table->string("dirty_title", 254);
			$table->string("slug")->unique();

			// кто загрузил изображение
			$table->integer("user_id")->unsigned()->index();
			$table->foreign("user_id")->references('id')->on('users')->onDelete("restrict")->onUpdate("cascade");

			// описания файла
			$table->text("description");
				
			// имя файла
			$table->string("filename", 254);

			// размер файла
			$table->integer("bytes");

			// формат файла
			$table->string("format", 16);
			$table->string("mime_type", 24);

			// физический путь
			$table->string("path", 254);

			$table->morphs('fileable');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop("files");
	}

}
