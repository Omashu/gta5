<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileDownloadsTable extends Migration {

	public function up()
	{
		Schema::create('file_downloads', function(Blueprint $table) {
			$table->increments('id');
			$table->integer("file_id")->unsigned();
			$table->foreign("file_id")->references('id')->on('files')->onDelete("cascade")->onUpdate("cascade");

			// гость по IP, юзер по id
			$table->integer("user_id")->unsigned()->nullable()->default(null);
			$table->foreign("user_id")->references('id')->on('users')->onDelete("cascade")->onUpdate("cascade");

			$table->string("ip_address", 16);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('file_downloads');
	}

}
