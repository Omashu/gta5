<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("comments", function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer("user_id")->unsigned();
			$table->foreign("user_id")->references('id')->on('users')->onDelete("restrict")->onUpdate("cascade");

			// morph
			$table->integer("commentable_id")->unsigned()->index();
			$table->string("commentable_type", 254)->index();

			$table->timestamps();
			$table->integer("rating")->index()->default(0);

			$table->text("message");

			NestedSet::columns($table);
		});

		Schema::table("comments", function(Blueprint $table)
		{
			$table->foreign("parent_id")->references("id")->on("comments")->onDelete("restrict")->onUpdate("cascade");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table("comments", function(Blueprint $table)
		{
			$table->dropForeign("comments_parent_id_foreign");
		});

		Schema::drop("comments");
	}

}
