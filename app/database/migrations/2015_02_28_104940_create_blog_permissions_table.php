<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogPermissionsTable extends Migration {

	public function up()
	{
		Schema::create("blog_permissions", function(Blueprint $table)
		{
			$table->increments('id');

			// связь на блог
			$table->integer("blog_id")->unsigned();
			$table->foreign("blog_id")->references('id')->on('blogs')->onDelete("cascade")->onUpdate("cascade");

			// пользователь, чьи права "обсуждаются"
			$table->integer("user_id")->unsigned();
			$table->foreign("user_id")->references('id')->on('users')->onDelete("cascade")->onUpdate("cascade");

			// публикация топиков (permission_public не учитывается) или запрет на публикацию
			$table->boolean("flag_write")->default(false);

			// модерация топиков, возможность отправлять топики обратно авторам
			$table->boolean("flag_moderate")->default(false);

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('blog_permissions');
	}

}
