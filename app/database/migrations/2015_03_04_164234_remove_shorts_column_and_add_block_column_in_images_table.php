<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveShortsColumnAndAddBlockColumnInImagesTable extends Migration {

	public function up()
	{
		Schema::table("images", function(Blueprint $table)
		{
			$table->dropColumn("shorts");
			$table->boolean("block")->default(false);
		});
		DB::table("images")
			->where("path", "/app/frontend/images/avatars/")
			->delete();
	}

	public function down()
	{
		Schema::table("images", function(Blueprint $table)
		{
			$table->dropColumn("block");
			$table->longText("shorts");
		});
	}

}
