<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create("users", function(Blueprint $table)
		{
			$table->increments('id');
			$table->string("username", 24)->unique();
			$table->string("password", 254);
			$table->string("email", 254)->unique();
			$table->rememberToken();

			$table->integer("group_id")->unsigned();
			$table->string("first_ip", 16);
			$table->string("last_ip", 16);

			$table->string("first_name", 64);
			$table->string("last_name", 64);
			$table->text("about_myself");

			// email confirm
			$table->boolean('confirmed');
			$table->string("confirmation_code", 254);
			$table->dateTime('confirmation_at');

			// foreign to images table
			$table->integer("avatar_id")->unsigned()->nullable()->default(null);

			// account write block, is_write? ok, no? - go home bitch
			$table->boolean("write_only")->default(1);
			// блокировка прав на возможность публиковать С - ПО, если write_only = false, и read_only_to = null, значит перманент
			$table->dateTime("read_only_from")->default(null)->nullable();
			$table->dateTime("read_only_to")->default(null)->nullable();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
