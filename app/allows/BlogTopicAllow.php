<?php namespace Allow;

use System\AllowException;

class BlogTopic extends \System\Allow {
	// может ли юзер создать новый топик
	public static function add(\User $user = null, $throwIfDeny = false)
	{
		if (!self::checkUser($user, $throwIfDeny))
			return false;

		$permissions = $user->group->permissions;

		// не заблокирован и есть право на создание
		if (!$permissions->allow("blogTopic.add"))
		{
			if ($throwIfDeny)
				throw new AllowException(\Lang::get("frontend/message.denyBlogTopicAdd"));

			return false;
		}

		return true;
	}

	public static function edit(\User $user = null, \BlogTopic $blogTopic, $throwIfDeny = false)
	{
		if (!self::checkUser($user, $throwIfDeny))
			return false;

		// топик удален
		if ($blogTopic->trashed())
		{
			if ($throwIfDeny)
				throw new AllowException("Топик удален, его невозможно отредактировать.");

			return false;
		}

		$permissions = $user->group->permissions;

		if ($permissions->allow("blogTopic.editAll"))
			return true;

		if ((int) $user->id === (int) $blogTopic->user_id AND $permissions->allow("blogTopic.editMy"))
			return true;

		if ($throwIfDeny)
			throw new AllowException("У вас нет прав на редактирование этого топика.");

		return false;
	}

	public static function delete(\User $user = null, \BlogTopic $blogTopic, $throwIfDeny = false)
	{
		if (!self::checkUser($user, $throwIfDeny))
			return false;

		// топик удален
		if ($blogTopic->trashed())
		{
			if ($throwIfDeny)
				throw new AllowException("Топик уже удален.");

			return false;
		}

		$permissions = $user->group->permissions;

		if ($permissions->allow("blogTopic.deleteAll"))
			return true;

		if ((int) $user->id === (int) $blogTopic->user_id AND $permissions->allow("blogTopic.deleteMy"))
			return true;

		if ($throwIfDeny)
			throw new AllowException("У вас нет прав на удаление этого топика.");

		return false;
	}
}