<?php namespace Allow;

use System\AllowException;

class Blog extends \System\Allow {

	// может ли юзер создать новый блог
	public static function add(\User $user = null, $throwIfDeny = false)
	{
		if (!self::checkUser($user, $throwIfDeny))
			return false;

		$permissions = $user->group->permissions;

		// не заблокирован и есть право на создание
		if (!$permissions->allow("blog.add"))
		{
			if ($throwIfDeny)
				throw new AllowException(\Lang::get("frontend/message.denyBlogAdd"));

			return false;
		}

		return true;
	}

	// может ли отредактировать блог
	public static function edit(\User $user = null, \Blog $blog, $throwIfDeny = false)
	{
		// может ли этот юзер редактировать этот блог
		// редактировать блог может только его создатель (с правами blog.editMy) или чувак с правами редактировать все (blog.editAll)
		// аккаунт должен быть не заблокированным!

		if (!self::checkUser($user, $throwIfDeny))
			return false;

		$permissions = $user->group->permissions;

		// чекнем наличие супер-права
		if ($permissions->allow("blog.editAll"))
			return true;

		// дальше только если блог твой
		if ((int) $user->id !== (int) $blog->user_id)
		{
			if ($throwIfDeny)
				throw new AllowException("Это чужой блог.");

			return false;
		}

		// и есть право на редактирование своего
		if ($permissions->allow("blog.editMy"))
			return true;

		if ($throwIfDeny)
			throw new AllowException("У вас нет прав на редактирование этого блога.");

		return false;
	}

	public static function delete(\User $user = null, \Blog $blog, $throwIfDeny = false)
	{
		// !!! При удалении блога все его топики перемещаются в черновики к создателям!

		if (!self::checkUser($user, $throwIfDeny))
			return false;

		// черновой блог удалить невозможно
		if ($blog->is_draft)
		{
			if ($throwIfDeny)
				throw new AllowException("Черновой блог удалить не возможно.");

			return false;
		}

		$permissions = $user->group->permissions;

		// чекнем наличие супер-права
		if ($permissions->allow("blog.deleteAll"))
			return true;

		// дальше только если блог твой
		if ((int) $user->id !== (int) $blog->user_id)
		{
			if ($throwIfDeny)
				throw new AllowException("Это чужой блог.");

			return false;
		}

		// и есть право на удаление своего
		if ($permissions->allow("blog.editMy"))
			return true;

		if ($throwIfDeny)
			throw new AllowException("У вас нет прав на удаление этого блога.");

		return false;
	}

	// может ли юзер опубликовать топик в этот блог
	public static function newTopic(\User $user = null, \Blog $blog, $throwIfDeny = false)
	{
		if (!self::checkUser($user, $throwIfDeny))
			return false;

		// админ блога
		if ((int) $user->id === (int) $blog->user_id)
			return true;

		// публиковать в черновые блоги других пользователей невозможно
		if ($blog->is_draft)
		{
			if ($throwIfDeny)
				throw new AllowException("Публиковать в черновые блоги других юзеров нельзя.");

			return false;
		}

		// проверим наличие записи в blog_permissions
		$blogPermission = $blog->permissions()->where("user_id", $user->getKey())->first();

		if ($blogPermission)
		{
			// имеет флаг публикации в блог
			if ($blogPermission->flag_write)
				return true;

			// администратор блога запретил этому юзеру публиковать
			if ($throwIfDeny)
				throw new AllowException("Администратор блога запретил вам публиковать в этот блог.");

			return false;
		}

		// блог не публичный, подписчики не могут в него публиковать
		if (!$blog->permission_public)
		{
			if ($throwIfDeny)
				throw new AllowException("Администратор блога отключил возможность публикации для подписчиков.");

			return false;
		}

		// у подписчика мало рейтинга
		if ($user->rating < $blog->permission_public_min_rating)
		{
			if ($throwIfDeny)
				throw new AllowException("Минимальный рейтинг для публикации ".$blog->permission_public_min_rating.", а у вас сейчас ".$user->rating.".");

			return false;
		}

		// является ли подписчиком
		if ($blog->userIsSubscribed($user))
			return true;

		// не является подписчиком
		if ($throwIfDeny)
			throw new AllowException("Вы не подписаны на этот блог.");

		return false;
	}
}