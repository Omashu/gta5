<?php namespace System;

class Allow {
	public static function checkUser(\User $user = null, $throwIfDeny)
	{
		if (!$user)
		{
			if ($throwIfDeny)
				throw new AllowException(\Lang::get("frontend/message.regOrLogin"));

			return false;
		}

		// акк заблокирован
		if (!$user->isWriteOnly())
		{
			if ($throwIfDeny)
				throw new AllowException($user->read_only_to
					? \Lang::get("frontend/message.bannedTo", ["date" => \Date::parse($user->read_only_to)->format("Y-m-d H:i")])
					: \Lang::get("frontend/message.bannedPerm"));

			return false;
		}

		return true;
	}
}