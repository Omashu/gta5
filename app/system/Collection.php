<?php namespace System;

class Collection extends \Illuminate\Database\Eloquent\Collection {

	public function getAssoc($k = "id", $v = "title", array $unshift = [])
	{
		$values = [];
		foreach ($unshift as $_k => $_v)
		{
			$values[$_k] = $_v;
		}

		foreach ($this->items as $item)
		{
			$values[$item->$k] = $item->$v;
		}

		return $values;
	}
}