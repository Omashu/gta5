<?php namespace System;
use HTMLPurifier;
use HTMLPurifier_Config;
use Config;

class Purifier {
	protected $objects = [];
	protected static $instance = null;

	public static function getObject($configKey)
	{
		if (is_null(self::$instance))
			self::$instance = new Purifier;

		$inst = self::$instance;

		if (isset($inst->objects[$configKey]))
			return $inst->objects[$configKey];

		$pConfigs = Config::get("purifier");
		if (!isset($pConfigs[$configKey]))
			throw new \Exception("Undefined purifier config key");

		$pConfigValues = $pConfigs[$configKey];

		$config = HTMLPurifier_Config::createDefault();

		// папка для кеша по умолчанию
		$config->set('Cache.SerializerPath', storage_path("purifier"));

		foreach ($pConfigValues as $key => $value) {
			$config->set($key, $value);
		}

		$customMethodName = "Custom_" . ucfirst($configKey);

		if (method_exists($inst, $customMethodName))
			$inst->$customMethodName($config);

		$object = new HTMLPurifier($config);

		return $inst->objects[$configKey] = $object;
	}

	protected function Custom_BlogTopicDescription(HTMLPurifier_Config $config)
	{
		$def = $config->getHTMLDefinition(true);
		$def->addElement("cut", "Block", "Inline", "Common", []);
	}
}