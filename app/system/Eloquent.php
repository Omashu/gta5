<?php namespace System;

class Eloquent extends \Eloquent {

	// defaults
	protected static $rules = [];

	/**
	 * Get model rules
	 * 
	 * @param NULL|string $scenario ALL/scenario
	 * @param array $replace
	 * @return array
	 */
	public static function getRules($scenario = NULL, array $replace = [])
	{
		$rules = [];
		$complReplace = [];

		foreach ($replace as $key => $value)
		{
			$complReplace["{".$key."}"] = $value;
		}

		foreach (static::$rules as $column => $values)
		{
			foreach ($values as $value)
			{
				if (is_null($scenario) 
					OR (isset($value[1]) and is_array($value[1]) and in_array($scenario, $value[1])))
				{
					$rules[$column][] = $complReplace
						? str_replace(array_keys($complReplace), array_values($complReplace), $value[0])
						: $value[0];
				}
			}
		}

		return $rules;
	}

}