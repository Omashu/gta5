<?php

/**
 * Helpers
 */
function redirectBackOrHome()
{
	if (!Request::header('referer'))
		return Redirect::home();

	return Redirect::back();
}

function formatBytes($B, $D=2) {
	$S = 'BkMGTPEZY';
	$F = floor((strlen($B) - 1) / 3);
	return sprintf("%.{$D}f", $B/pow(1024, $F)).' '.@$S[$F].'B';
}

function tagNameDisplayer($name)
{
	return \HTML::entities(\Str::title($name));
}

function tagNamesChecker($value)
{
	if (is_string($value))
		$value = explode(",", $value);

	if (!is_array($value))
		return [];

	$newTags = [];

	foreach ($value as $name)
	{
		if (!is_string($name))
			continue;

		$name = trim($name);
		if (!mb_strlen($name))
			continue;

		$newTags[] = $name;
	}

	return $newTags;
}

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Auth::guest())
	{
		if (Request::ajax())
		{
			return Response::make('Unauthorized', 401);
		}
		else
		{
			return Redirect::guest('login');
		}
	}
});


Route::filter('auth.basic', function()
{
	return Auth::basic();
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Auth::check()) return Redirect::to('/');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() !== Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});

/**
 * Фильтры доступов к разделам
 */
Route::filter("allow.blog.add", function()
{
	\Allow\Blog::add(Auth::user(), true);
});

Route::filter("allow.blogTopic.add", function()
{
	\Allow\BlogTopic::add(Auth::user(), true);
});

App::error(function(System\AllowException $e)
{
	Message::danger($e->getMessage());
	return redirectBackOrHome();
});