<?php

/**
 * Dashdoard Global Settings
 */

return [
	"title" => Lang::get("frontend/front._"),
	"description" => "Пивко под ГТА. Наблюдения в GTA 5 :) Мастер составлять мета в ГТА.",
	"keywords" => "GTA 5 моды, ГТА 5, прохождение, части, дата выхода, системные требования",

	"styles" => [
		"http://fonts.googleapis.com/css?family=Ubuntu+Condensed|Open+Sans:700italic,400,700&subset=latin,cyrillic-ext,cyrillic,latin-ext",

		"/app/frontend/bootstrap/css/bootstrap.min.css",
		"/app/frontend/bootstrap/css/bootstrap-theme.min.css",
		"/app/frontend/style.css",

		"//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css",

		// graph
		"//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css",

		// colorbox
		"/app/system/colorbox/example5/colorbox.css",

		// bootstrap dialog
		"/app/system/bootstrap3-dialog/dist/css/bootstrap-dialog.min.css",
	],
	"scripts" => [
		"https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js",
		"/app/frontend/bootstrap/js/bootstrap.min.js",

		// ckeditor
		"/app/system/ckeditor/ckeditor.js",

		// colorbox
		"/app/system/colorbox/jquery.colorbox-min.js",

		"/app/system/ajax.js",
		"/app/system/upload.js",
		"/app/frontend/template.js",

		"/app/frontend/AButtons.js",
		"/app/frontend/MediaObject.js",
		"/app/frontend/CKEDITOR.js",
		"/app/frontend/Blog.js",
		"/app/frontend/BlogTopic.js",
		"/app/frontend/Comment.js",
		"/app/frontend/Subscribe.js",
		"/app/frontend/Rating.js",
		"/app/frontend/Button.js",
		"/app/frontend/Poll.js",
		"/app/frontend/Tag.js",
		"/app/frontend/User.js",

		// croppa
		"/packages/bkwld/croppa/js/croppa.js",

		// graph
		"//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js",
		"//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js",

		// autocomplete
		"/app/system/jQuery-Autocomplete/dist/jquery.autocomplete.min.js",

		// fileupload
		"/app/system/jQuery-File-Upload/js/vendor/jquery.ui.widget.js",
		"/app/system/JavaScript-Load-Image/js/load-image.all.min.js",
		"/app/system/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js",
		"/app/system/jQuery-File-Upload/js/jquery.iframe-transport.js",
		"/app/system/jQuery-File-Upload/js/jquery.fileupload.js",
		"/app/system/jQuery-File-Upload/js/jquery.fileupload-process.js",
		"/app/system/jQuery-File-Upload/js/jquery.fileupload-image.js",
		"/app/system/jQuery-File-Upload/js/jquery.fileupload-validate.js",

		// bootstrap dialog
		"/app/system/bootstrap3-dialog/dist/js/bootstrap-dialog.min.js",

		// выравнивание блоков
		"/app/system/jquery-match-height/jquery.matchHeight-min.js",

		["http://static.aninova.ru/libs/js/html5.js", ["before" => "<!--[if lt IE 9]>", "after" => "<![endif]-->"]],
	],
];