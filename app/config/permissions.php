<?php

// перечислен список уровней системы, эти уровни удалить нельзя и они буду синхронизированы (добавлены) в случае отсутствия
return [
	[
		"name" => "dashboard.index",
		"title" => "Вход в админ панель"
	],
	[
		"name" => "auth.login",
		"title" => "Авторизация на сайте",
	],
	// блоги
	[
		"name" => "blog.add",
		"title" => "Создание своего нового блога",
	],
	[
		"name" => "blog.editAll",
		"title" => "Редактирование любых блогов",
	],
	[
		"name" => "blog.editMy",
		"title" => "Редактирование своих блогов",
	],
	[
		"name" => "blog.deleteAll",
		"title" => "Удаление любых блогов",
	],
	[
		"name" => "blog.deleteMy",
		"title" => "Удаление своих блогов",
	],
	// топики в блогах
	[
		"name" => "blogTopic.add",
		"title" => "Создание своего нового топика"
	],
	[
		"name" => "blogTopic.editAll",
		"title" => "Редактирование любых топиков"
	],
	[
		"name" => "blogTopic.editMy",
		"title" => "Редактирование своих топиков",
	],
	[
		"name" => "blogTopic.deleteAll",
		"title" => "Удаление любых топиков"
	],
	[
		"name" => "blogTopic.deleteMy",
		"title" => "Удаление своих топиков",
	],
	// комментарии
	[
		"name" => "comment.setSpamFlagAll",
		"title" => "Установка спам-флага любым комментариям (скрывается) мгновенно",
	],
	[
		"name" => "comment.upSpamLevel",
		"title" => "Возможность повышать спам-уровень комментариям",
	]
];