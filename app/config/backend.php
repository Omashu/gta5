<?php

/**
 * Dashdoard Global Settings
 */

return [
	"title" => Lang::get("backend/front._"),
	"prefix" => "dashboard", // uri

	"styles" => [
		"http://fonts.googleapis.com/css?family=Ubuntu+Condensed|Open+Sans:700italic,400,700&subset=latin,cyrillic-ext,cyrillic,latin-ext",

		"/app/backend/bootstrap/css/bootstrap.min.css",
		"/app/backend/bootstrap/css/bootstrap-theme.min.css",
		"/app/backend/style.css",

		"//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css",

		// graph
		"//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css",

		// datepicker
		"/app/system/datepicker/css/datepicker.css",
	],
	"scripts" => [
		"https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js",
		"/app/backend/bootstrap/js/bootstrap.min.js",

		"/app/system/ajax.js",
		"/app/backend/indexGraphs.js",
		"/app/backend/template.js",

		// graph
		"//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js",
		"//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js",

		// datepicker
		"/app/system/datepicker/js/bootstrap-datepicker.js",
	],
];