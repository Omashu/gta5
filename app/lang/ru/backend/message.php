<?php

return [
	"adminGroupWasNoFound" => "Группа администраторов не обнаружена, регистрация не возможна.",

	"groupCreated" => "Группа :title успешно создана",
	"groupUpdated" => "Данные группы :title успешно обновлены",
	"groupTransfUndefined" => "Неверно указана группа для переноса",
	"groupDeleted" => "Группа :title успешно удалена",
	"groupDefault" => "Группа :title установлена группой по умолчанию",

	"blogCreated" => "Блог :title успешно создан",

	"userUpdated" => "Данные пользователя :username обновлены",
];