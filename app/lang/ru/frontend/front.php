<?php

return [
	"_" => "GTA 5, моды, прохождение, дата выхода, системные требования, новости, rockstar",

	"home" => [
		"index" => "",
	],
	"blog" => [
		"topic" => [
			"create" => "Создание топика",
		],
		"create" => "Создание блога",
		"indexPopular" => "Популярные блоги",
	],
	"user" => [
		"blogs" => "Блоги :username",
		"topics" => "Топики :username",
		"images" => "Изображения :username",
	],
	"login" => "Вход на сайт",
	"register" => "Регистрация",
	"remind" => "Забыл пароль",
	"reset" => "Новый пароль",
];