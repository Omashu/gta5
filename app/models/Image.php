<?php

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Image extends System\Eloquent implements SluggableInterface {

	use SluggableTrait;

	protected static $uploadLastValidator = NULL;
	protected $sluggable = [];

	public static function boot()
	{
		parent::boot();
		Image::observe(new ImageObserver);
	}

	public function blogs()
	{
		return $this->morphedByMany('Blog', 'imageable');
	}

	public function blogTopics()
	{
		return $this->morphedByMany('BlogTopic', 'imageable');
	}

	public function users()
	{
		return $this->morphedByMany('User', 'imageable');
	}

	// пользователь
	public function user()
	{
		return $this->hasOne('User', 'id', 'user_id');
	}

	public static function getValidator()
	{
		return static::$uploadLastValidator;
	}

	public static function upload(Symfony\Component\HttpFoundation\File\UploadedFile $file)
	{
		$mainAbsPath = public_path() . "/" . trim(Config::get("app.uploads.images"), "/") . "/";
		$mainRelPath = "/" . trim(Config::get("app.uploads.images"), "/") . "/";

		$validator = Validator::make(["file" => $file], ["file" => "required|image|max:5000|mimes:png,gif,jpeg,jpg"]);

		if ($validator->fails())
		{
			static::$uploadLastValidator = $validator;
			return FALSE;
		}

		$ext = $file->getClientOriginalExtension();

		$filename = preg_replace("/\.{$ext}$/", "", $file->getClientOriginalName());

		// чистим название
		$title = $filename;
		$title = str_replace("-", " ", $title);
		$title = str_replace("_", " ", $title);
		$title = preg_replace("/\s\d+$/", "", $title);
		$title = preg_replace("/^\d+\s/", "", $title);
		$title = trim($title);

		$filename = trim($filename);
		if (!$filename) $filename = Str::random();

		$filename = Slug::make($filename);
		$filename = substr($filename, 0, 62);
		// добавим это окончание, чтобы разграничить название и параметры у миниатюр
		$filename .= "_".Str::random(1);

		// из хеша получим директории хранения
		$filenameHash = md5($filename);

		$folders = [substr($filenameHash, 0, 2), substr($filenameHash, 2, 2)];

		$fullAbsPath = $mainAbsPath . implode("/", $folders) . "/";
		$fullRelPath = $mainRelPath . implode("/", $folders) . "/";

		do {
			$filename = Str::random(2) . "-" . $filename;
		} while(file_exists($fullAbsPath . $filename . "." . $ext));

		// создаем сущность
		$image = new Image();
		$image->user_id = Auth::user()->id;
		$image->title = $title;
		$image->filename = $filename;
		$image->mime_type = $file->getMimeType();
		$image->format = $ext;

		// переносим файл
		$file->move($fullAbsPath, $filename . "." . $ext);

		$fileAbsPath = $fullAbsPath . $filename . "." . $ext;
		$fileRelPath = $fullRelPath . $filename . "." . $ext;

		$image->path = $fullRelPath;

		// получаем доп. инфу
		list($width, $height) = @getimagesize($fileAbsPath);
		if (!$width) $width = 0;
		if (!$height) $height = 0;

		// готово ;)
		$image->bytes = filesize($fileAbsPath);
		$image->width = $width;
		$image->height = $height;
		$image->save();

		return $image;
	}

	public function setTitleAttribute($value)
	{
		$this->attributes['dirty_title'] = $value;
		$this->attributes['title'] = HTML::entities($value);
	}

	public function setDescriptionAttribute($value)
	{
		$purifier = Purifier::getObject("imageDescription");
		$value = $purifier->purify($value);

		$this->attributes['description'] = $value;
	}

	public function getHtmlLink($href = "#", $title = null, array $attrs = [], $entities = false)
	{
		$url = url($href, [], null);
		if (is_null($title) || $title === false) $title = $url;

		return '<a href="'.$url.'"'.HTML::attributes($attrs).'>'. ($entities ? $this->entities($title) : $title) .'</a>';
	}

	public function getHtmlImage($src, $alt = null, array $attrs = [])
	{
		return HTML::image($src, $alt, $attrs);
	}

	public function getRelPath()
	{
		return $this->path . $this->filename . "." . $this->format;
	}

	public function getUrl($width = null, $height = null, array $options = [])
	{
		if (is_null($width) and is_null($height) and !$options) return asset($this->getRelPath());

		return \Croppa::url($this->getRelPath(), $width, $height, $options);
	}

	public function resetCrops()
	{
		\Croppa::reset($this->getRelPath());
	}

	// изображения юзера, не прикрепленные
	public static function userNonUsed(User $user)
	{
		return Image::where(function($q) use($user)
		{
			$q->where("user_id", $user->getKey());
			$q->where("block", false);
			$q->whereNotExists(function($q)
			{
				$q->select(\DB::raw(1))
					->from('imageables')
					->whereRaw('images.id = imageables.image_id');
			});
		});
	}

	// удалить изображение в случае, если оно больше нигде не используется
	public function deleteIfNonUsed()
	{
		// это изображение нельзя удалить
		if ($this->block)
			return false;

		$check = DB::table("images")
			->where("id", $this->getKey())
			->whereExists(function($q)
			{
				$q->select(DB::raw(1))
					->from('imageables')
					->whereRaw('images.id = imageables.image_id');
			})
			->count();

		if ($check)
			return false;

		$this->delete();
		return true;
	}
}
