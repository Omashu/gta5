<?php

class Group extends System\Eloquent {

	protected $fillable = ["title", "description"];
	protected $guarded = ["id", "name", "created_at", "updated_at"];

	protected static $rules = [
		"name" => [
			["required", ["create", "update"]],
			["min:1", ["create", "update"]],
			["max:16", ["create", "update"]],
			["alpha", ["create", "update"]],
			["unique:groups,name", ["create"]],
			["unique:groups,name,{id}", ["update"]],
		],
		"title" => [
			["required", ["create", "update"]],
		],
	];

	public function newCollection(array $models = [])
	{
		return new System\Collection($models);
	}

	/* Relations */
	public function permissions()
	{
		return $this->belongsToMany('Permission');
	}

	public function users()
	{
		return $this->hasMany('User');
	}

	public function allow($name, $true = true, $false = false)
	{
		return $this->permissions->allow($name, $true, $false);
	}
}
