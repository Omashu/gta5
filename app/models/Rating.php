<?php

class Rating extends Eloquent {

	use SoftDeletingTrait;

	protected $guarded = ["*"];
	protected $dates = ['deleted_at'];

	public function ratingable()
	{
		return $this->morphTo();
	}
}
