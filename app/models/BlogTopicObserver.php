<?php

class BlogTopicObserver {

	public function created($model)
	{
		// создан новый топик, необходимо всем подписчикам сообщить
		$model->blog->subscribes()->where("user_id", "!=", $model->user_id)->increment("new", 1);
	}

	public function updated($model)
	{
		// сбрасываем кеши описания
		Cache::pull($model->getCacheKeyDescriptionCleaned());
		Cache::pull($model->getCacheKeyShortDescriptionCleaned());
	}
}