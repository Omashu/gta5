<?php

class BlogTopicDescriptionReplacer {

	protected $storage = [];
	protected static $inst;

	public static function replace($content, $cacheKey)
	{
		if (!isset(static::$inst))
			static::$inst = new BlogTopicDescriptionReplacer();

		$inst = static::$inst;
		$hash = md5($content);

		if (isset($inst->storage[$hash]))
			return $inst->storage[$hash];

		if (Cache::has($cacheKey))
			return $inst->storage[$hash] = Cache::get($cacheKey);

		$inst->removeCut($content);
		$inst->removeEmptyP($content);
		$inst->replaceImage($content);
		$inst->replaceIframe($content);
		$inst->replaceTable($content);
		// TODO: добавить замену @username
		// $inst->replaceUsername($content);

		// save cache
		Cache::forever($cacheKey, $content);

		// return content
		return $inst->storage[$hash] = $content;
	}

	protected function removeCut(&$content)
	{
		// вырезаем cut
		$content = preg_replace("/<cut.*<\/cut>/Uum", "", $content);
	}

	protected function removeEmptyP(&$content)
	{
		// пустые параграфы
		$content = preg_replace("/<p>\s<\/p>/s", "", $content);
		$content = preg_replace("/<p><br \/>/s", "<p>", $content);
	}

	protected function replaceImage(&$content)
	{
		// обновляем изображения в тексте, оборачиваем в .thumbnail и вставляем текст
		preg_match_all("/<img.*\/>/Uum", $content, $matches);

		if (!$matches OR !isset($matches[0]))
			return;

		// обновляем вид изображений
		foreach ($matches[0] as $imageHtml)
		{
			$newImageHtml = null;
			$imageBlock = "";
			$oImage = null;

			// смотрим float, если нет то по центру
			preg_match("/float:(left|right);/", $imageHtml, $float);

			// выравнивание по левому или правому краю
			if (isset($float[1])) $float = $float[1];
			else $float = null;

			// смотрим наше ли это изображение
			preg_match("/src=\"(?<path>.*)\/(?<filename>.*?)\.(?<format>.*?)\"/", $imageHtml, $imageSrcData);

			if ($imageSrcData)
			{
				// find image
				$oImage = Image::where("path", $imageSrcData["path"]."/")->where("filename", $imageSrcData["filename"])->where("format", $imageSrcData["format"])->first();

				if ($oImage)
				{
					if ($float)
					{
						$newImageHtml = $oImage->getHtmlLink($oImage->getUrl(),
							$oImage->getHtmlImage($oImage->getUrl(300, null, ["crop"]),
								$oImage->title,
								[
									"class" => "thumbnail",
									"align" => $float
								]),
								[
									"title" => $oImage->title,
									"class" => "blog-topic-image-link blog-topic-image-link_pull_".$float
								]);
					} else
					{
						$newImageHtml = $oImage->getHtmlLink($oImage->getUrl(), $oImage->getHtmlImage($oImage->getUrl(845, null, ["crop"]), $oImage->title, ["class" => "thumbnail thumbnail-inline"]), ["title" => $oImage->title, "class" => "blog-topic-image-link blog-topic-image-link_pull_center"]);
					}
				}
			}

			if (!$oImage) {
				// изобажение не найдено в БД
				$newImageHtml = $imageHtml;
				// добавим классы
				$newImageHtml = str_replace("<img", '<img class="thumbnail'.($float?"":" thumbnail-inline").'"', $newImageHtml);

				if ($float)
					$newImageHtml = str_replace("<img", '<img align="'.$float.'"', $newImageHtml);
			}

			if (!$float) {
				// по центру
				$imageBlock .= '<span class="clearfix center-block text-center">';
				$imageBlock .= $newImageHtml;
				$imageBlock .= '</span>';
			} else {
				$imageBlock = $newImageHtml;
			}

			$content = str_replace($imageHtml, $imageBlock, $content);
		}
	}

	protected function replaceIframe(&$content)
	{
		// обрабатываем ифреймы (с видео)
		preg_match_all("/<iframe.*<\/iframe>/U", $content, $matches);

		if (!$matches OR !isset($matches[0]))
			return;

		foreach ($matches[0] as $iframe)
		{
			$defIframe = $iframe;
			$bSoundcloud = (bool) preg_match("/soundcloud/", $iframe);

			$newIframe = '<span class="blog-topic-iframe blog-topic-iframe_'.($bSoundcloud?"soundcloud":"xz").'">';
			$newIframe .= $defIframe;
			$newIframe .= '</span>';

			$content = str_replace($defIframe, $newIframe, $content);
		}
	}

	protected function replaceTable(&$content)
	{
		$content = str_replace("<table>", '<table class="table">', $content);
	}

}