<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use System\Purifier;

class User extends System\Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;
	use ImageTrait;

	// для синхронизации колонки с прикрепленными изображениями
	protected $imageIdColumns = ["avatar_id"];

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	protected $fillable = ["first_name", "last_name", "about_myself", "sex"];
	protected $guarded = ["*"];

	protected static $rules = [
		"username" => [
			["required", ["create", "update"]],
			["min:3", ["create", "update"]],
			["max:24", ["create", "update"]],
			["alpha_dash", ["create", "update"]],
			["unique:users,username", ["create"]],
			["unique:users,username,{id}", ["update"]],
		],
		"email" => [
			["required", ["create", "update"]],
			["email", ["create", "update"]],
			["unique:users,email", ["create"]],
			["unique:users,email,{id}", ["update"]],
		],
		"group_id" => [
			["required", ["create", "update"]],
			["exists:groups,id", ["create", "update"]],
		],
		"avatar_id" => [
			["exists:images,id", ["create", "update"]],
		],
		"password" => [
			["required", ["create"]],
			["confirmed", ["create", "update"]],
			["min:6", ["create", "update"]],
		]
	];

	public static function boot()
	{
		parent::boot();
		User::observe(new UserObserver);
	}

	public static function getSortColumns()
	{
		return [
			"id" => "ID",
			"username" => "Логин",
			"email" => "E-mail",
			"confirmation_at" => "Дата подтверждения e-mail'a",
			"created_at" => "Дата регистрации",
			"updated_at" => "Дата входа",
		];
	}

	public function getFullnameAttribute()
	{
		$val = trim($this->first_name . " " . $this->last_name);
		if ($val) return $val;
		return null;
	}

	public function getSexAttribute($value)
	{
		$sex = Config::get("app.user.sex", []);
		if (!is_array($sex) OR !isset($sex[$value]))
			return null;

		return $value;
	}

	public function getSexLangAttribute()
	{
		$sex = Config::get("app.user.sex", []);
		if (!is_array($sex) OR !isset($sex[$this->sex]))
			return null;

		return $sex[$this->sex];
	}

	public function getSexArrayAttribute()
	{
		$sex = Config::get("app.user.sex", []);
		if (!is_array($sex))
			return [];

		$array = [null => "Не знаю"];

		foreach ($sex as $sexKey => $sexValue) {
			$array[$sexKey] = $sexValue;
		}
		return $array;
	}

	public function setSexAttribute($value)
	{
		$sex = Config::get("app.user.sex", []);
		if (!is_array($sex) OR !isset($sex[$value]))
		{
			$this->attributes['sex'] = null;
		} else
		{
			$this->attributes["sex"] = $value;
		}
	}

	public function setFirstNameAttribute($value)
	{
		$this->attributes["first_name"] = HTML::entities($value);
	}

	public function setLastNameAttribute($value)
	{
		$this->attributes["last_name"] = HTML::entities($value);
	}

	public function setAboutMyselfAttribute($value)
	{
		$purifier = Purifier::getObject("userAboutMyself");
		$value = $purifier->purify($value);

		$this->attributes['about_myself'] = $value;
	}

	/* Relations */
	public function group()
	{
		return $this->hasOne('Group', 'id', 'group_id');
	}

	public function avatar()
	{
		if (!$this->avatar_id AND $this->getKey())
		{
			while (true)
			{
				// не используются дефолтные аватары
				if (!Config::get("app.user.avatars"))
					break;

				$avatarsNameRange = Config::get("app.user.avatarsNameRange");
				if (!$avatarsNameRange OR !is_array($avatarsNameRange))
					break;

				$avatars = "/" . trim(Config::get("app.user.avatars"), "/") . "/";
				$avatarsFormat = Config::get("app.user.avatarsFormat", "png");

				// генерируем имя автарки для юзера
				$fileName = mt_rand($avatarsNameRange[0], $avatarsNameRange[1]);
				$absPath = public_path($avatars.$fileName.".".$avatarsFormat);

				// нет такого файла, аватара
				if (!File::exists($absPath) OR !File::isFile($absPath))
					break;

				// ищем в БД строку с аватаром, может она уже есть
				$image = Image::where("path", $avatars)
					->where("filename", $fileName)
					->where("format", $avatarsFormat)
					->first();

				if ($image)
				{
					$this->avatar_id = $image->getKey();
					$this->save();

					// синхронизируем, делая видимую связь
					$this->imagesSyncFromArray();

					// готово
					break;
				}

				// сохраним юзеру аватарку
				$image = new Image();
				$image->format = $avatarsFormat;
				$image->path = $avatars;
				$image->filename = $fileName;
				$image->user_id = $this->getKey();
				$image->title = $fileName;
				$image->bytes = File::size($absPath);
				// нельзя удалить, изменить и не выводится в списке изображений юзера
				$image->block = true;

				// получаем доп. инфу
				list($width, $height) = @getimagesize($absPath);
				if (!$width) $width = 0;
				if (!$height) $height = 0;

				$image->width = $width;
				$image->height = $height;
				$image->mime_type = Config::get("app.user.avatarsMimeType");
				$image->save();

				$this->avatar_id = $image->getKey();
				$this->save();

				// синхронизируем изображения
				$this->imagesSyncFromArray();

				// выполнили
				break;
			}
		}

		return $this->hasOne('Image', 'id', 'avatar_id');
	}

	public function blogs()
	{
		return $this->hasMany('Blog', 'user_id', 'id');
	}

	public function subscribes()
	{
		return $this->hasMany('Subscribe', 'user_id', 'id');
	}

	public function blogTopics()
	{
		return $this->hasMany('BlogTopic', 'user_id', 'id');
	}

	public function blogTopics_countPublished()
	{
		return $this->blogTopics()->whereHas("blog", function($q)
		{
			$q->where("is_draft", "=", false);
		})->count();
	}

	public function blogTopics_countDraft()
	{
		return $this->blogTopics()->whereHas("blog", function($q)
		{
			$q->where("is_draft", "=", true);
		})->count();
	}

	public function isWriteOnly($isWrite = true, $isRead = false)
	{
		$bState = $this->checkAnUnban();
		return $bState ? $isWrite : $isRead;
	}

	public function ban($toDate=null)
	{
		if ($toDate) {
			// до даты
			$this->read_only_to = Date::parse($toDate)->format("Y-m-d H:i:s");
		} else {
			// перманент
			$this->read_only_to = null;
		}

		$this->read_only_from = Date::now()->format("Y-m-d H:i:s");
		$this->write_only = false;
		$this->save();

		return $this;
	}

	public function unban()
	{
		// не забанен
		if ($this->write_only)
			return true;

		$this->read_only_from = null;
		$this->read_only_to = null;
		$this->write_only = true;
		$this->save();

		return (bool) $this->write_only;
	}

	public function checkAnUnban()
	{
		// не забанен
		if ($this->write_only)
			return true;

		// перманентом забанен
		if (!$this->read_only_to)
			return false;

		// дата не подошла
		$toDate = Date::parse($this->read_only_to);
		if (Date::now() < $toDate)
			return false;

		return $this->unban();
	}

	public function allow($name, $true = true, $false = false)
	{
		return $this->group->allow($name, $true, $false);
	}

	public function sendConfirmMail()
	{
		$confirmUrl = route("confirm", $this->confirmation_code);

		$email = $this->email;
		$username = $this->username;

		return Mail::send('emails.auth.confirm',
			[
				"username" => $username,
				"confirmUrl" => $confirmUrl
			],
			function($message) use ($email, $username)
			{
				$message->to($email)->subject(Lang::get("mail.confirm", ["username" => $username]));
			});
	}

	public function sendWelcomeMail()
	{
		$confirmUrl = route("confirm", $this->confirmation_code);

		$email = $this->email;
		$username = $this->username;

		return Mail::send('emails.auth.welcome',
			[
				"username" => $username,
				"confirmUrl" => $confirmUrl
			],
			function($message) use ($email, $username)
			{
				$message->to($email)->subject(Lang::get("mail.welcome", ["username" => $username]));
			});
	}

	// генерация уникального confirmation_code
	public static function getNewConfirmationCode()
	{
		do {
			$newCode = md5(Str::random());
			$newCode = str_shuffle(Str::random().sha1($newCode.microtime(true)));
		} while((bool) User::where("confirmation_code", $newCode)->count());

		return $newCode;
	}

	public function isOnline($true=true,$false=false)
	{
		$lastDate = Date::parse($this->updated_at);
		$curDate = Date::now()->parse('-3 minute');
		
		if ($lastDate > $curDate)
			return $true;

		return $false;
	}
}
