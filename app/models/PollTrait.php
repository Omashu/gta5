<?php

trait PollTrait {

	public function polls()
	{
		return $this->morphMany('Poll', 'pollable');
	}

	/**
	 * Массив опросов, которые нужно создать/обновить и прикрепить к сущности
	 * Не передынне будут удалены
	 */
	public function pollsSyncFromArray($polls)
	{
		if (!is_array($polls) OR !$this->getKey())
			return ["attached" => 0, "detached" => 0];

		// удалим не переданные опросы
		$oldPollIds = $this->polls->modelKeys();
		$newPollIds = [];
		$deletePollIds = [];

		// валидируем массив
		$countPolls = 0;

		foreach ($polls as $poll)
		{
			if ($countPolls >= 15) break;

			if ($pollModel = $this->pollAddFromArray($poll))
			{
				$newPollIds[] = $pollModel->getKey();
				$countPolls++;
			}
		}

		// сверяем ID опросов
		foreach ($oldPollIds as $voteId) {
			if (!in_array($voteId, $newPollIds, true)) {
				$deletePollIds[] = $voteId;
			}
		}

		// удаляем не переданные опросы
		if ($deletePollIds)
			$this->polls()->whereIn("id", $deletePollIds)->delete();

		return ["attached" => $countPolls, "detached" => count($deletePollIds)];
	}

	public function pollAddFromArray($poll)
	{
		$poll = Poll::validateArray($poll);
		if (!$poll)
			return false;

		$bNew = false;
		if (isset($poll["id"]))
		{
			$pollModel = $this->polls()->where("id", $poll["id"])->first();
		}

		if (!isset($poll["id"]) OR !$pollModel)
		{
			// если нет ID, или по нему не найдено сущности, делаем новый опрос
			$pollModel = new Poll();
			$bNew = true;
		}

		// создатель опроса
		if (!$pollModel->user_id)
			$pollModel->user_id = Auth::id();

		$pollModel->title = $poll["title"];
		$pollModel->can_use = $poll["can_use"];
		$pollModel->multiple = $poll["multiple"];
		$pollModel->save();

		// создаем ответы
		$oldAnswerIds = $bNew ? [] : $pollModel->answers->modelKeys();
		$newAnswerIds = [];
		$deleteAnswerIds = [];

		foreach ($poll["answers"] as $answer)
		{
			// новый ответ к новому опросу или новый ответ к старому опросу
			if ($bNew OR (!$bNew AND !is_array($answer)))
			{
				// новый ответ
				$pollAnswer = new PollAnswer();
				$pollAnswer->answer = is_array($answer)
					? $answer["answer"]
					: $answer;

				$pollModel->answers()->save($pollAnswer);
				$newAnswerIds[] = $pollAnswer->id;
				continue;
			}

			// обновляем старый ответ к старому опросу(уже давно созданному)
			if (!$bNew AND is_array($answer))
			{
				$pollAnswer = PollAnswer::find($answer["id"]);
				if ($pollAnswer) {
					$pollAnswer->answer = $answer["answer"];
					$pollAnswer->save();
					$newAnswerIds[] = $pollAnswer->id;
				}
			}
		}

		foreach ($oldAnswerIds as $answerId) {
			if (!in_array($answerId, $newAnswerIds)) {
				$deleteAnswerIds[] = $answerId;
			}
		}

		if ($deleteAnswerIds) {
			PollAnswer::whereIn("id", $deleteAnswerIds)->delete();
		}

		// прикрепляем опрос к топику
		if ($bNew)
			$this->polls()->save($pollModel);

		return $pollModel;
	}

	// текущие опросы (модели) в массив для формы
	public function pollsConvertCurrentToArray()
	{
		$results = [];

		// подтянем созданные опросы
		foreach ($this->polls as $poll) {
			$answers = [];

			foreach ($poll->answers as $answer) {
				$answers[] = ["id" => $answer->id, "answer" => $answer->answer];
			}

			$results[] = [
				"title" => $poll->title,
				"can_use" => $poll->can_use,
				"answers" => $answers,
				"id" => $poll->id,
				"multiple" => $poll->multiple,
			];
		}

		return $results;
	}
}