<?php

class UserObserver {

	public function creating($model)
	{
		$model->first_ip = Request::getClientIp();
		$model->last_ip = Request::getClientIp();

		// генерируем подтверждение
		$model->confirmation_code = User::getNewConfirmationCode();
		$model->confirmed = false;
	}

	public function created($model)
	{
		$model->sendWelcomeMail();
	}

	public function updating($model)
	{
		$model->last_ip = Request::getClientIp();

		// если мыло обновляется
		if ($model->getOriginal("email") !== $model->email)
		{
			$model->confirmation_code = User::getNewConfirmationCode();
			$model->confirmed = false;
			$model->sendConfirmMail();
		}
	}
}