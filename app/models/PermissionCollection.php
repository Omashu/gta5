<?php

class PermissionCollection extends Illuminate\Database\Eloquent\Collection {

	public function allow($name, $true = true, $false = false)
	{
		foreach ($this->items as $item)
		{
			if ($item->name === $name)
			{
				return $true;
			}
		}

		return $false;
	}
}