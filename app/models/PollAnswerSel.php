<?php
// голос
class PollAnswerSel extends Eloquent {
	protected $guarded = ["*"];

	public function answer()
	{
		return $this->hasOne('PollAnswer', "id", "poll_answer_id");
	}
}
