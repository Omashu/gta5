<?php

class FileModelObserver {
	public function deleting($model)
	{
		// удаляем файл
		if (File::exists($model->getAbsPath()))
			File::delete($model->getAbsPath());
	}
}