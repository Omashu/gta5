<?php 

use System\Purifier;

class Feedback extends System\Eloquent {

	protected $fillable = ["theme", "email", "message"];
	protected $guarded = ["*"];

	protected static $rules = [
		"email" => [
			["required"],
			["email"],
		],
		"theme" => [
			["required"],
		],
		"message" => [
			["required"],
		],
	];

	public static function boot()
	{
		parent::boot();
		Feedback::observe(new FeedbackObserver);
	}

	public function saveAndSend()
	{
		$this->save();
		return $this->send();
	}

	public function send()
	{
		if (!$this->getKey())
			return false;

		// admin emails
		$emails = Config::get("app.feedback.emails", []);
		if (!$emails)
			return false;

		$feedback = $this;

		foreach ($emails as $email)
		{
			Mail::send('emails.feedback',
				[
					"user" => User::find($this->user_id),
					"feedback" => $feedback,
				],
				function($message) use ($feedback, $email)
				{
					$message->from($feedback->email);
					$message->to($email)->subject(Lang::get("mail.feedback", ["theme" => $feedback->theme]));
				});
		}

		return true;
	}

	public function setThemeAttribute($value)
	{
		$this->attributes['theme'] = HTML::entities($value);
	}

	public function setMessageAttribute($value)
	{
		// чистим сообщение фильтрами
		$purifier = Purifier::getObject("feedbackMessage");
		$value = $purifier->purify($value);

		$this->attributes['message'] = $value;
	}
}
