<?php

trait FileModelTrait {

	// прикрепленные файлы
	public function files()
	{
		return $this->morphMany('FileModel', 'fileable');
	}

	public function filesSyncFromArray($files)
	{
		if (!is_array($files) OR !$files)
			return false;

		$checkedFileIds = [];
		foreach ($files as $fileId)
		{
			if (count($checkedFileIds)>=(int)Config::get("app.uploads.maxFilesOnOneTarget"))
				break;

			if (!is_string($fileId) AND !is_integer($fileId) AND !(int)$fileId)
				continue;

			$checkedFileIds[] = (int) $fileId;
		}

		if (!$checkedFileIds)
			return false;

		$topicUserId = $this->user_id;

		$fileModels = FileModel::whereIn("id", $checkedFileIds)
			->where("fileable_type", "")
			->where(function($q) use ($topicUserId)
			{
				$q->orWhere("user_id", Auth::id());
				$q->orWhere("user_id", $topicUserId);
			})->update(["fileable_type" => __CLASS__, "fileable_id" => $this->getKey()]);

		return true;
	}

}