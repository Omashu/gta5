<?php

/**
 * flag_write: Публикация топиков (permission_public у Blog не влияет) или запрет на публикацию
 * flag_moderate: Возможность отказывать в публикации чужих топиков в этот блог (отправлять их обратно автору)
 * 
 * blog (int) | user (int) | flag_write (bool) | flag_moderate (bool)
 */

class BlogPermission extends System\Eloquent {
	public function user()
	{
		return $this->hasOne('User', 'id', 'user_id');
	}
}