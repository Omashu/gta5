<?php

class FeedbackObserver {

	public function creating($model)
	{
		$model->user_id = Auth::id();
		$model->ip_address = Request::getClientIp();
	}
}