<?php

class BlogObserver {

	protected function _transfTopics($model)
	{
		// переносим топики из блога в черновики авторам
		$topicIds = [];
		$topics = DB::table("blog_topics")
			->where("blog_id", $model->getKey())->get();

		foreach ($topics as $topic) {
			$topicIds[$topic->user_id][] = $topic->id;
		}

		// нет топиков для переноса
		if (!$topicIds) return;

		$userIds = array_keys($topicIds);

		$userDraftBlogs = DB::table("blogs")
			->where("is_draft", true)
			->whereIn("user_id", $userIds)
			->lists("id", "user_id");

		// для тех,у кого нет чернового блога - создадим
		foreach ($userIds as $userId) {
			if (!isset($userDraftBlogs[$userId]))
				$userDraftBlogs[$userId] = Blog::getUserDraft($userId)->id;
		}

		// переносим
		foreach ($topicIds as $userId => $topicIdsValues) {
			DB::table("blog_topics")
				->where("user_id", $userId)
				->whereIn("id", $topicIdsValues)
				->update(["blog_id" => $userDraftBlogs[$userId]]);
		}
	}


	public function deleting($model)
	{
		// переносим все топики авторам в черновики
		$this->_transfTopics($model);

		// удаляем подписки
		$model->subscribes()->delete();

		// удаляем связи на изображения
		$model->images()->delete();
	}

	public function updating($model)
	{
	}
}