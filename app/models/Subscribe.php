<?php

class Subscribe extends Eloquent {
	protected $guarded = ["*"];

	public function subscribeable()
	{
		return $this->morphTo();
	}
}
