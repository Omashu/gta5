<?php

class PollAnswer extends Eloquent {

	protected $fillable = ["answer"];
	protected $guarded = ["*"];

	protected static $rules = [
		"answer" => [
			["required", ["create", "update"]],
		],
		"vote_id" => [
			["required", ["create", "update"]],
			["exists:votes,id", ["create", "update"]],
		],
	];

	public function setAnswerAttribute($value)
	{
		$this->attributes['answer'] = HTML::entities($value);
	}

	public function poll()
	{
		return $this->hasOne('Poll', 'id', 'poll_id');
	}

	public function sels()
	{
		return $this->hasMany('PollAnswerSel');
	}
}
