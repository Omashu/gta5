<?php

trait RatingTrait {

	public function ratings()
	{
		return $this->morphMany('Rating', 'ratingable');
	}

	/**
	 * @param object $user сущность пользователя
	 * @param mixed $true вернется, если может голосовать
	 * @param mixed $false вернется, если голосовать не может
	 * @param bool $throw true - будет брошено исключение с ошибкой, в случае если нет прав голосовать
	 */
	public function ratingAllowUseByUser(User $user = null, $true = true, $false = false, $throw = false)
	{
		if (!$user) {
			if ($throw) throw new RatingException("Undefined user", 1);
			return $false;
		}

		// аккаунт активен?
		if (!$user->isWriteOnly()) {
			if ($throw) throw new RatingException("Аккаунт заблокирован", 2);
			return $false;
		}

		$columnUserId = isset($this->columnUserId) ? $this->columnUserId : "user_id";
		if ((int) $this->$columnUserId === (int) $user->id) {
			if ($throw) throw new RatingException("Самому себе нельзя", 3);
			return $false;
		}

		$object = $this->ratingObjectByUser($user, null, true);

		// сущности нет, значит не голосовал, можно
		if (!$object) return $true;

		// в течение месяца можно сбросить голос

		$monthAgo = Date::now()->parse('-1 month');
		if ($monthAgo > $object->created_at) {
			if ($throw) throw new RatingException("Ваш голос зафиксирован и изменить его уже нельзя", 4);
			return $false;
		}

		// доп. проверки у каждой модели
		if (method_exists($this, "_ratingAllowUseByUser"))
			return $this->_ratingAllowUseByUser($user, $true, $false, $throw);

		return $true;
	}

	public function ratingAllowUseByUserThrow(User $user = null)
	{
		return $this->ratingAllowUseByUser($user, true, false, true);
	}

	public function ratingObjectByUser(User $user = null, $default = null, $withTrashed = false)
	{
		if (!$user) return $default;
		$object = $this->ratings()->where("user_id", "=", $user->id);
		if ($withTrashed) $object->withTrashed();
		$object = $object->first();

		return $object ? $object : $default;
	}
}