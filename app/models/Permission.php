<?php

class Permission extends System\Eloquent {

	protected $fillable = ["title", "description"];
	protected $guarded = ["*"];
	
	public function newCollection(array $models = [])
	{
		return new PermissionCollection($models);
	}
}
