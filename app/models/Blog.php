<?php

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use System\Purifier;

class Blog extends System\Eloquent implements SluggableInterface {

	use SluggableTrait;
	use ImageTrait;

	protected $fillable = ["title", "description"];
	protected $guarded = ["*"];
	protected $sluggable = [];

	// для синхронизации колонки с прикрепленными изображениями
	protected $imageIdColumns = ["preview_id"];

	protected static $rules = [
		"title" => [
			["required", ["create", "update"]],
		],
		"user_id" => [
			["required", ["create", "update"]],
			["exists:users,id", ["create", "update"]],
		],
		"preview_id" => [
			["exists:images,id", ["create", "update"]],
		],
	];

	public static function boot()
	{
		parent::boot();
		Blog::observe(new BlogObserver);
	}

	public static function getSortColumns()
	{
		return [
			"id" => "ID",
			"title" => "Название",
			"created_at" => "Дата регистрации",
			"updated_at" => "Дата входа",
		];
	}

	public function newCollection(array $models = [])
	{
		return new System\Collection($models);
	}

	// получаем черновой блог юзера
	public static function getUserDraft($userId)
	{
		// если нет, создадим
		$blog = Blog::where("user_id", $userId)->where("is_draft", true)->first();

		if (!$blog)
		{
			$blog = new Blog();
			$blog->user_id = $userId;
			$blog->title = "Мои черновики";
			$blog->permission_public = false;
			$blog->is_draft = true;
			$blog->save();
		}

		return $blog;
	}

	// блоги, на которые подписан юзер и может в них писать
	public static function getUserSubsAndWriteOnly(User $user)
	{
		return Blog::where("is_draft", false)
			->where("user_id", "<>", $user->getKey()) // если админ блога, то ненужн
			->whereHas("subscribes", function($q) use ($user)
			{
				$q->where("user_id", $user->getKey());
			})->where(function($q) use ($user)
			{
				$q->where(function($q) use ($user)
				{
					$q->where("permission_public", true);
					$q->where("permission_public_min_rating", "<=", $user->rating);

					$q->whereNotExists(function($query) use ($user)
					{
						$query->select(DB::raw(1))
							->from('blog_permissions')
							->where("user_id", $user->getKey())
							->whereRaw('blog_permissions.blog_id = blogs.id');
					})->orWhereHas("permissions", function($q) use ($user)
					{
						$q->where("user_id", $user->getKey())->where("flag_write", true);
					});
				});

				$q->orWhereHas("permissions", function($q) use ($user)
				{
					$q->where("user_id", $user->getKey())->where("flag_write", true);
				});
			});
	}

	/********
	 * Relations
	 *********/

	// пользователь, создавший блог
	public function user()
	{
		return $this->hasOne('User', 'id', 'user_id');
	}

	// топики блога
	public function topics()
	{
		return $this->hasMany('BlogTopic');
	}

	// привилегии юзеров
	public function permissions()
	{
		return $this->hasMany("BlogPermission");
	}

	// превью
	public function preview()
	{
		return $this->hasOne('Image', 'id', 'preview_id');
	}

	// подписчики
	public function subscribes()
	{
		return $this->morphMany('Subscribe', 'subscribeable');
	}

	// подписан ли юзер на блог
	public function userIsSubscribed(User $user, $rBool = true)
	{
		$q = $this->subscribes()->where("user_id", $user->id);
		return $rBool ? (bool) $q->count() : $q->first();
	}

	public function setTitleAttribute($value)
	{
		$this->attributes['dirty_title'] = $value;
		$this->attributes['title'] = HTML::entities($value);
	}

	public function setDescriptionAttribute($value)
	{
		$purifier = Purifier::getObject("blogDescription");
		$value = $purifier->purify($value);

		$this->attributes['description'] = $value;
	}

	/**
	 * Allows
	 * return Bool
	 */

	public static function allowAdd(User $user = null)
	{
		return \Allow\Blog::add($user);
	}

	public function allowEdit(User $user = null)
	{
		return \Allow\Blog::edit($user, $this);
	}

	public function allowDelete(User $user = null)
	{
		return \Allow\Blog::delete($user, $this);
	}

	// может ли юзер писать в этот блог
	public function allowNewTopic(User $user = null)
	{
		return \Allow\Blog::newTopic($user, $this);
	}
}
