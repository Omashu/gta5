<?php 

// голос
class FileModelDownload extends Eloquent {
	protected $guarded = ["*"];
	protected $table = "file_downloads";

	public function user()
	{
		return $this->hasOne('User', "id", "user_id");
	}
}
