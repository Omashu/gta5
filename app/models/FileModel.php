<?php 

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class FileModel extends System\Eloquent implements SluggableInterface {

	use SluggableTrait;

	protected $table = "files";
	protected $sluggable = [];
	protected static $uploadLastValidator = NULL;

	public function blogTopics()
	{
		return $this->morphedByMany('BlogTopic', 'fileable');
	}

	public function downloads()
	{
		return $this->hasMany('FileModelDownload', 'file_id');
	}

	public static function boot()
	{
		parent::boot();
		FileModel::observe(new FileModelObserver);
	}

	/**
	 * Загрузка файла
	 */
	public static function getValidator()
	{
		return static::$uploadLastValidator;
	}

	public static function upload(Symfony\Component\HttpFoundation\File\UploadedFile $file)
	{
		$mainAbsPath = public_path() . "/" . trim(Config::get("app.uploads.files"), "/") . "/";
		$mainRelPath = "/" . trim(Config::get("app.uploads.files"), "/") . "/";

		$validator = Validator::make(["file" => $file], ["file" => "required|max:50000|mimes:rar,zip,7z"]);

		if ($validator->fails())
		{
			static::$uploadLastValidator = $validator;
			return FALSE;
		}

		$ext = $file->getClientOriginalExtension();

		$filename = preg_replace("/\.{$ext}$/", "", $file->getClientOriginalName());

		// чистим название
		$title = $filename;
		$title = str_replace("-", " ", $title);
		$title = str_replace("_", " ", $title);
		$title = preg_replace("/\s\d+$/", "", $title);
		$title = preg_replace("/^\d+\s/", "", $title);
		$title = trim($title);

		$filename = trim($filename);
		if (!$filename) $filename = Str::random();

		$filename = Slug::make($filename);
		$filename = substr($filename, 0, 64);
		$filenameHash = md5($filename);

		$folders = [substr($filenameHash, 0, 2), substr($filenameHash, 2, 2)];

		$fullAbsPath = $mainAbsPath . implode("/", $folders) . "/";
		$fullRelPath = $mainRelPath . implode("/", $folders) . "/";

		do {
			$filename = Str::random(2) . "-" . $filename;
		} while(file_exists($fullAbsPath . $filename . "." . $ext));

		// создаем сущность
		$fileModel = new FileModel();
		$fileModel->user_id = Auth::user()->id;
		$fileModel->title = $title;
		$fileModel->filename = $filename;
		$fileModel->mime_type = $file->getMimeType();
		$fileModel->format = $ext;

		// переносим файл
		$file->move($fullAbsPath, $filename . "." . $ext);

		$fileAbsPath = $fullAbsPath . $filename . "." . $ext;
		$fileRelPath = $fullRelPath . $filename . "." . $ext;

		$fileModel->path = $fullRelPath;

		// готово ;)
		$fileModel->bytes = filesize($fileAbsPath);
		$fileModel->save();

		return $fileModel;
	}

	public function setTitleAttribute($value)
	{
		$this->attributes['dirty_title'] = $value;
		$this->attributes['title'] = HTML::entities($value);
	}

	public function getRelPath()
	{
		return $this->path . $this->filename . "." . $this->format;
	}

	public function getAbsPath()
	{
		return public_path($this->getRelPath());
	}

	public function getUrl()
	{
		return asset($this->getRelPath());
	}

	public static function userNonUsed(User $user)
	{
		return FileModel::where(function($q) use ($user)
		{
			$q->where("fileable_type", "");
			$q->where("user_id", $user->getKey());
		});
	}
}
