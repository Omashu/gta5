<?php

use System\Purifier;
class Comment extends Node {

	use RatingTrait;

	protected $fillable = ["message"];
	protected $guarded = ["*"];

	public static function boot()
	{
		parent::boot();
		Comment::observe(new CommentObserver);
	}

	public function user()
	{
		return $this->hasOne('User', 'id', 'user_id');
	}

	public function getRulesCreate()
	{
		return [
			"user_id" => ["required", "exists:users,id"],
			"message" => ["required"]
		];
	}

	public function getMessageRawAttribute($value)
	{
		// оригинальный
		if ($value)
			return $value;

		// если его нет - то этот (в старых версиях небыло _raw)
		return $this->message;
	}

	public function setMessageAttribute($value)
	{
		$purifier = Purifier::getObject("commentMessage");
		$value = $purifier->purify(nl2br($value));

		// сохраняем "оригинал" (до реплейсов)
		$this->attributes['message_raw'] = $value;

		// реплейсим данные и сохраним
		$this->attributes['message'] = $this->replaceMessage($value);
	}

	public function replaceMessage($value)
	{
		preg_match_all("/@(?<username>[^\s@,.<>]{3,24})/su", $value, $matches);

		if (!$matches)
			return $value;

		$usernames = array_slice(array_unique($matches["username"]), 0, 50);

		foreach ($usernames as $key => $username)
		{
			$raw = $matches[0][$key];
			$user = User::where("username", $username)->select("username")->first();

			if (!$user)
				continue;

			$new = HTML::link(route("showUserProfile", $user->username), $raw, ["class" => "comment-link comment-link_user", "title" => $user->username]);
			$value = str_replace($raw, $new, $value);
		}

		return $value;
	}

	public function allowUpSpamLevel(User $user = null)
	{
		if (!$user)
			return false;

		// для не заблокированных
		if (!$user->isWriteOnly())
			return false;

		$permissions = $user->group->permissions;

		if ($permissions->allow("comment.upSpamLevel"))
			return true;

		return false;
	}

	public function allowSetSpam(User $user = null)
	{
		if (!$user)
			return false;

		// для не заблокированных
		if (!$user->isWriteOnly())
			return false;

		$permissions = $user->group->permissions;

		if ($permissions->allow("comment.setSpamFlagAll"))
			return true;

		return false;
	}

	protected function _ratingAllowUseByUser($user, $true, $false, $throw)
	{
		if ($this->spam) {
			if ($throw) throw new RatingException("Комментарий скрыт, изменить ему рейтинг уже нельзя");
			return $false;
		}

		return $true;
	}
}
