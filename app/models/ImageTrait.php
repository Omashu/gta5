<?php

trait ImageTrait {

	// прикрепленные изображения
	public function images()
	{
		return $this->morphToMany('Image', 'imageable');
	}

	public function imagesSyncFromArray($images=null)
	{
		$checkedImageIds = [];
		if (is_array($images) AND $images)
		{
			foreach ($images as $imageId)
			{
				if (count($checkedImageIds)>=(int)Config::get("app.uploads.maxImagesOnOneTarget"))
					break;

				if (!is_string($imageId) AND !is_integer($imageId) AND !(int)$imageId)
					continue;

				$checkedImageIds[] = (int) $imageId;
			}
		}

		// добавим в лист изображения из колонок
		if (isset($this->imageIdColumns) AND is_array($this->imageIdColumns))
			foreach ($this->imageIdColumns as $column)
				if ($this->$column) $checkedImageIds[] = $this->$column;

		$checkedImageIds = array_unique($checkedImageIds);

		if ($checkedImageIds)
			$checkedImageIds = Image::whereIn("id", $checkedImageIds)->get()->modelKeys();

		// сцепляем/отцепляем
		$this->images()->sync($checkedImageIds);
		return true;
	}
}