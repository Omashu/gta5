<?php

class CommentObserver {

	public function creating($model)
	{
		preg_match_all("/http/", $model->message, $matches);
		if ($matches and isset($matches[0]))
			$model->spam_level = count($matches[0]) * 5;
	}
}