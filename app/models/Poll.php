<?php

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Poll extends System\Eloquent implements SluggableInterface {

	use SluggableTrait;

	protected $fillable = ["title"];
	protected $guarded = ["*"];
	protected $sluggable = [];

	protected static $rules = [
		"title" => [
			["required", ["create", "update"]],
		],
		"user_id" => [
			["required", ["create", "update"]],
			["exists:users,id", ["create", "update"]],
		],
	];

	// валидирует массив данных опроса
	// return null если не прошел валидацию
	public static function validateArray($poll)
	{
		if (!is_array($poll))
			return null;

		$poll = array_only($poll, ["title", "id", "can_use", "multiple", "answers"]);

		// название обязательно
		if (!isset($poll["title"]) OR !is_string($poll["title"]) OR !mb_strlen(trim($poll["title"])))
			return null;

		// не передан ID, значит это новый опрос
		if (!isset($poll["id"]) OR !$poll["id"])
			$poll["id"] = null;

		// кто может голосовать
		if (!isset($poll["can_use"]) OR !is_string($poll["can_use"]) OR !in_array($poll["can_use"], ["users", "users_and_ghosts"]))
			$poll["can_use"] = "users";

		// можно ли выбрать несколько вариантов
		if (!isset($poll["multiple"]))
			$poll["multiple"] = false;

		$poll["multiple"] = (bool) $poll["multiple"];

		// варианты ответов обязательно должны быть
		if (!isset($poll["answers"]) OR !is_array($poll["answers"]) OR (!isset($poll["answers"]["old"]) AND !isset($poll["answers"]["new"])))
			return null;

		// проверенные ответы
		$checkedPollAnswers = [];

		// check old answers, (старые, это те которые уже созданы, например это форма редактирования)
		if (isset($poll["answers"]["old"]) AND is_array($poll["answers"]["old"]))
		{
			foreach ($poll["answers"]["old"] as $answerId => $answer)
			{
				if (!is_string($answer) OR !mb_strlen(trim($answer)) OR !(int)$answerId)
					continue;

				$checkedPollAnswers[] = ["id" => (int) $answerId, "answer" => $answer];
			}
		}

		// check new answers
		if (isset($poll["answers"]["new"]) AND is_array($poll["answers"]["new"]))
		{
			foreach ($poll["answers"]["new"]  as $answer)
			{
				if (!is_string($answer) OR !mb_strlen(trim($answer)))
					continue;

				$checkedPollAnswers[] = $answer;
			}
		}

		// минимум 2 варианта ответа, если их нет, опрос не редактируется/создается
		if (count($checkedPollAnswers) < 2)
			return null;

		$poll["answers"] = $checkedPollAnswers;
		return $poll;
	}

	// приходит много "опросов", пропускаем только корректные
	public static function validateArrayAll($polls)
	{
		if (!is_array($polls) OR !count($polls))
			return [];

		$checkedPolls = [];
		foreach ($polls as $poll)
		{
			$poll = self::validateArray($poll);
			if (!$poll)
				continue;

			$checkedPolls[] = $poll;
		}

		return $checkedPolls;
	}

	public function setTitleAttribute($value)
	{
		$this->attributes['dirty_title'] = $value;
		$this->attributes['title'] = HTML::entities($value);
	}

	// может ли юзер или этот IP голосовать
	public function allow(User $user = null, $clientIp = null)
	{
		// только для юзеров
		if (!$user AND $this->can_use === "users")
			return false;

		if ($user) {
			$sels = DB::table("poll_answers")
				->where("poll_answers.poll_id", "=", $this->getKey())
				->whereExists(function($q) use ($user, $clientIp)
				{
					$q->select(DB::raw(1))
						->from("poll_answer_sels")
						->whereRaw("poll_answer_sels.poll_answer_id = poll_answers.id AND (poll_answer_sels.user_id = ? OR poll_answer_sels.ip_address = ?)",
							[
								$user->getKey(),
								$clientIp
							]);
				})->count();

			// не голосовал
			if (!$sels)
				return true;

			// голосовал
			return false;
		}

		// теперь по IP
		$sels = DB::table("poll_answers")
			->where("poll_answers.poll_id", "=", $this->getKey())
			->whereExists(function($q) use ($user, $clientIp)
			{
				$q->select(DB::raw(1))
					->from("poll_answer_sels")
					->whereRaw("poll_answer_sels.poll_answer_id = poll_answers.id AND poll_answer_sels.ip_address = ?",
						[
							$clientIp
						]);
			})->count();

		// не голосовал
		if (!$sels)
			return true;

		// голосовал
		return false;
	}

	/**
	 * Вернет данные ответов в формате
	 * [
	 * 	"answers" => [
	 * 		answerId =>
	 * 		[
	 * 			"id" => $answerId,
	 * 			"percent" => %,
	 * 			"countVotes" => integer (кол-во голосов)
	 * 		]
	 * 	],
	 * 	"countAll" => integer (общ. кол-во выбранных ответов (не уник)),
	 *  "countAllUniq" => integer (общ.кол-во юзеров проголосвавших)
	 * 	"answerIdMaxCount" => integer
	 * ]
	 */
	public function getAnswersData()
	{
		$percents = [];

		$countAll = 0;
		$answerCountSels = [];

		foreach ($this->answers as $answer)
		{
			$sels = $answer->sels()->count();
			$countAll += $sels;
			$answerCountSels[$answer->id] = $sels;
		}

		$answerIdMaxCount = null;
		$answerSelsMax = 0;

		foreach ($answerCountSels as $answerId => $answerSels)
		{

			$percents[$answerId] = [
				"percent" => $countAll ? round(100 / $countAll * $answerSels) : 0,
				"countVotes" => $answerSels,
				"id" => $answerId,
			];

			if ($answerSels > $answerSelsMax) {
				$answerIdMaxCount = $answerId;
				$answerSelsMax = $answerSels;
			}
		}

		return ["answerIdMax" => $answerIdMaxCount, "results" => $percents, "countAll" => $countAll, "countAllUniq" => $this->getCountUnique()];
	}

	public function getCountUnique()
	{
		$countAllUniq = DB::table("poll_answer_sels")
			->leftJoin("poll_answers", "poll_answer_sels.poll_answer_id", "=", "poll_answers.id")
			->where("poll_answers.poll_id", "=", $this->getKey())->distinct();

		// "users", "users_and_ghosts"
		if ($this->can_use === "users")
			$countAllUniq->select("poll_answer_sels.user_id");
		else if ($this->can_use === "users_and_ghosts")
			$countAllUniq->select("poll_answer_sels.ip_address");

		$countAllUniq = count($countAllUniq->remember(15)->get());

		return $countAllUniq;
	}

	// Relations
	public function answers()
	{
		return $this->hasMany('PollAnswer');
	}

	public function pollable()
	{
		return $this->morphTo();
	}

	public function user()
	{
		return $this->hasOne('User', 'id', 'user_id');
	}
}
