<?php

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use System\Purifier;

class BlogTopic extends System\Eloquent implements SluggableInterface {

	use SluggableTrait, RatingTrait;
	use SoftDeletingTrait;
	use Conner\Tagging\TaggableTrait;
	use ImageTrait;
	use FileModelTrait;
	use PollTrait;

	protected $fillable = ["title", "description"];
	protected $guarded = ["*"];
	protected $sluggable = ["include_trashed" => true];
	protected $dates = ['deleted_at'];

	// для синхронизации колонки с прикрепленными изображениями
	protected $imageIdColumns = ["preview_id"];

	protected static $rules = [
		"title" => [
			["required", ["create", "update"]],
		],
		"blog_id" => [
			["required", ["create", "update"]],
			["exists:blogs,id", ["create", "update"]],
		],
		"user_id" => [
			["required", ["create", "update"]],
			["exists:users,id", ["create", "update"]],
		],
		"preview_id" => [
			["exists:images,id", ["create", "update"]],
		]
	];

	public static function boot()
	{
		parent::boot();
		BlogTopic::observe(new BlogTopicObserver);
	}

	public function user()
	{
		return $this->hasOne('User', 'id', 'user_id');
	}

	public function blog()
	{
		return $this->hasOne('Blog', 'id', 'blog_id');
	}

	public function preview()
	{
		return $this->hasOne('Image', 'id', 'preview_id');
	}

	public function comments()
	{
		return $this->morphMany('Comment', 'commentable');
	}

	public function setTitleAttribute($value)
	{
		$this->attributes['dirty_title'] = $value;
		$this->attributes['title'] = HTML::entities($value);
	}

	public function setDescriptionAttribute($value)
	{
		// чистим описание филтрами
		$purifier = Purifier::getObject("blogTopicDescription");
		$value = $purifier->purify($value);

		// ищем тег cur (первое совпадение) - короткое описание
		preg_match("/(?<short>.*)<cut.*/Uusm", $value, $matches);
		$beforeCutRaw = isset($matches["short"]) ? $matches["short"] : null;

		// ищем название cut'a (первое совпадение)
		preg_match("/.*<cut.*>(?<cutTitle>.*)?<\/cut/", $value, $matches);
		$cutTitle = isset($matches["cutTitle"]) ? trim($matches["cutTitle"]) : null;

		// чистим шорт и cut_title, готово
		$this->attributes['description'] = $value;
		$this->attributes['short'] = $purifier->purify($beforeCutRaw);
		$this->attributes['cut_title'] = $cutTitle ? HTML::entities($cutTitle) : null;
	}

	// ключи кеша, там хранятся, очищенное и обработанное методом replaceContent
	public function getCacheKeyDescriptionCleaned() {
		return "BlogTopic_DescriptionCleaned_" . $this->getKey() . "_" . $this->updated_at;
	}

	public function getCacheKeyShortDescriptionCleaned() {
		return "BlogTopic_ShortDescriptionCleaned_" . $this->getKey() . "_" . $this->updated_at;
	}

	public function getDescriptionShort() {
		return BlogTopicDescriptionReplacer::replace($this->short, $this->getCacheKeyShortDescriptionCleaned());
	}

	public function getDescription() {
		return BlogTopicDescriptionReplacer::replace($this->description, $this->getCacheKeyDescriptionCleaned());
	}

	public function getCutTitleAttribute($value)
	{
		if ($value) return $value;
		return Lang::get("base.readMore");
	}

	/**
	 * Allows
	 */

	public static function allowAdd(User $user = null)
	{
		return \Allow\BlogTopic::add($user);
	}

	public function allowEdit(User $user = null)
	{
		return \Allow\BlogTopic::edit($user, $this);
	}

	public function allowDelete(User $user = null)
	{
		return \Allow\BlogTopic::delete($user, $this);
	}
}
