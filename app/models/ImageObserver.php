<?php

class ImageObserver {
	public function deleting($model)
	{
		// удаляем оригинал и миниатюры
		Croppa::delete($model->getRelPath());
	}
}