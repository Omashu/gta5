var Ajax = (function()
{
	var AJAX;
	var Method = "POST";
	var DataType = "JSON";
	var Data = {"_token":_VALUES["token"]};
	var Url;
	var Callbacks = {
		"success" : $.Callbacks(),
		"error" : $.Callbacks(),
		"complete" : $.Callbacks()
	};

	this.on = function(name, func)
	{
		if (!Callbacks[name])
		{
			console.log("Undefined callback name", name);
			return this;
		}

		Callbacks[name].add(func);
		return this;
	};

	this.empty = function(name)
	{
		if (!Callbacks[name])
		{
			console.log("Undefined callback name", name);
			return this;
		}

		Callbacks[name].empty();
		return this;
	};

	this.dataType = function(value)
	{
		DataType = value;
		return this;
	};

	this.url = function(value)
	{
		Url = value;
		return this;
	};

	this.data = function(k,v)
	{
		if (typeof k === "object") {
			for (var _k in k) {
				Data[_k] = k[_k];
			}

			return this;
		}

		Data[k] = v;
		return this;
	};

	this.method = function(value)
	{
		Method = value;
		return this;
	};

	this.execute = function()
	{
		AJAX = $.ajax(
			{
				type : Method,
				dataType : DataType,
				data : Data,
				url : Url,
				success : function(e)
				{
					Callbacks.success.fire(e);
				},
				error : function(e)
				{
					Callbacks.error.fire(e);
				},
				complete : function(e)
				{
					Callbacks.complete.fire(e);
				}
			});
		return this;
	};

	return this;
});