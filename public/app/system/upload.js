var Upload_IMAGE = "image";
var Upload_FILE = "file";

var Upload = (function(oValues, oFileObject, fType)
{
	var self = this;
	var oFileObjects = [];
	var oFileUploadConfig = {
		formData : {
			"_token" : _VALUES["token"]
		},
		dataType : "json",
		previewCrop : true,
		previewMaxWidth : 64,
		previewMaxHeight : 64,
		maxFileSize : 10485760, // 10 МБ
		multiple : false
	};

	// callbacks
	var oCallbacks = {
		"add" : $.Callbacks(),
		"progress" : $.Callbacks(),
		"done" : $.Callbacks(),
		"fail" : $.Callbacks(),
		"processAlways" : $.Callbacks()
	};

	// handlers
	var handlerAdd, handlerDone, handlerProcessAlways, handlerFail, handlerProgress;

	handlerAdd = function(e, data)
	{
		$.each(data.files, function(index, file) {
			data.context = new (function()
			{
				var oStorage = {};
				var data = {loaded:0,total:1,res:null};

				this.storage = function(k,v)
				{
					if (typeof v !== "undefined")
					{
						oStorage[k] = v;
						return this;
					}

					return oStorage[k] || null;
				};

				this.getName = function()
				{
					return file.name;
				};

				this.getPreview = function()
				{
					return file.preview;
				};

				this.getPercent = function()
				{
					return parseInt(data.loaded / data.total * 100, 10);
				};

				this.getRes = function()
				{
					return data.res;
				};

				this.set = function(k,v)
				{
					data[k] = v;
					return this;
				};

				return this;
			});

			oCallbacks.add.fire(data.context);
		});
	};

	handlerDone = function(e, data)
	{
		data.context.set("res", data.result);
		oCallbacks.done.fire(data.context);
	};

	handlerProcessAlways = function(e, data)
	{
		oCallbacks.processAlways.fire(data.context);
	};

	handlerFail = function(e, data)
	{
		oCallbacks.fail.fire(data.context);
	};

	handlerProgress = function(e, data)
	{
		data.context.set("loaded", data.loaded);
		data.context.set("total", data.total);
		oCallbacks.progress.fire(data.context);
	};

	var bindFileObject = function(o, oValues)
	{
		o.fileupload(oValues || {}).on("fileuploadadd", handlerAdd)
			.on("fileuploaddone", handlerDone)
			.on("fileuploadprocessalways", handlerProcessAlways)
			.on("fileuploadfail", handlerFail)
			.on("fileuploadprogress", handlerProgress);
	};

	this.on = function(name, func)
	{
		if (!oCallbacks[name]) {
			return this;
		}

		oCallbacks[name].add(func);
		return this;
	};

	this.addFileObject = function(o)
	{
		if (!o || !o.length) return this;

		o.prop("multiple", !!oFileUploadConfig.multiple);
		oFileObjects.push(o);
		bindFileObject(o, oFileUploadConfig);

		return this;
	};

	// configure
	oValues = oValues || {};
	if (!oValues.url)
	{
		oValues.url = (fType == Upload_FILE
			? _VALUES["url.ajaxUploadFile"]
			: _VALUES["url.ajaxUploadImage"]);
	}

	oFileUploadConfig = $.extend(true, oFileUploadConfig, oValues);
	this.addFileObject(oFileObject);

	return this;
});