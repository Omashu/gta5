jQuery(function()
{
	$('.panel-filter').each(function()
	{
		var oForm = $(this).parent('form');

		$(this).find('input, select').each(function()
		{
			$(this).change(function()
			{
				oForm.submit();
			});
		});
	});

	$('.js-datepicker').datepicker();
});