var Dashboard = Dashboard || {};

Dashboard.IndexGraphs = (function(args)
{
	var _initNewUsers = function(oBlock)
	{
		var Query = (new Ajax())
			.url(_VALUES["route.as.BackendAjaxGraphGetNewUsersMonth"])
			.method("get")
			.on("success", function(e)
			{
				console.log(e);
				new Morris.Line({
					element: oBlock,
					data: e.data,
					xkey: 'date',
					ykeys: ['count'],
					labels: ['Регистраций']
				});
			})
			.execute();
		
	};

	_initNewUsers($(args.newUsers));

	return this;
});