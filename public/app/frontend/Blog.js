var Space = Space || {};

Space.Blog = (function()
{
	var self = this;
	var AjaxDelete = new Ajax();
	AjaxDelete.method("post").url(_VALUES["url.ajaxBlogDelete"]);

	this.aEPermissions = function(args)
	{
		var btn = args.btn;
		var cont = args.cont;

		$(cont).find('.js-username').each(function()
		{
			Space.User.autocomplete($(this));
		});

		$(btn).click(function(e)
		{
			e.preventDefault();

			var cloneRow = $(cont).find('tr').eq(0).clone();
			Space.User.autocomplete(cloneRow.find('.js-username'));

			cloneRow.appendTo(cont);
			cloneRow.show();
		});

		cont.on('click', '.js-save', function(event) {
			event.preventDefault();

			var btn = $(this);
			var row = btn.parent().parent().parent();
			var blogId = row.find('.js-blog-id').val();
			var username = row.find('.js-username').val();
			var flagWrite = row.find('.js-flag-write').val();
			var flagModerate = row.find('.js-flag-moderate').val();

			btn.addClass("disabled").prop("disabled", true);
			var Query = (new Ajax()).method("post").url(_VALUES["url.ajaxBlogPermission"]);
			Query.on("complete", function(e)
			{
				btn.removeClass("disabled").prop("disabled", false);
			});

			Query.data(
				{
					username : username,
					blog_id : blogId,
					flag_write : flagWrite,
					flag_moderate : flagModerate
				});
			Query.execute();
		});

		cont.on('click', '.js-delete', function(event) {
			event.preventDefault();
			
			var btn = $(this);
			var row = btn.parent().parent().parent();
			var blogId = row.find('.js-blog-id').val();
			var username = row.find('.js-username').val();

			btn.addClass("disabled").prop("disabled", true);
			var Query = (new Ajax()).method("post").url(_VALUES["url.ajaxBlogPermissionDelete"]);
			Query.on("complete", function(e)
			{
				btn.removeClass("disabled").prop("disabled", false);
				row.remove();
			});

			Query.data(
				{
					username : username,
					blog_id : blogId
				});
			Query.execute();
		});
	};

	this.deleteFast = function(blogId, oBtn)
	{
		BootstrapDialog.show(
		{
			spinicon : "fa fa-spinner fa-spin fa-fw",
			draggable: true,
			type : BootstrapDialog.TYPE_DANGER,
			title : "Подтвердите действие",
			message : $('<div>Все топики будут возвращены пользователям в их черновики. <b>Процесс удаления отменить невозможно!</b> Для продолжения удаления введите в поле ниже слово "<b>Удалить</b>" (регистр важен): <hr style="margin:15px 0;" /> <input type="text" class="form-control" placeholder="Удалить"/></div>'),
			buttons : [
				{
					id : "btn-submit",
					icon : "glyphicon glyphicon-check",
					label : "Подтвердить удаление",
					cssClass : "btn-danger pull-left",
					action : function(dialog)
					{
						if (dialog.getMessage().find('input').val() !== "Удалить")
						{
							BootstrapDialog.show({
								type : BootstrapDialog.TYPE_WARNING,
								message: 'Повторите ввод!'
							});
							return;
						}

						// deleting...
						var $button = this;
						$button.disable();
						$button.spin();
						dialog.setClosable(false);
						dialog.enableButtons(false);

						// выполняем post запрос
						AjaxDelete.data("id", blogId);
						AjaxDelete.empty("success").on("success", function(e)
						{
							dialog.enableButtons(true);
							dialog.setClosable(true);
							$button.stopSpin();

							if (!e.bOk) {
								BootstrapDialog.show({
									type : BootstrapDialog.TYPE_WARNING,
									title : "Ошибка",
									message: e.message
								});
								return;
							}

							dialog.setType(BootstrapDialog.TYPE_SUCCESS);
							dialog.setTitle("Блог успешно удален!");

							var message = "Блог <b>"+e.blogTitle+"</b> успешно удален.";
							if (e.movedTopicsCount>0)
								message += " Все его топики <b>("+e.movedTopicsCount+")</b> были перемещены авторам в черновики.";

							dialog.setMessage(message);
							$button.disable();

							$button.remove();
							dialog.getButton("btn-close").text("Закрыть").addClass("btn-success").removeClass("btn-default");

						}).execute();
					}
				},
				{
					id : "btn-close",
					label : "Отменить",
					action : function(dialog)
					{
						dialog.close();
					}
				}
			]
		});
	};

	return this;
}).call(Space.Blog);