var Space = Space || {};

Space.CKEDITOR = (function()
{

	this.replace = function(field, confName)
	{
		var conf = {};
		if (confName)
		{
			conf.customConfig = "/app/frontend/CKEDITOR/"+confName+".js";
		}

		CKEDITOR.replace(field, conf);
		return this;
	};

	return this;
}).call(Space.CKEDITOR || {});

// Create Button CUT

(function()
{
	CKEDITOR.plugins.add('ButtonCut',
	{
		requires: ['dialog'],
		init: function(editor)
		{
			CKEDITOR.addCss("cut {display:block;width: 100%;background:#ddd;padding:3px 5px;margin:15px 0;text-align:center;}");

			CKEDITOR.dtd['cut']={};
			// CKEDITOR.dtd.$empty['cut'] = true;
			CKEDITOR.dtd.$nonEditable['cut'] = true;
			CKEDITOR.dtd.$object['cut'] = true;

			editor.addCommand('insertCut', {
				exec: function(editor) {
					var element = CKEDITOR.dom.element.createFromHtml('<cut>Читать далее...</cut>');
					element.unselectable();
					element.contentEditable = false;
					editor.insertElement(element);
				}
			});

			editor.ui.addButton('ButtonCut',
			{
				label: "Cut",
				command: "insertCut",
				toolbar : "insert",
				icon : "/app/frontend/images/cut.png"
			});
		}
	});
})();
