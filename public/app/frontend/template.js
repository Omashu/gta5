jQuery(function()
{
	$('[data-toggle="tooltip"]').tooltip({placement:"auto"});

	if ($('.content .topic-content').length)
	{
		$('.content .topic-content').find('.blog-topic-image-link').colorbox({rel:'BlogTopic',scalePhotos:true,maxWidth:'90%', maxHeight : '90%'});
	}
});