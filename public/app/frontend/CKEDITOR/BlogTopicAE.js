CKEDITOR.editorConfig = function(config)
{
	config.allowedContent = true;
	config.extraPlugins = 'ButtonCut,youtube,div';

	config.removePlugins = 'save, print, preview, pagebreak, newpage, language, smiley, iframe, forms, font, flash, div, templates, colordialog, bidi, dialogadvtab, wsc, scayt, filebrowser, about';
	config.removeButtons = 'Image';
};