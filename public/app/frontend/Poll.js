var Space = Space || {};

Space.Poll = (function()
{
	var Query = new Ajax();
	Query.method("post").url(_VALUES["url.ajaxPoll"]);

	this.query = function(pollId, ids, success, error)
	{
		Query.data("id", pollId).data("ids", ids).empty("error").empty("success").on("success", success)
			.on("error", error)
			.execute();
	};

	this.getResults = function(pollId, oBtn, oBlock, oBlockActions)
	{
		oBtn.prop("disabled", true).addClass("disabled");

		var resQuery = new Ajax();
		resQuery.data("id", pollId).method("get").url(_VALUES["url.ajaxPollResults"])
			.on("success", function(e)
				{
					if (!e.bOk) return false;

					oBlock.html(e.results);
					oBlockActions.hide();
				}).execute();
	};

	return this;
}).call(Space.Poll || {});

jQuery(function()
{
	$.each($(".js-polling"), function(index, val) {
		var oBlock = $(this);

		oBlock.find(".js-polling-submit").click(function(e)
		{
			e.preventDefault();

			$(this).prop("disabled", true).addClass("disabled");

			var selectedObjs = oBlock.find(".js-polling-answers").find("input:checked");
			var selectedVals = [];

			$.each(selectedObjs, function()
			{
				selectedVals.push($(this).val());
			});

			if (selectedVals)
			{
				Space.Poll.query(oBlock.data("poll-id"), selectedVals, function(e)
				{
					oBlock.find(".js-polling-answers").html(e.results);
					oBlock.find(".js-polling-actions").hide();
				}, function()
				{
					$(this).prop("disabled", false).removeClass("disabled");
				});
			}
		});
	});

});
