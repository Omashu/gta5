var Space = Space || {};

Space.User = (function()
{
	this.autocomplete = function(field)
	{
		$(field).autocomplete(
			{
				params :
				{
					_token : _VALUES["token"]
				},
				delimiter : ", ",
				serviceUrl : _VALUES["url.ajaxUserComplete"]
			});
	};

	return this;
}).call(Space.User || {});