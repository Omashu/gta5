var Space = Space || {};

Space.AButtons = (function()
{
	this.mode = function(btn)
	{
		btn.click(function()
		{
			var mode = $(this).data("mode");
			var input = $($(this).data("input"));

			input.val(mode);
			return true;
		});
	};

	return this;
}).call(Space.AButtons || {});