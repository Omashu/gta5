var Space = Space || {};

Space.Button = (function(value)
{
	var tmpl = $([
		'<button class="btn btn-default">',
		'</button>'
		].join(""));

	var removeTypeClasses = function()
	{
		tmpl.removeClass("btn-default btn-primary btn-danger btn-info btn-success btn-link");
	};

	var removeSizeClasses = function()
	{
		tmpl.removeClass('btn-xs btn-lg btn-sm');
	};

	this.setHtml = function(v)
	{
		tmpl.html(v);
		return this;
	};

	if (value) this.setHtml(value);

	this.sizeXs = function()
	{
		tmpl.addClass('btn-xs');
		return this;
	};

	this.sizeSm = function()
	{
		tmpl.addClass('btn-sm');
		return this;
	};

	this.sizeLg = function()
	{
		tmpl.addClass('btn-lg');
		return this;
	};

	this.typePrimary = function()
	{
		removeTypeClasses();
		tmpl.addClass('btn-primary');
		return this;
	};

	this.typeDanger = function()
	{
		removeTypeClasses();
		tmpl.addClass('btn-danger');
		return this;
	};

	this.typeLink = function()
	{
		removeTypeClasses();
		tmpl.addClass('btn-link');
		return this;
	};

	this.getTmpl = function()
	{
		return tmpl;
	};

	return this;
});