var Space = Space || {};

Space.Rating = (function()
{
	var upDownHandler = function(url, id, oBtn, oValue)
	{
		if (oBtn.hasClass("disabled"))
			return false;

		if (!_VALUES["userId"])
		{
			BootstrapDialog.show({
				type : BootstrapDialog.TYPE_WARNING,
				message: "Только для пользователей. Авторизуйтесь или зарегистрируйтесь."
			});
			return false;
		}

		oBtn.addClass("disabled");

		var Query = new Ajax();
		Query.method("post")
		Query.url(url)
		Query.data("id", id);

		var curValueOBtn = oBtn.html();

		oBtn.html('<i class="fa fa-spinner fa-spin"></i>');
		oValue.html('<i class="fa fa-spinner fa-spin"></i>');

		Query.on("success", function(e)
		{
			oBtn.removeClass("disabled");
			oBtn.html(curValueOBtn);
			oValue.text(e.value);

			if (!e.bOk) {
				BootstrapDialog.show({
					type : BootstrapDialog.TYPE_WARNING,
					message: e.message
				});

				return;
			}
		}).execute();

		return false;
	};

	this.up = function(url, id, oBtn, oValue)
	{
		return upDownHandler(url, id, oBtn, oValue);
	};

	this.down = function(url, id, oBtn, oValue)
	{
		return upDownHandler(url, id, oBtn, oValue);
	};

	return this;
}).call(Space.Rating || {});