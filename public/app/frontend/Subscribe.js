var Space = Space || {};

Space.Subscribe = (function()
{
	var POST = new Ajax();
	POST.method("post");

	this.toggleBlog = function(blogId, objectOnSuccessShow, objectOnSuccessHide, countersSelector)
	{
		countersSelector = countersSelector ? $(countersSelector) : null;

		if (objectOnSuccessShow.hasClass("disabled") || objectOnSuccessHide.hasClass("disabled"))
		{
			return false;
		}

		objectOnSuccessShow.addClass("disabled");
		objectOnSuccessHide.addClass("disabled");

		POST.data("id", blogId);
		POST.url(_VALUES["url.ajaxSubscribeToggleBlog"]);
		POST.empty("success").on("success", function(e)
		{
			objectOnSuccessShow.show();
			objectOnSuccessHide.hide();

			objectOnSuccessShow.removeClass("disabled");
			objectOnSuccessHide.removeClass("disabled");

			$(countersSelector).each(function()
			{
				if (e.toggleType == "delete")
					$(this).text().length
						? $(this).text(parseInt($(this).text())-1)
						: $(this).val(parseInt($(this).val())-1);
				else
					$(this).text().length
						? $(this).text(parseInt($(this).text())+1)
						: $(this).val(parseInt($(this).val())+1);
			});

		}).execute();

		return false;
	};

	return this;
}).call(Space.Subscribe || {});
