var Space = Space || {};

Space.Tag = (function()
{

	this.autocomplete = function(field)
	{
		$(field).autocomplete(
			{
				params :
				{
					_token : _VALUES["token"]
				},
				delimiter : ", ",
				serviceUrl : _VALUES["url.ajaxTagComplete"]
			});
	};

	return this;
}).call(Space.Tag || {});