var Space = Space || {};
Space.Comment = Space.Comment || {};

Space.Comment.upSpamLevel = function(cID, oBtn)
{
	if (oBtn.hasClass("disabled"))
		return false;

	var Query = new Ajax();
	Query.method("post").data("id", cID).url(_VALUES["url.ajaxCommentSpamUp"]);

	var origHtml = oBtn.html();
	oBtn.text("Подождите...").prop("disabled", true).addClass("disabled");

	Query.on("success", function(e)
	{
		if (!e.bOk) {
			oBtn.html(e.message).addClass("text-danger");
			return;
		}

		oBtn.prop("disabled", false).removeClass("disabled");

		if (e.commentSpam) {
			$("#comment_message_"+cID).text("Комментарий удален").css("color", "gray");
			oBtn.text("Восстановить").prop("disabled", false).removeClass("disabled");
		} else {
			if (!e.commentSpamLevel) {
				if (e.allowSetSpam)
					oBtn.text("Скрыть");
				else
					oBtn.text("Это спам");

				$("#comment_message_"+cID).html(e.commentMessage).css("color", "#333");
			} else {
				oBtn.text("Модератор уведомлен");
			}
		}

		oBtn.addClass("text-success");
	}).on("error", function()
	{
		oBtn.text("Ошибка").addClass("text-danger");
	}).execute();

	return false;
};

Space.Comment.DefaultFormTmpl = (function()
{
	var pieceSubmitOriginalValue = '<i class="fa fa-paper-plane-o"></i> Отправить';

	var pieces = {
		bold : $('<button type="button" class="btn btn-default btn-sm"><i class="fa fa-bold"></i></button>'),
		italic : $('<button type="button" class="btn btn-default btn-sm"><i class="fa fa-italic"></i></button>'),
		underline : $('<button type="button" class="btn btn-default btn-sm"><i class="fa fa-underline"></i></button>'),
		strikethrough : $('<button type="button" class="btn btn-default btn-sm"><i class="fa fa-strikethrough"></i></button>'),
		smile : $('<button type="button" class="btn btn-default btn-sm"><i class="fa fa-smile-o"></i></button>'),
		message : $('<textarea name="message" class="form-control js-message" placeholder="Ваш комментарий..."></textarea>'),
		submit : $('<button class="btn btn-sm btn-primary js-submit">'+pieceSubmitOriginalValue+'</button>')
	};

	var tmpl = $([
		'<div><div class="form-group" style="margin-bottom:5px;">',
			'<div class="btn-group js-buttons" role="group"></div>',
		'</div>',
		'<div class="form-group js-message"></div></div>'
	].join(""));

	tmpl.find(".js-buttons").append(pieces.bold);
	tmpl.find(".js-buttons").append(pieces.italic);
	tmpl.find(".js-buttons").append(pieces.underline);
	tmpl.find(".js-buttons").append(pieces.strikethrough);
	tmpl.find(".js-buttons").append(pieces.smile);

	tmpl.find(".js-message").append(pieces.message);
	tmpl.append(pieces.submit);

	this.getTmpl = function()
	{
		return tmpl;
	};

	this.setStateLoading = function()
	{
		pieces.message.prop("disabled", true);
		pieces.submit.prop("disabled", true).html('<i class="fa fa-spinner fa-spin"></i>');
		return this;
	};

	this.setStateDisabled = function()
	{
		tmpl.css('opacity', .7);
		pieces.message.prop("disabled", true);
		pieces.submit.prop("disabled", true);
		return this;
	};

	this.resetState = function()
	{
		tmpl.css('opacity', 1);
		pieces.message.prop("disabled", false);
		pieces.submit.prop("disabled", false).html(pieceSubmitOriginalValue);
		return this;
	};

	this.getPieceSubmit = function()
	{
		return pieces.submit;
	};

	this.getPieceMessage = function()
	{
		return pieces.message;
	};

	this.getGroupButtons = function()
	{
		return tmpl.find(".js-buttons");
	};

	return this;
});

Space.Comment.FormController = (function(args)
{
	var self = this;
	var elId = args.id || null;

	var FORM = args.formTemplate;
	var FORM_ANSWER = args.formAnswerTemplate;

	var FORM_CONT = args.formContainer;
	var COMM_CONT = args.commentsContainer;

	var BTN_ANSWER_SELECTOR = args.answerButtonSelector;

	var ROUTE_NAME = args.route;
	var URL = _VALUES["url." + ROUTE_NAME];

	// ajax
	var POST = new Ajax().method("post").url(URL).data("id", elId);

	// cancel buttons
	var BTN_CANCEL_ANSWER = $('<button/>').hide().text("Отменить ответ")
		.addClass("btn btn-default btn-sm pull-right");
	var BTN_CANCEL_ANSWER1 = BTN_CANCEL_ANSWER.clone().show();

	var MESSAGE_BLOCK = $('<span/>').css({
			'position': 'relative',
			'left': '10px',
			'top': '6px'
		}).addClass("text-success");

	var MESSAGE_BLOCK_TIMEOUT= null;

	var RunMessageBlockTimeout = function()
	{
		ClearMessageBlockTimeout();

		MESSAGE_BLOCK_TIMEOUT = setTimeout(function()
		{
			MESSAGE_BLOCK.fadeOut(300);
		}, 6600);
	};

	var ClearMessageBlockTimeout = function()
	{
		if (MESSAGE_BLOCK_TIMEOUT) clearTimeout(MESSAGE_BLOCK_TIMEOUT);
	};

	var PostProcess = function(pID)
	{
		// adding POST data
		POST.data("parent_id", pID);

		if (pID)
		{
			// state loading answer form
			FORM_ANSWER.setStateLoading();

			// answer message
			POST.data("message", FORM_ANSWER.getPieceMessage().val());
		} else
		{
			FORM.setStateLoading();

			// main form message
			POST.data("message", FORM.getPieceMessage().val());
		}

		POST.execute();
	};

	var CancelAnswer = function()
	{
		// прячем форму ответа
		FORM_ANSWER.getTmpl().hide();

		// возвращаем message box на главную форму
		FORM.getGroupButtons().append(MESSAGE_BLOCK);

		// активируем форму
		FORM.resetState();

		// прячем кнопку
		BTN_CANCEL_ANSWER.hide();
	};

	// вставляем в контейнер
	FORM.getTmpl().appendTo(FORM_CONT);
	FORM.getTmpl().append(BTN_CANCEL_ANSWER);

	FORM.getGroupButtons().append(MESSAGE_BLOCK);

	// изменяем FORM_ANSWER
	FORM_ANSWER.getTmpl().css('margin-top', '5px').css('padding-top', '5px').css('border-top', '1px solid #ddd');
	FORM_ANSWER.getTmpl().append(BTN_CANCEL_ANSWER1);

	// биндимся на кнпоку "ответить"
	COMM_CONT.on('click', BTN_ANSWER_SELECTOR, function(e)
	{
		e.preventDefault();
		var pID = $(this).data("id");

		// is opened?
		if (FORM_ANSWER.getTmpl().data("id") == pID && !FORM_ANSWER.getTmpl().is(":hidden"))
		{
			// da, zakrivaem
			BTN_CANCEL_ANSWER1.click();
			return;
		}

		$(this).data("_answerOn", true);

		// подставляем форму ответа
		var answCont = $("#comment_"+pID+"_answer_form");
		answCont.html(FORM_ANSWER.getTmpl().show());

		// перенесем message block
		FORM_ANSWER.getGroupButtons().append(MESSAGE_BLOCK);

		// запомним parent_id
		FORM_ANSWER.getTmpl().data("id", pID);

		// отключаем основную форму
		FORM.setStateDisabled();

		// включаем кнопку отмены у основной формы
		BTN_CANCEL_ANSWER.show();

		BTN_CANCEL_ANSWER.unbind().click(CancelAnswer);
		FORM_ANSWER.getTmpl().find(BTN_CANCEL_ANSWER1).unbind().click(CancelAnswer);
		FORM_ANSWER.getPieceSubmit().unbind().click(function()
		{
			PostProcess(FORM_ANSWER.getTmpl().data("id"));
		})
	});

	FORM.getPieceSubmit().click(function()
	{
		PostProcess(null);
	});

	POST.on("success", function(e)
	{
		RunMessageBlockTimeout();

		if (e.parent_id) FORM_ANSWER.resetState();
		else FORM.resetState();

		if (e.parent_id && e.bOk)
		{
			FORM_ANSWER.getTmpl().data("id", null);
			BTN_CANCEL_ANSWER.click();
		}

		if (!e.bOk) {
			MESSAGE_BLOCK.fadeIn(300).addClass("text-danger").removeClass("text-success").text(e.message);
			return;
		}

		MESSAGE_BLOCK.fadeIn(300).addClass("text-success").removeClass("text-danger").text(e.message);

		if (e.parent_id) FORM_ANSWER.getPieceMessage().val('');
		else FORM.getPieceMessage().val('');

		// var eCView = $(e.view).hide();

		if (e.parent_id)
			$("#comment_"+e.parent_id+"_answers").append(e.view);
		else
			COMM_CONT.prepend(e.view);
	});

	POST.on("error", function(e)
	{
		// pyst' cmotryat raqi
		ClearMessageBlockTimeout();

		// RunMessageBlockTimeout();
		MESSAGE_BLOCK.text("Ошибка сервера! Обновите страницу и попробуйте снова, если ошибка сохранится собщите администратору.").addClass("text-danger").removeClass("text-success");

		if (e.parent_id) FORM_ANSWER.resetState();
		else FORM.resetState();
	});

	return this;
});