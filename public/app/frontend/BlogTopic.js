var Space = Space || {};

Space.BlogTopic = (function()
{
	var self = this;

	var AjaxDelete = new Ajax();
	AjaxDelete.method("post").url(_VALUES["url.ajaxBlogTopicDelete"]);

	this.deleteFast = function(blogTopcId, oBtn)
	{
		BootstrapDialog.show(
		{
			spinicon : "fa fa-spinner fa-spin fa-fw",
			draggable: true,
			type : BootstrapDialog.TYPE_DANGER,
			title : "Подтвердите действие",
			message : $('<div>Топик будет отправлен в корзину. Восстановить его можно будет невозможно.</div>'),
			buttons : [
				{
					id : "btn-submit",
					icon : "glyphicon glyphicon-check",
					label : "Подтвердить удаление",
					cssClass : "btn-danger pull-left",
					action : function(dialog)
					{
						// deleting...
						var $button = this;
						$button.disable();
						$button.spin();
						dialog.setClosable(false);
						dialog.enableButtons(false);

						// выполняем post запрос
						AjaxDelete.data("id", blogTopcId);
						AjaxDelete.empty("success").on("success", function(e)
						{
							dialog.enableButtons(true);
							dialog.setClosable(true);
							$button.stopSpin();

							if (!e.bOk) {
								BootstrapDialog.show({
									type : BootstrapDialog.TYPE_WARNING,
									title : "Ошибка",
									message: e.message
								});
								return;
							}

							dialog.setType(BootstrapDialog.TYPE_SUCCESS);
							dialog.setTitle("Выполнено!");
							dialog.setMessage(e.message);

							$button.disable();
							$button.remove();
							dialog.getButton("btn-close").text("Закрыть").addClass("btn-success").removeClass("btn-default");
							dialog.getButton("btn-close").click(function()
							{
								location.href = _VALUES["url.home"];
							});
						}).execute();
					}
				},
				{
					id : "btn-close",
					label : "Отменить",
					action : function(dialog)
					{
						dialog.close();
					}
				}
			]
		});
	};

	// управление опросами
	this.aEPolls = function(args)
	{
		var oBtn = args.btn;
		var oCont = args.cont;
		var i = 0;

		var PollMediaObject = (function()
		{
			var iterNum = i++;

			var self = this;
			var countAnswers = 0;

			var media;
			var heading;
			var body;

			var headingTitleGroup;
			var headingTitleInput;
			var headingTitleAddonCanUse;
			var headingTitleAddonAdd;
			var headingTitleAddonMultiple;
			var headingTitleAddonRemove;

			var headingTitleAddonCanUseButton;
			var headingTitleAddonAddButton;
			var headingTitleAddonMultipleButton;
			var headingTitleAddonRemoveButton;

			this.Create = function()
			{
				headingTitleGroup = $("<div/>").addClass("input-group");
				headingTitleInput = $("<input/>").addClass("form-control").prop("name", "_polls["+iterNum+"][title]").prop("placeholder", "Вопрос");

				headingTitleAddonCanUse = $("<div/>")
					.addClass("input-group-addon")
					.css("padding", "0 10px").css({borderLeft : 0, borderRight : 0});

				headingTitleAddonAdd = $("<div/>").addClass("input-group-addon").css("padding", "0 10px");
				headingTitleAddonRemove = $("<div/>").addClass("input-group-addon").css("padding", "0 10px");
				headingTitleAddonMultiple = $("<div/>").addClass("input-group-addon").css("padding", "0 10px").css("border-left", "0");

				headingTitleGroup
					.append(headingTitleInput)
					.append(headingTitleAddonCanUse)
					.append(headingTitleAddonAdd)
					.append(headingTitleAddonMultiple)
					.append(headingTitleAddonRemove);

				headingTitleAddonCanUseButton = $([
					'<div class="btn-group">',
						'<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-users fa-fw"></i> Только пользователи</button>',
						'<input name="_polls['+iterNum+'][can_use]" type="hidden" value="users">',
						'<ul class="dropdown-menu" role="menu">',
							'<li><a href="javascript:void(0);" data-can-use="users"><i class="fa fa-users fa-fw"></i> Только пользователи</a></li>',
							'<li><a href="javascript:void(0);" data-can-use="users_and_ghosts"><i class="fa fa-user-secret fa-fw"></i> Пользователи и гости</a></li>',
						'</ul>',
					'</div>'
				].join(""));

				headingTitleAddonAddButton = $("<button/>").prop("type", "button").addClass("btn btn-default btn-xs").html('<i class="fa fa-plus fa-fw"></i> Ответ');

				headingTitleAddonMultiple.append('<input name="_polls['+iterNum+'][multiple]" type="hidden" value="0">');
				headingTitleAddonMultiple.append('<button class="btn btn-default btn-xs" type="button"><i class="fa fa-check-square-o fa-fw"></i> Множественый</button>');
				headingTitleAddonMultipleButton = headingTitleAddonMultiple.find("button");
				headingTitleAddonRemoveButton = $("<button/>").prop("type", "button").addClass("btn btn-default btn-xs").html('<i class="fa fa-times fa-fw"></i>');

				headingTitleAddonCanUse.html(headingTitleAddonCanUseButton);
				headingTitleAddonAdd.html(headingTitleAddonAddButton);
				headingTitleAddonRemove.html(headingTitleAddonRemoveButton);

				var mediaObject = new Space.MediaObject();
				mediaObject.getTmpl().appendTo(oCont);
				mediaObject.hideMediaLeft(1);
				mediaObject.getBody().css({width:"100%",display:"block"});
				mediaObject.getHeading().css({"margin-bottom":"0"});

				heading = mediaObject.getHeading();
				body = mediaObject.getBody();
				media = mediaObject.getTmpl();

				heading.html(headingTitleGroup);

				bindButtonCanUse();
				bindButtonAdd();
				bindButtonMultiple();
				bindButtonRemove();

				this.AddAnswerRow();
				this.AddAnswerRow();
			};

			this.Parse = function(obj)
			{
				media = obj;
				heading = media.find(".media-heading");
				body = media.find(".media-body");
				i = iterNum = media.data("iter");
				i++;

				headingTitleGroup = heading.find(".input-group");
				headingTitleInput = headingTitleGroup.find("input").first();
				headingTitleAddonCanUse = headingTitleGroup.find(".input-group-addon").eq(0);
				headingTitleAddonAdd = headingTitleGroup.find(".input-group-addon").eq(1);
				headingTitleAddonMultiple = headingTitleGroup.find(".input-group-addon").eq(2);
				headingTitleAddonRemove = headingTitleGroup.find(".input-group-addon").eq(3);

				headingTitleAddonCanUseButton = headingTitleAddonCanUse.find(".btn-group");
				headingTitleAddonAddButton = headingTitleAddonAdd.find("button");
				headingTitleAddonMultipleButton = headingTitleAddonMultiple.find("button");
				headingTitleAddonRemoveButton = headingTitleAddonRemove.find("button");

				bindButtonCanUse();
				bindButtonAdd();
				bindButtonMultiple();
				bindButtonRemove();

				var val = headingTitleAddonCanUse.find("input").val();

				if (val === "users")
					headingTitleAddonCanUse.find("li > a").eq(0).click();
				else if (val === "users_and_ghosts")
					headingTitleAddonCanUse.find("li > a").eq(1).click();

				body.find(".media-answer-row").each(function()
				{
					countAnswers++;
					bindButtonAnswerRemove($(this).find("button"));
				});
			};

			this.AddAnswerRow = function()
			{
				var oAnswerGroup = $("<div/>").addClass("input-group media-answer-row").css("margin-top", "5px");
				var oAnswerInput = $("<input/>").addClass("form-control").prop("name", "_polls["+iterNum+"][answers][new][]").prop("placeholder", "Вариант ответа");
				var oAnswerInputAddonRemove = $("<div/>").addClass("input-group-addon").css("padding", "0 10px");
				var oAnswerInputAddonRemoveButton = $("<button/>").prop("type", "button").addClass("btn btn-default btn-xs").html('<i class="fa fa-times fa-fw"></i>');

				oAnswerInputAddonRemove
					.append(oAnswerInputAddonRemoveButton);

				oAnswerGroup.append(oAnswerInput).append(oAnswerInputAddonRemove);

				body.append(oAnswerGroup);
				bindButtonAnswerRemove(oAnswerInputAddonRemoveButton);

				countAnswers++;
			};

			var bindButtonAnswerRemove = function(button)
			{
				button.click(function(e)
				{
					e.preventDefault();
					if (countAnswers <= 2) return false;

					countAnswers--;
					$(this).parent().parent().remove();
				});
			};

			var bindButtonCanUse = function()
			{
				headingTitleAddonCanUseButton.find("li > a").click(function(e)
				{
					e.preventDefault();
					headingTitleAddonCanUse.find("button").html($(this).html());
					headingTitleAddonCanUse.find("input").val($(this).data("can-use"));
				});
			};

			var bindButtonAdd = function()
			{
				headingTitleAddonAddButton.click(function(e)
				{
					e.preventDefault();
					self.AddAnswerRow();
				});
			};

			var bindButtonMultiple = function()
			{
				headingTitleAddonMultipleButton.click(function(e)
				{
					e.preventDefault();

					if ($(this).hasClass('active'))
					{
						$(this).removeClass('active');
						$(this).parent().find('input').val(0);
					} else
					{
						$(this).addClass('active');
						$(this).parent().find('input').val(1);
					}
				});
			};

			var bindButtonRemove = function()
			{
				headingTitleAddonRemoveButton.click(function(e)
				{
					e.preventDefault();
					media.remove();
				});
			};

			return this;
		});


		$(oBtn).click(function()
		{
			var row = new PollMediaObject();
			row.Create();
		});

		oCont.find(".media").each(function()
		{
			var row = new PollMediaObject();
			row.Parse($(this));
		});
	};

	// загрузка файлов
	this.aEFiles = function(args)
	{
		var cont = args.cont;
		var file = args.file;
		var upl = new Upload({multiple:true,maxFileSize:5242880*10}, file, Upload_FILE);

		if ($(args.contInputs).find("input").length)
			$(args.emptyBlock).hide();

		upl.on("add", function(el)
		{
			var mediaObject = new Space.MediaObject();
			el.storage("mediaObject", mediaObject);
			el.storage("percent", $('<small/>').addClass('text-success').css("padding-left", "5px"));

			cont.append(mediaObject.getTmpl());

			mediaObject.setHeadingText(el.getName());
			mediaObject.hideMediaLeft(1);
			mediaObject.appendToHeading(el.storage("percent"));
			mediaObject.getHeading();

			$(args.emptyBlock).hide();
		});

		upl.on("progress", function(el)
		{
			el.storage("percent").text(el.getPercent() + "%");
		});

		upl.on("fail", function(el)
		{
			el.storage("percent").text("Неудалось загрузить файл").toggleClass('text-success text-danger');
		});

		upl.on("done", function(el)
		{
			if (!el.getRes().bOk)
			{
				el.storage("percent").text(el.getRes().message).toggleClass('text-success text-danger');
				return;
			}

			el.storage("mediaObject").setHeadingText(el.getRes().file.title);

			// create hiden input
			var hInput = $('<input/>').prop("id", "_file_id_"+el.getRes().file.id).prop("type", "hidden").prop("name", args.hiddenInputName).val(el.getRes().file.id).appendTo($(args.contInputs));

			var oBtnEdit = new Space.Button("Изменить");
			var oBtnDelete = new Space.Button("Удалить");

			oBtnEdit.sizeXs().typePrimary();
			oBtnDelete.sizeXs().typeDanger();

			var oBtnGroup = $("<div/>").addClass("btn-group");
			oBtnGroup.append(oBtnEdit.getTmpl().addClass("js-edit").data("id", el.getRes().file.id))
				.append(oBtnDelete.getTmpl().addClass("js-delete").data("id", el.getRes().file.id));

			el.storage("mediaObject").append(oBtnGroup);
		});

		cont.on('click', '.js-delete', function(event) {
			event.preventDefault();

			var oBtn = $(this);
			oBtn.prop("disabled", true).addClass("disabled");

			var ID = $(this).data("id");

			BootstrapDialog.show(
			{
				spinicon : "fa fa-spinner fa-spin fa-fw",
				draggable: true,
				type : BootstrapDialog.TYPE_DANGER,
				title : "Подтвердите действие",
				message : $('<div>Вы действительно хотите удалить этот файл?</div>'),
				buttons : [
					{
						id : "btn-submit",
						icon : "glyphicon glyphicon-check",
						label : "Подтвердить удаление",
						cssClass : "btn-danger pull-left",
						action : function(dialog)
						{
							// deleting...
							var $button = this;
							$button.disable();
							$button.spin();
							dialog.setClosable(false);
							dialog.enableButtons(false);

							// query delete
							var Query = new Ajax();
							Query.url(_VALUES["url.ajaxFileDelete"])
							Query.data("id", ID).execute();

							// remove mediaObject
							oBtn.parent().parent().parent().remove();

							// remove input
							$('#_file_id_'+ID).remove();

							oBtn.prop("disabled", false).removeClass("disabled");
							dialog.close();
						}
					},
					{
						id : "btn-close",
						label : "Отменить",
						action : function(dialog)
						{
							dialog.close();
							oBtn.prop("disabled", false).removeClass("disabled");
						}
					}
				]
			});
		});
	};

	// aEImages - добавление/изменение изображений топика
	this.aEImages = function(args)
	{
		var cont = args.cont;
		var previewInput = args.previewInput;
		var textareaId = args.textareaId;
		var file = args.file;
		var upl = new Upload({multiple: true, previewMaxWidth:50,previewMaxHeight:50}, file, Upload_IMAGE);

		if ($(args.contInputs).find("input").length)
			$(args.emptyBlock).hide();

		upl.on("add", function(el)
		{
			var mediaObject = new Space.MediaObject();
			el.storage("mediaObject", mediaObject);
			el.storage("percent", $('<small/>').addClass('text-success').css("padding-left", "5px"));

			cont.append(mediaObject.getTmpl());

			mediaObject.setHeadingText(el.getName());
			mediaObject.hideMediaLeft(1);
			mediaObject.appendToHeading(el.storage("percent"));
			mediaObject.getHeading();

			$(args.emptyBlock).hide();
		});

		upl.on("processAlways", function(el)
		{
			if (!el.getPreview())
			{
				return;
			}

			el.storage("mediaObject").showMediaLeft(300);
			el.storage("mediaObject").setMediaLeftHtml(el.getPreview());
		});

		upl.on("progress", function(el)
		{
			el.storage("percent").text(el.getPercent() + "%");
		});

		upl.on("fail", function(el)
		{
			el.storage("percent").text("Неудалось загрузить файл").toggleClass('text-success text-danger');
		});

		upl.on("done", function(el)
		{
			if (!el.getRes().bOk)
			{
				el.storage("percent").text(el.getRes().message).toggleClass('text-success text-danger');
				return;
			}

			el.storage("mediaObject").setHeadingText(el.getRes().file.title);

			// create hiden input
			var hInput = $('<input/>').prop("id", "_image_id_"+el.getRes().file.id).prop("type", "hidden").prop("name", args.hiddenInputName).val(el.getRes().file.id).appendTo($(args.contInputs));

			var imageThumbUrl = croppa.url(el.getRes().file.relUrl, 50, 50, ["crop"]);
			var imageObject = $("<img/>").prop("src", imageThumbUrl).prop("alt", el.getRes().file.title).css({
				width : 50,
				height: 50,
				marginBottom : 0
			}).addClass("thumbnail");

			el.storage("mediaObject").setMediaLeftHtml(imageObject);

			// 
			var oBtnPreview = new Space.Button('<i class="fa fa-image"></i> Превью');
			var oBtnDelete = new Space.Button('<i class="fa fa-trash-o"></i> Удалить');
			var oBtnPush = new Space.Button('<i class="fa fa-arrow-circle-o-left"></i> Вставить');

			oBtnPreview.sizeXs().typePrimary();
			oBtnDelete.sizeXs().typeDanger();
			oBtnPush.sizeXs();

			var oBtnGroup = $("<div/>").addClass("btn-group");
			oBtnGroup.append(oBtnPreview.getTmpl().addClass("js-preview").data("id", el.getRes().file.id))
				.append(oBtnDelete.getTmpl().addClass("js-delete").data("id", el.getRes().file.id))
				.append(oBtnPush.getTmpl().addClass("js-push").data("relurl", el.getRes().file.relUrl));

			el.storage("mediaObject").append(oBtnGroup);
		});

		// bind buttons
		cont.on('click', '.js-preview', function(event) {
			event.preventDefault();
			var iId = $(this).data("id");

			previewInput.val(iId);
			cont.find('.js-preview.active').removeClass("active");
			$(this).addClass("active");
		});

		cont.on('click', '.js-push', function(event) {
			event.preventDefault();
			
			var relUrl = $(this).data("relurl");
			var imageObject = $("<img/>").prop("src", relUrl).css("width", 150);
			var imageHtml = imageObject.wrap("<div/>").parent().html();

			CKEDITOR.instances[textareaId].insertHtml(imageHtml);
		});

		cont.on('click', '.js-delete', function(event) {
			event.preventDefault();

			var oBtn = $(this);
			oBtn.prop("disabled", true).addClass("disabled");

			var ID = $(this).data("id");

			BootstrapDialog.show(
			{
				spinicon : "fa fa-spinner fa-spin fa-fw",
				draggable: true,
				type : BootstrapDialog.TYPE_DANGER,
				title : "Подтвердите действие",
				message : $('<div>Вы действительно хотите удалить это изображение?</div>'),
				buttons : [
					{
						id : "btn-submit",
						icon : "glyphicon glyphicon-check",
						label : "Подтвердить удаление",
						cssClass : "btn-danger pull-left",
						action : function(dialog)
						{
							// deleting...
							var $button = this;
							$button.disable();
							$button.spin();
							dialog.setClosable(false);
							dialog.enableButtons(false);

							// query delete
							var Query = new Ajax();
							Query.url(_VALUES["url.ajaxImageDelete"])
							Query.data("id", ID).execute();

							// remove mediaObject
							oBtn.parent().parent().remove();

							// remove input
							$('#_image_id_'+ID).remove();

							// if is preview removing...
							if (previewInput.val() == ID)
								previewInput.val('');

							oBtn.prop("disabled", false).removeClass("disabled");
							dialog.close();
						}
					},
					{
						id : "btn-close",
						label : "Отменить",
						action : function(dialog)
						{
							dialog.close();
							oBtn.prop("disabled", false).removeClass("disabled");
						}
					}
				]
			});
		});
	};

	return this;
}).call(Space.BlogTopic || {});